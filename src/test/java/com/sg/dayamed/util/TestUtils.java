package com.sg.dayamed.util;

import com.sg.dayamed.persistence.domain.UserInformationData;
import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Eresh Gorantla on 15/Apr/2019
 **/

public final class TestUtils {

    private TestUtils() {

    }

    public static UserInformationV generateUserInformationV(Boolean adminFlag, UserInformationData userInformationData, String gender, String email, Integer userAge, String firstName, String lastName, String middleName, String mobileNumber) {
        UserInformationV userInformationV = new UserInformationV();
        userInformationV.setAdminFlag(adminFlag);
        userInformationV.setData(userInformationData);
        userInformationV.setGender(gender);
        userInformationV.setUserEmailId(email);
        userInformationV.setUserAge(userAge);
        userInformationV.setUserFirstName(firstName);
        userInformationV.setUserLastName(lastName);
        userInformationV.setUserMiddleName(middleName);
        userInformationV.setUserMobileNumber(mobileNumber);
        return userInformationV;
    }

    public static UserInformationV generateRandomUserInformationV() {
        return generateUserInformationV(true, null, "Male", RandomStringUtils.randomAlphabetic(25), 25, RandomStringUtils.randomAlphabetic(25), RandomStringUtils.randomAlphabetic(25), RandomStringUtils.randomAlphabetic(25), RandomStringUtils.randomAlphabetic(10));
    }
}
