package com.sg.dayamed.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.sg.dayamed.commons.rest.IResponseCodes;
import com.sg.dayamed.persistence.domain.FrequencyDetails;
import com.sg.dayamed.persistence.domain.Prescription;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import com.sg.dayamed.persistence.domain.PrescriptionInfoType;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import com.sg.dayamed.prescription.vo.FrequencyVO;
import com.sg.dayamed.prescription.vo.PrescriptionBasicInfoVO;
import com.sg.dayamed.prescription.vo.PrescriptionInfoDetailVO;
import com.sg.dayamed.prescription.vo.PrescriptionInfoTypeVO;
import com.sg.dayamed.prescription.vo.PrescriptionInformationVO;
import com.sg.dayamed.prescription.vo.PrescriptionMinInfoVO;
import com.sg.dayamed.rest.ws.WSGetPrescriptionResponse;
import com.sg.dayamed.rest.ws.WSPrescriptionBasicInfo;
import com.sg.dayamed.rest.ws.WSPrescriptionMinInfoResponse;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Assert;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 16/Apr/2019
 **/

public final class AssertUtils {

    private AssertUtils() {

    }

    public static void assertPrescriptions(List<Prescription> prescriptions, List<PrescriptionInformationVO> prescriptionInformationVOS) {
        Assert.assertEquals(prescriptions.size(), prescriptionInformationVOS.size());
        for (int index = 0; index < prescriptionInformationVOS.size(); index++) {
            Prescription prescription = prescriptions.get(index);
            PrescriptionInformationVO informationVO = prescriptionInformationVOS.get(index);
            Assert.assertEquals(prescription.getId(), informationVO.getId());
            Assert.assertEquals(prescription.getUserRoleMapping().getId(), informationVO.getCreatorId());
            Assert.assertEquals(prescription.getPatient().getId(), informationVO.getPatientId());
            Assert.assertEquals(prescription.getActive(), informationVO.getActive());
            Assert.assertEquals(prescription.getPatientIllnessDiagnosis(), informationVO.getDiagnosis());
            Assert.assertEquals(prescription.getPatientIllnessDesc(), informationVO.getIllnessDesc());
            Assert.assertEquals(prescription.getName(), informationVO.getName());
            Assert.assertEquals(prescription.getProviderComment(), informationVO.getProviderComment());
            assertPrescriptionInfoS(prescription.getPrescriptionInfoS(), informationVO.getDetails());
        }
    }

    public static void assertPrescriptionInfoS(List<PrescriptionInfo> prescriptionInfoS, List<PrescriptionInfoDetailVO> prescriptionInfoDetailVOS) {
        Assert.assertEquals(prescriptionInfoS.size(), prescriptionInfoDetailVOS.size());
        for (int index = 0; index < prescriptionInfoS.size(); index++) {
            PrescriptionInfo prescriptionInfo = prescriptionInfoS.get(index);
            PrescriptionInfoDetailVO infoDetailVO = prescriptionInfoDetailVOS.get(index);
            Assert.assertEquals(prescriptionInfo.getId(), infoDetailVO.getId());
            Assert.assertEquals(prescriptionInfo.getActive(), infoDetailVO.getActive());
            Assert.assertEquals(prescriptionInfo.getComment(), infoDetailVO.getComment());
            Assert.assertEquals(prescriptionInfo.getCourseDuration(), infoDetailVO.getDuration());
            if (prescriptionInfo.getIntakeTime() != null) {
                Assert.assertEquals(prescriptionInfo.getIntakeTime().getDetails(), infoDetailVO.getDetails());
            }
            if (prescriptionInfo.getFrequency() != null) {
                assertFrequencyDetails(prescriptionInfo.getFrequency(), infoDetailVO.getFrequency());
            }
            if (prescriptionInfo.getPrescriptionInfoType() != null) {
                assertPrescriptionInfoType(prescriptionInfo.getPrescriptionInfoType(), infoDetailVO.getPrescriptionInfoType());
            }
        }
    }

    public static void assertFrequencyDetails(FrequencyDetails frequencyDetails, FrequencyVO frequencyVO) {
        List<String> details = frequencyDetails.getActualValues().stream().filter(val -> val != null).map(val -> val.toString()).collect(Collectors.toList());
        Assert.assertEquals(details.size(), frequencyVO.getActualValues().size());
        Assert.assertEquals(details, frequencyVO.getActualValues());
        Assert.assertEquals(frequencyDetails.getTypeId(), frequencyVO.getTypeId());
        Assert.assertEquals(frequencyDetails.getDuration(), frequencyVO.getDuration());
        Assert.assertEquals(frequencyDetails.getRecurrence(), frequencyVO.getRecurrence());
        Assert.assertEquals(frequencyDetails.getStartDate(), frequencyVO.getStartDate());
    }

    public static void assertPrescriptionInfoType(PrescriptionInfoType infoType, PrescriptionInfoTypeVO infoTypeVO) {
        Assert.assertEquals(infoType.getId(), infoTypeVO.getId());
        Assert.assertEquals(infoType.getDescription(), infoTypeVO.getDescription());
        Assert.assertEquals(infoType.getName(), infoTypeVO.getName());
        Assert.assertEquals(infoType.getPrescriptionType() != null, infoType.getPrescriptionType() != null);
        Assert.assertEquals(infoType.getPrescriptionType().getId(), infoTypeVO.getPrescriptionType().getId());
        Assert.assertEquals(infoType.getPrescriptionType().getName(), infoTypeVO.getPrescriptionType().getName());
        Assert.assertEquals(infoType.getPrescriptionType().getDescription(), infoTypeVO.getPrescriptionType().getDescription());
    }

    public static void assertPrescriptionInfoType(PrescriptionDetailInfoV detailInfoV, PrescriptionInfoTypeVO infoTypeVO) {
        Assert.assertEquals(detailInfoV.getPresInfoTypeId(), infoTypeVO.getId());
        Assert.assertEquals(detailInfoV.getPresInfoType(), infoTypeVO.getName());
        if (infoTypeVO.getPrescriptionType() != null) {
            Assert.assertEquals(detailInfoV.getPresTypeId(), infoTypeVO.getPrescriptionType().getId());
            Assert.assertEquals(detailInfoV.getPresType(), infoTypeVO.getPrescriptionType().getName());
        }
    }

    public static void assertPrescriptionInfo(PrescriptionDetailInfoV detailInfoV, PrescriptionInformationVO informationVO) {
        Assert.assertEquals(detailInfoV.getActivePrescription(), informationVO.getActive());
        Assert.assertEquals(detailInfoV.getDiagnosis(), informationVO.getDiagnosis());
        Assert.assertEquals(detailInfoV.getPatientId(), informationVO.getPatientId());
        Assert.assertEquals(detailInfoV.getProviderComment(), informationVO.getProviderComment());
        Assert.assertEquals(detailInfoV.getCreatorId(), informationVO.getCreatorId());
        Assert.assertEquals(detailInfoV.getPrescriptionId(), informationVO.getId());
        Assert.assertEquals(detailInfoV.getPatientIllnessDesc(), informationVO.getIllnessDesc());
        Assert.assertEquals(detailInfoV.getPrescriptionName(), informationVO.getName());
    }

    public static void assertGetPrescriptionsResponse(WSPrescriptionMinInfoResponse response, PrescriptionMinInfoVO minInfoVO) {
        assertEquals(IResponseCodes.SUCCESSFUL, response.getResult());
        assertEquals(response.getTotalRecords(), minInfoVO.getTotalRecords());
        assertEquals(response.getPrescriptions().size(), minInfoVO.getPrescriptions().size());
        if (response.getTotalRecords() > NumberUtils.LONG_ZERO) {
            for (int index = 0; index < minInfoVO.getPrescriptions().size(); index ++) {
                WSPrescriptionBasicInfo basicInfo = response.getPrescriptions().get(index);
                PrescriptionBasicInfoVO infoVO = minInfoVO.getPrescriptions().get(index);
                assertPrescriptionBasicInfo(basicInfo, infoVO);
            }
        }
    }

    public static void assertPrescriptionBasicInfo(WSPrescriptionBasicInfo basicInfo, PrescriptionBasicInfoVO basicInfoVO) {
        assertEquals(basicInfoVO.getActive(), basicInfo.getActive());
        assertEquals(basicInfoVO.getDiagnosis(), basicInfo.getDiagnosis());
        assertEquals(basicInfoVO.getIllnessDesc(), basicInfo.getIllnessDesc());
        assertEquals(basicInfoVO.getName(), basicInfo.getName());
        assertEquals(basicInfoVO.getProviderComment(), basicInfo.getProviderComment());
    }

    public static void assertGetPrescriptionResponse(WSGetPrescriptionResponse response, PrescriptionInformationVO informationVO) {
        assertEquals(IResponseCodes.SUCCESSFUL, response.getResult());
        assertNotNull(response.getPrescription());
        assertEquals(informationVO.getActive(), response.getPrescription().getActive());
        assertEquals(informationVO.getDiagnosis(), response.getPrescription().getDiagnosis());
        assertEquals(informationVO.getIllnessDesc(), response.getPrescription().getIllnessDesc());
        assertEquals(informationVO.getName(), response.getPrescription().getName());
        assertEquals(informationVO.getProviderComment(), response.getPrescription().getProviderComment());
    }

}
