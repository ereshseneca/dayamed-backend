package com.sg.dayamed.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.sg.dayamed.commons.crypto.CryptoUtil;
import com.sg.dayamed.commons.util.DateUtils;
import com.sg.dayamed.config.BeanUtil;
import com.sg.dayamed.security.UserPrincipal;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 15/Apr/2019
 **/
@PrepareForTest({BeanUtil.class, CryptoUtil.class, ZonedDateTime.class})
@SuppressStaticInitializationFor("com.sg.dayamed.commons.crypto.CryptoUtil")
public class BaseTestCase {

    protected final String VIEW_RESPONSE_FILE = "view_data.json";
    protected final String SERVICE_REQUESTS_FILE = "service_requests.json";
    protected final String DB_OBJECTS_DATA_FILE = "db_objects_data.json";
    protected final String SERVICE_RESPONSE_FILE = "service_responses.json";
    protected final String WS_REQUESTS_FILE = "ws_requests.json";
    protected JsonNode rootNode;
    protected ObjectMapper objectMapper;
    private BeanUtil beanUtil;

    protected UserPrincipal providerUserPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));

    public BaseTestCase() {
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.registerModule(new JavaTimeModule());
        beanUtil = PowerMockito.mock(BeanUtil.class);
        PowerMockito.mockStatic(BeanUtil.class);
        PowerMockito.mock(CryptoUtil.class);
        PowerMockito.mockStatic(CryptoUtil.class);
    }

    /*public void getBeanUtilMocked(Class<T> clazz) {
        PowerMockito.mock(clazz);
        PowerMockito.when(BeanUtil.getBean(clazz)).thenReturn(clazz);
    }*/

    protected void mockZonedDateTime(String dateString, String zoneId) {
        PowerMockito.mock(ZonedDateTime.class);
        PowerMockito.mockStatic(ZonedDateTime.class);
        ZonedDateTime zonedDateTime = DateUtils.getZonedDateTime(dateString, zoneId);
        PowerMockito.when(ZonedDateTime.now()).thenReturn(zonedDateTime);
    }

    protected void mockCryptoUtilDecryptIdField(String expectedValue) {
        PowerMockito.when(CryptoUtil.decryptIdField(Mockito.anyString())).thenReturn(expectedValue);
    }

    protected void mockCryptoUtilEncryptIdField() {
        PowerMockito.when(CryptoUtil.encryptIdField(Mockito.anyString())).thenReturn(UUID.randomUUID().toString());
    }

    protected <T> T mockBeanUtils(Class<T> beanClass) {
        T mockT = PowerMockito.mock(beanClass);
        PowerMockito.mockStatic(beanClass);
        PowerMockito.when(BeanUtil.getBean(Mockito.eq(beanClass))).thenReturn(mockT);
        return mockT;
    }

    public void setUp(String requestJsonFile) {
        if (StringUtils.isBlank(requestJsonFile)) { // No need to load request JSON file.
            return;
        }

        try (InputStream is = getClass().getClassLoader().getResourceAsStream(requestJsonFile)) {
            rootNode = objectMapper.readTree(is);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to load file:" + requestJsonFile, ex);
        } finally {

        }
    }

    /*protected <T> T generateRequestObject(String nodeName, Class<T> clazz) throws Exception {
        JsonNode requestNode = rootNode.path(nodeName);
        return objectMapper.readValue(requestNode.toString(), clazz);
    }*/

    protected <T> T generateRequestObject(String nodeName, Class<T> clazz, String fileName) throws Exception {
        JsonNode node;
        if (StringUtils.isBlank(fileName)) {
            return null;
        }
        try (InputStream is = getClass().getClassLoader().getResourceAsStream(fileName)) {
            node = objectMapper.readTree(is);
            JsonNode requestNode = node.path(nodeName);
            return objectMapper.readValue(requestNode.toString(), clazz);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to load file:" + fileName, ex);
        } finally {

        }
    }

    protected <T> T generateRequestObject(String nodeName, CollectionType clazz, String fileName) throws Exception {
        JsonNode node;
        if (StringUtils.isBlank(fileName)) {
            return null;
        }
        try (InputStream is = getClass().getClassLoader().getResourceAsStream(fileName)) {
            node = objectMapper.readTree(is);
            JsonNode requestNode = node.path(nodeName);
            return objectMapper.readValue(requestNode.toString(), clazz);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to load file:" + fileName, ex);
        } finally {

        }
    }
}
