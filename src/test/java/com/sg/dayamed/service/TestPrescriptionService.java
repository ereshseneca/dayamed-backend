package com.sg.dayamed.service;

import com.sg.dayamed.commons.constants.IGlobalErrorKeys;
import com.sg.dayamed.commons.constants.IWSGlobalApiErrorKeys;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.commons.util.ValidationUtils;
import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.persistence.domain.Prescription;
import com.sg.dayamed.persistence.domain.PrescriptionInfoType;
import com.sg.dayamed.persistence.domain.UserDetails;
import com.sg.dayamed.persistence.domain.UserRoleMapping;
import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.persistence.repository.NotificationTypeRepository;
import com.sg.dayamed.persistence.repository.PatientAssociationInformationVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailInfoVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionInfoTypeRepository;
import com.sg.dayamed.persistence.repository.PrescriptionRepository;
import com.sg.dayamed.persistence.repository.UserDetailsRepository;
import com.sg.dayamed.persistence.repository.UserInformationVRepository;
import com.sg.dayamed.persistence.repository.UserRoleMappingRepository;
import com.sg.dayamed.prescription.PrescriptionService;
import com.sg.dayamed.prescription.PrescriptionServiceImpl;
import com.sg.dayamed.prescription.vo.FrequencyVO;
import com.sg.dayamed.prescription.vo.PrescriptionInfoVO;
import com.sg.dayamed.prescription.vo.PrescriptionInformationVO;
import com.sg.dayamed.prescription.vo.PrescriptionMinInfoVO;
import com.sg.dayamed.prescription.vo.PrescriptionVO;
import com.sg.dayamed.prescription.vo.SavePrescriptionVO;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.AssertUtils;
import com.sg.dayamed.util.BaseTestCase;
import com.sg.dayamed.util.FrequencyType;
import com.sg.dayamed.validation.IServiceFieldNames;
import com.sg.dayamed.vo.NotificationInformationVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 15/Apr/2019
 **/
@RunWith(PowerMockRunner.class)
@PrepareForTest({PrescriptionInfoTypeRepository.class, NotificationTypeRepository.class})
public class TestPrescriptionService extends BaseTestCase {

    @InjectMocks
    PrescriptionService prescriptionService = new PrescriptionServiceImpl();

    @Mock
    private UserInformationVRepository mockUserInformationVRepository;

    @Mock
    private PatientAssociationInformationVRepository mockPatientAssociationInformationVRepository;

    @Mock
    private PrescriptionDetailInfoVRepository mockPrescriptionDetailInfoVRepository;

    @Mock
    private UserRoleMappingRepository mockUserRoleMappingRepository;

    @Mock
    private PrescriptionRepository mockPrescriptionRepository;

    @Mock
    private UserDetailsRepository mockUserDetailsRepository;

    private UserPrincipal userPrincipal;


    @Before
    public void setUp() throws Exception {
        //super.setUp("view_data.json");
        //super.setUp("service_requests.json");
    }

    @After
    public void tearDown() {
        Mockito.reset(mockUserInformationVRepository, mockPatientAssociationInformationVRepository);
    }

    @Test
    public void testSavePrescription_ErrorScenarios() throws Exception {
        SavePrescriptionVO savePrescriptionVO = new SavePrescriptionVO();
        savePrescriptionVO.setPatientId(NumberUtils.LONG_MINUS_ONE);
        validateSavePrescriptionError(IServiceFieldNames.PATIENT_ID, APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID, new Object[]{NumberUtils.LONG_MINUS_ONE}, savePrescriptionVO, userPrincipal);

        savePrescriptionVO.setPatientId(NumberUtils.LONG_ONE);
        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Invalid")).collect(Collectors.toList()));
        UserInformationV patientInformationV = generateRequestObject("UserInformationV_Patient", UserInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockUserInformationVRepository.findByUserId(Mockito.anyLong())).thenReturn(Stream.of(patientInformationV).collect(Collectors.toList()));
        validateSavePrescriptionError(IServiceFieldNames.PATIENT_ID, APIErrorKeys.ERROR_PATIENT_NOT_ASSOCIATED_WITH_USER, null, savePrescriptionVO, userPrincipal);

        PatientAssociationInformationV informationV = generateRequestObject("PatientAssociationInformationV_Patient", PatientAssociationInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockPatientAssociationInformationVRepository.findByPatientIdAndUserRoleMapId(Mockito.anyLong(), Mockito.anyLong())).thenReturn(informationV);
        validateSavePrescriptionError(IServiceFieldNames.ROLE, APIErrorKeys.ERROR_USER_ROLE_INVALID, null, savePrescriptionVO, userPrincipal);

        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Patient")).collect(Collectors.toList()));
        validateSavePrescriptionError(IServiceFieldNames.ROLE, APIErrorKeys.ERROR_PATIENT_CANNOT_ADD_PRESCRIPTION_FOR_ANOTHER_PATIENT, null, savePrescriptionVO, userPrincipal);

        Mockito.reset(mockUserInformationVRepository);
        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        UserInformationV providerInformationV = generateRequestObject("UserInformationV_Provider", UserInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockUserInformationVRepository.findByUserId(Mockito.anyLong())).thenReturn(Stream.of(providerInformationV).collect(Collectors.toList()));
        validateSavePrescriptionError(IServiceFieldNames.PATIENT_ID, APIErrorKeys.ERROR_PRESCRIPTION_CANNOT_BE_ADDED_OTHER_THAN_PATIENTS, null, savePrescriptionVO, userPrincipal);

        Mockito.reset(mockUserInformationVRepository);
        Mockito.when(mockUserInformationVRepository.findByUserId(Mockito.anyLong())).thenReturn(Stream.of(patientInformationV).collect(Collectors.toList()));
        Mockito.when(mockUserInformationVRepository.findByRoleMappingId(Mockito.anyLong())).thenReturn(null);
        validateSavePrescriptionError(IServiceFieldNames.ROLE_MAP_ID, APIErrorKeys.ERROR_USER_ROLE_MAP_ID_NOT_EXISTS, null, savePrescriptionVO, userPrincipal);
    }

    @Test
    public void testSavePrescription_PrescriptionErrors() throws Exception {
        SavePrescriptionVO savePrescriptionVO = new SavePrescriptionVO();
        savePrescriptionVO.setPatientId(NumberUtils.LONG_ONE);
        savePrescriptionVO.setZoneId("Australia/Eucla");
        PrescriptionVO prescriptionVO = new PrescriptionVO();
        prescriptionVO.setId(100L);
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));

        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        UserInformationV patientInformationV = generateRequestObject("UserInformationV_Patient", UserInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockUserInformationVRepository.findByUserId(Mockito.anyLong())).thenReturn(Stream.of(patientInformationV).collect(Collectors.toList()));
        PatientAssociationInformationV informationV = generateRequestObject("PatientAssociationInformationV_Patient", PatientAssociationInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockPatientAssociationInformationVRepository.findByPatientIdAndUserRoleMapId(Mockito.anyLong(), Mockito.anyLong())).thenReturn(informationV);
        Mockito.when(mockUserInformationVRepository.findByRoleMappingId(Mockito.anyLong())).thenReturn(patientInformationV);
        validateSavePrescriptionError("prescriptions[0].prescriptionId", APIErrorKeys.ERROR_PRESCRIPTION_ID_NOT_FOUND, null, savePrescriptionVO, userPrincipal);
    }

    @Test
    public void testSavePrescription_PrescriptionInfo_Errors() throws Exception {
        SavePrescriptionVO savePrescriptionVO = new SavePrescriptionVO();
        savePrescriptionVO.setPatientId(NumberUtils.LONG_ONE);
        savePrescriptionVO.setZoneId("Australia/Eucla");
        PrescriptionVO prescriptionVO = new PrescriptionVO();
        prescriptionVO.setId(100L);
        PrescriptionInfoVO prescriptionInfoVO = new PrescriptionInfoVO();
        prescriptionInfoVO.setId(1L);
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));

        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        UserInformationV patientInformationV = generateRequestObject("UserInformationV_Patient", UserInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockUserInformationVRepository.findByUserId(Mockito.anyLong())).thenReturn(Stream.of(patientInformationV).collect(Collectors.toList()));
        PatientAssociationInformationV informationV = generateRequestObject("PatientAssociationInformationV_Patient", PatientAssociationInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockPatientAssociationInformationVRepository.findByPatientIdAndUserRoleMapId(Mockito.anyLong(), Mockito.anyLong())).thenReturn(informationV);
        Mockito.when(mockUserInformationVRepository.findByRoleMappingId(Mockito.anyLong())).thenReturn(patientInformationV);
        Mockito.when(mockPrescriptionDetailInfoVRepository.findByPrescriptionIdIn(Mockito.anyList())).thenReturn(Stream.of(generateRequestObject("PrescriptionDetailInfoV_100", PrescriptionDetailInfoV.class, VIEW_RESPONSE_FILE)).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.SAVE_PRESCRIPTION_INFO_ID, true, 0, 0), APIErrorKeys.ERROR_PRESCRIPTION_INFO_ID_NOT_FOUND, null, savePrescriptionVO, userPrincipal);

        prescriptionInfoVO.setId(100L);
        prescriptionVO.getPrescriptionInfoS().clear();
        prescriptionVO.setDeletedPrescriptionInfoIds(Stream.of(new IdIndexVO(0, 1L)).collect(Collectors.toList()));
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.DELETED_PRESCRIPTION_INFO_IDS, true, 0, 0), APIErrorKeys.ERROR_PRESCRIPTION_INFO_ID_NOT_FOUND, null, savePrescriptionVO, userPrincipal);

        prescriptionInfoVO.setId(100L);
        prescriptionVO.getPrescriptionInfoS().clear();
        prescriptionVO.setDeletedPrescriptionInfoIds(Stream.of(new IdIndexVO(0, 100L)).collect(Collectors.toList()));
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));
        PrescriptionDetailInfoV prescriptionDetailInfoV = generateRequestObject("PrescriptionDetailInfoV_100", PrescriptionDetailInfoV.class, VIEW_RESPONSE_FILE);
        prescriptionDetailInfoV.setActivePrescriptionInfo(false);
        Mockito.when(mockPrescriptionDetailInfoVRepository.findByPrescriptionIdIn(Mockito.anyList())).thenReturn(Stream.of(prescriptionDetailInfoV).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.DELETED_PRESCRIPTION_INFO_IDS, true, 0, 0), APIErrorKeys.ERROR_PRESCRIPTION_INFO_ID_ALREADY_DELETED, null, savePrescriptionVO, userPrincipal);

        prescriptionDetailInfoV.setActivePrescriptionInfo(true);
        FrequencyVO frequencyVO = new FrequencyVO();
        frequencyVO.setTypeId(FrequencyType.WEEKLY.getId());
        prescriptionInfoVO.getDeletedNotificationIds().clear();
        prescriptionInfoVO.setFrequency(frequencyVO);
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));
        Mockito.when(mockPrescriptionDetailInfoVRepository.findByPrescriptionIdIn(Mockito.anyList())).thenReturn(Stream.of(prescriptionDetailInfoV).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES, true, 0, 0), IGlobalErrorKeys.ERRORS_GENERAL_VALUE_REQUIRED, null, savePrescriptionVO, userPrincipal);

        frequencyVO.setActualValues(Stream.of("").collect(Collectors.toList()));
        prescriptionInfoVO.setFrequency(frequencyVO);
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES_VALUE, true, 0, 0, 0), APIErrorKeys.ERROR_NULL_IS_NOT_ACCEPTED, null, savePrescriptionVO, userPrincipal);

        frequencyVO.setActualValues(Stream.of("Mon", "Tue", "xxx").collect(Collectors.toList()));
        prescriptionInfoVO.setFrequency(frequencyVO);
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES_VALUE, true, 0, 0, 2), APIErrorKeys.ERROR_INVALID_WEEK_DAY_VALUE, null, savePrescriptionVO, userPrincipal);

        frequencyVO.setTypeId(FrequencyType.MONTHLY.getId());
        frequencyVO.setActualValues(Stream.of("1", "10", "0").collect(Collectors.toList()));
        prescriptionInfoVO.setFrequency(frequencyVO);
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES_VALUE, true, 0, 0, 2), APIErrorKeys.ERROR_INVALID_DAY_VALUE_FOR_MONTH, null, savePrescriptionVO, userPrincipal);
    }

    @Test
    public void testSavePrescription_ValidatePresNotifications() throws Exception {
        SavePrescriptionVO savePrescriptionVO = new SavePrescriptionVO();
        savePrescriptionVO.setPatientId(NumberUtils.LONG_ONE);
        savePrescriptionVO.setZoneId("Australia/Eucla");
        PrescriptionVO prescriptionVO = new PrescriptionVO();
        prescriptionVO.setId(100L);
        PrescriptionInfoVO prescriptionInfoVO = new PrescriptionInfoVO();
        prescriptionInfoVO.setId(100L);
        NotificationInformationVO notificationInformationVO = new NotificationInformationVO();
        notificationInformationVO.setId(100L);
        prescriptionInfoVO.setNotifications(Stream.of(notificationInformationVO).collect(Collectors.toList()));
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));

        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        UserInformationV patientInformationV = generateRequestObject("UserInformationV_Patient", UserInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockUserInformationVRepository.findByUserId(Mockito.anyLong())).thenReturn(Stream.of(patientInformationV).collect(Collectors.toList()));
        PatientAssociationInformationV informationV = generateRequestObject("PatientAssociationInformationV_Patient", PatientAssociationInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockPatientAssociationInformationVRepository.findByPatientIdAndUserRoleMapId(Mockito.anyLong(), Mockito.anyLong())).thenReturn(informationV);
        Mockito.when(mockUserInformationVRepository.findByRoleMappingId(Mockito.anyLong())).thenReturn(patientInformationV);
        Mockito.when(mockPrescriptionDetailInfoVRepository.findByPrescriptionIdIn(Mockito.anyList())).thenReturn(Stream.of(generateRequestObject("PrescriptionDetailInfoV_100", PrescriptionDetailInfoV.class, VIEW_RESPONSE_FILE)).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.SAVE_PRESCRIPTION_NOTIFICATION_IDS, true, 0, 0, 0), APIErrorKeys.ERROR_NOTIFICATION_ID_NOT_FOUND,
                null, savePrescriptionVO, userPrincipal);

        prescriptionInfoVO.setDeletedNotificationIds(Stream.of(new IdIndexVO(0, 100L)).collect(Collectors.toList()));
        prescriptionInfoVO.getNotifications().clear();
        prescriptionVO.setPrescriptionInfoS(Stream.of(prescriptionInfoVO).collect(Collectors.toList()));
        savePrescriptionVO.setPrescriptions(Stream.of(prescriptionVO).collect(Collectors.toList()));
        validateSavePrescriptionError(ValidationUtils.getFieldName(IServiceFieldNames.DELETED_NOTIFICATIONS_IDS, true, 0, 0, 0), APIErrorKeys.ERROR_NOTIFICATION_ID_NOT_FOUND,
                null, savePrescriptionVO, userPrincipal);
    }


    @Test
    public void testSavePrescription() throws Exception {
        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        SavePrescriptionVO savePrescriptionVO = generateRequestObject("SavePrescriptionVO_New", SavePrescriptionVO.class, SERVICE_REQUESTS_FILE);
        UserInformationV patientInformationV = generateRequestObject("UserInformationV_Patient", UserInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockUserInformationVRepository.findByUserId(Mockito.anyLong())).thenReturn(Stream.of(patientInformationV).collect(Collectors.toList()));
        PatientAssociationInformationV informationV = generateRequestObject("PatientAssociationInformationV_Patient", PatientAssociationInformationV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockPatientAssociationInformationVRepository.findByPatientIdAndUserRoleMapId(Mockito.anyLong(), Mockito.anyLong())).thenReturn(informationV);
        Mockito.when(mockUserInformationVRepository.findByRoleMappingId(Mockito.anyLong())).thenReturn(patientInformationV);
        Mockito.when(mockPrescriptionDetailInfoVRepository.findByPrescriptionIdIn(Mockito.anyList())).thenReturn(Stream.of(generateRequestObject("PrescriptionDetailInfoV_100", PrescriptionDetailInfoV.class, VIEW_RESPONSE_FILE)).collect(Collectors.toList()));
        PrescriptionInfoTypeRepository mockInfoTypeRepository = mockBeanUtils(PrescriptionInfoTypeRepository.class);
        Mockito.when(mockInfoTypeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(generateRequestObject("PrescriptionInfoType_1", PrescriptionInfoType.class, DB_OBJECTS_DATA_FILE)));
        mockBeanUtils(NotificationTypeRepository.class);
        UserRoleMapping providerRoleMapping = generateRequestObject("UserRoleMapping_Provider", UserRoleMapping.class, DB_OBJECTS_DATA_FILE);
        Mockito.when(mockUserRoleMappingRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(providerRoleMapping));
        UserDetails patientUserDetails = generateRequestObject("UserDetails_Patient", UserDetails.class, DB_OBJECTS_DATA_FILE);
        Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(patientUserDetails));
        savePrescriptionVO.getPrescriptions().forEach(pres -> {
            pres.setId(100L);
        });
        List<Prescription> prescriptions = savePrescriptionVO.getPrescriptions().stream().map(pres -> pres.toPrescription(savePrescriptionVO.getZoneId())).collect(Collectors.toList());
        Mockito.when(mockPrescriptionRepository.saveAll(Mockito.anyList())).thenReturn(prescriptions);
        Mockito.when(mockPrescriptionRepository.findByIdIn(Mockito.anyList())).thenReturn(prescriptions);
        List<PrescriptionInformationVO> prescriptionInformationVOS = prescriptionService.savePrescription(savePrescriptionVO, userPrincipal);
        AssertUtils.assertPrescriptions(prescriptions, prescriptionInformationVOS);
    }

    @Test
    public void testGetPrescriptionInfo_Errors() throws Exception {
        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 100L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        validateGetPrescriptionInfoError(IServiceFieldNames.PRESCRIPTION_ID, APIErrorKeys.ERROR_PRESCRIPTION_DETAILS_NOT_FOUND_FOR_ID, null, 1L, userPrincipal);

        Mockito.when(mockPrescriptionDetailInfoVRepository.findByPrescriptionId(Mockito.any())).thenReturn(Stream.of(generateRequestObject("PrescriptionDetailInfoV_100", PrescriptionDetailInfoV.class, VIEW_RESPONSE_FILE)).collect(Collectors.toList()));
        validateGetPrescriptionInfoError(IServiceFieldNames.PRESCRIPTION_ID, IWSGlobalApiErrorKeys.ERRORS_AUTHORIZATION_FAILED, null, 1L, userPrincipal);
    }

    @Test
    public void testGetPrescriptionInfo() throws Exception {
        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        PrescriptionDetailInfoV prescriptionDetailInfoV = generateRequestObject("PrescriptionDetailInfoV_100", PrescriptionDetailInfoV.class, VIEW_RESPONSE_FILE);
        Mockito.when(mockPrescriptionDetailInfoVRepository.findByPrescriptionId(Mockito.any())).thenReturn(Stream.of(prescriptionDetailInfoV).collect(Collectors.toList()));
        PrescriptionInformationVO informationVO = prescriptionService.getPrescriptionInfo(100L, userPrincipal);
        AssertUtils.assertPrescriptionInfo(prescriptionDetailInfoV, informationVO);
    }

    @Test
    public void testGetPrescriptions_Errors() throws Exception {
        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 100L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        String patientId = "124311111111111";
        mockCryptoUtilDecryptIdField(null);
        validateGetPrescriptionsError("patientId", IWSGlobalApiErrorKeys.ERRORS_GENERAL_ERROR_UNEXPECTED, null, patientId, userPrincipal, 0, 0);

        patientId = "1";
        mockCryptoUtilDecryptIdField("1");
        Mockito.when(mockUserDetailsRepository.getOne(Mockito.anyLong())).thenReturn(null);
        validateGetPrescriptionsError("patientId", APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID, null, patientId, userPrincipal, 0, 0);

        Mockito.when(mockUserDetailsRepository.getOne(Mockito.anyLong())).thenReturn(new UserDetails());
        Mockito.when(mockUserRoleMappingRepository.getOne(Mockito.anyLong())).thenReturn(null);
        validateGetPrescriptionsError("mapId", APIErrorKeys.ERROR_USER_ROLE_MAP_ID_NOT_EXISTS, null, patientId, userPrincipal, 0, 0);

        Mockito.when(mockUserRoleMappingRepository.getOne(Mockito.anyLong())).thenReturn(new UserRoleMapping());
        Mockito.when(mockPatientAssociationInformationVRepository.findByPatientIdAndUserRoleMapId(Mockito.anyLong(), Mockito.anyLong())).thenReturn(null);
        validateGetPrescriptionsError("patientId", APIErrorKeys.ERROR_NO_PRIVILEGES_TO_VIEW_PATIENT_DATA, null, patientId, userPrincipal, 0, 0);
    }

    @Test
    public void testGetPrescriptions() throws Exception {
        userPrincipal = new UserPrincipal(2L, "name", "test@gmail.com", "********", 2L, Stream.of(new SimpleGrantedAuthority("Provider")).collect(Collectors.toList()));
        String patientId = "1";
        mockCryptoUtilDecryptIdField("1");
        UserDetails userDetails = new UserDetails();
        userDetails.setId(1L);
        UserRoleMapping roleMapping = new UserRoleMapping();
        roleMapping.setId(1L);
        Mockito.when(mockUserDetailsRepository.getOne(Mockito.anyLong())).thenReturn(userDetails);
        Mockito.when(mockUserRoleMappingRepository.getOne(Mockito.anyLong())).thenReturn(roleMapping);
        Mockito.when(mockPatientAssociationInformationVRepository.findByPatientIdAndUserRoleMapId(Mockito.anyLong(), Mockito.anyLong())).thenReturn(new PatientAssociationInformationV());
        List<Prescription> prescriptions = generateRequestObject("PrescriptionsMinInfo", objectMapper.getTypeFactory().constructCollectionType(List.class, Prescription.class), DB_OBJECTS_DATA_FILE);
        prescriptions = prescriptions.stream().sorted(Comparator.comparing(Prescription::getActive, Comparator.nullsLast(Comparator.naturalOrder())).reversed()).collect(Collectors.toList());
        Mockito.when(mockPrescriptionRepository.findByPatientAndUserRoleMappingOrderByActiveDesc(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(new PageImpl<Prescription>(prescriptions));
        PrescriptionMinInfoVO minInfoVO = prescriptionService.getPrescriptions(patientId, userPrincipal, -1, 0);
        assertTrue(minInfoVO != null);
        assertTrue(CollectionUtils.isNotEmpty(minInfoVO.getPrescriptions()));
        assertEquals(prescriptions.size(), minInfoVO.getPrescriptions().size());
    }

    private void validateSavePrescriptionError(String fieldName, String errorKey, Object[] errorParameters, SavePrescriptionVO savePrescriptionVO, UserPrincipal userPrincipal) throws DataValidationException {
        try {
            prescriptionService.savePrescription(savePrescriptionVO, userPrincipal);
            Assert.fail();
        } catch (Exception e) {
            if (e.getCause() instanceof DataValidationException) {
                validateDataValidationException(fieldName, errorKey, errorParameters, e);
            } else {
                Assert.fail();
            }
        }
    }

    private void validateGetPrescriptionsError(String fieldName, String errorKey, Object[] errorParameters, String patientId, UserPrincipal userPrincipal, Integer limit, Integer offset) {
        try {
            prescriptionService.getPrescriptions(patientId, userPrincipal, limit, offset);
            Assert.fail();
        } catch (Exception e) {
            if (e.getCause() instanceof DataValidationException) {
                validateDataValidationException(fieldName, errorKey, errorParameters, e);
            } else {
                Assert.fail();
            }
        }
    }

    private void validateGetPrescriptionInfoError(String fieldName, String errorKey, Object[] errorParameters, Long prescriptionId, UserPrincipal userPrincipal) {
        try {
            prescriptionService.getPrescriptionInfo(prescriptionId, userPrincipal);
            Assert.fail();
        } catch (Exception e) {
            if (e.getCause() instanceof DataValidationException) {
                validateDataValidationException(fieldName, errorKey, errorParameters, e);
            } else {
                Assert.fail();
            }
        }
    }

    private void validateDataValidationException(String fieldName, String errorKey, Object[] errorParameters, Exception e) {
        DataValidationException dve = (DataValidationException) e.getCause();
        Assert.assertEquals(dve.getFieldName(), fieldName);
        Assert.assertEquals(dve.getErrorKey(), errorKey);
        if (ArrayUtils.isNotEmpty(errorParameters)) {
            Assert.assertArrayEquals(dve.getErrorKeyParameters(), errorParameters);
        }
    }
}
