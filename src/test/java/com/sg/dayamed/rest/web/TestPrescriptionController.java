package com.sg.dayamed.rest.web;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.commons.rest.IResponseCodes;
import com.sg.dayamed.commons.rest.RestApiException;
import com.sg.dayamed.commons.rest.RestFault;
import com.sg.dayamed.commons.rest.RestResponse;
import com.sg.dayamed.prescription.PrescriptionService;
import com.sg.dayamed.prescription.vo.PrescriptionInformationVO;
import com.sg.dayamed.prescription.vo.PrescriptionMinInfoVO;
import com.sg.dayamed.rest.ws.WSGetPrescriptionResponse;
import com.sg.dayamed.rest.ws.WSGetPrescriptionsResponse;
import com.sg.dayamed.rest.ws.WSPrescriptionMinInfoResponse;
import com.sg.dayamed.rest.ws.WSSavePrescriptionRequest;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.AssertUtils;
import com.sg.dayamed.validation.IServiceFieldNames;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 17/Apr/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestPrescriptionController extends BaseRestImplTest {

    @Mock
    private PrescriptionService mockPrescriptionService;


    @InjectMocks
    private PrescriptionController prescriptionController = new PrescriptionController();

    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(prescriptionController, "objectMapper", objectMapper);
        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String date = formatter.format(ZonedDateTime.now());*/
        String dateTime = "2019-04-17 00:00:00";
        mockZonedDateTime(dateTime, null);
        mockCryptoUtilDecryptIdField("1");
        mockCryptoUtilEncryptIdField();
    }

    @Test
    public void testGetPatientPrescriptions_Errors() throws Exception {
        DataValidationException dve = new DataValidationException("patientId", APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID);
        Mockito.when(mockPrescriptionService.getPrescriptions(Mockito.anyString(), Mockito.any(), Mockito.anyInt(), Mockito.anyInt())).thenThrow(new ApplicationException(dve));
        validateGetPrescriptionsExceptions("patientId", APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID, IResponseCodes.DATA_VALIDATION_ERROR,"1", providerUserPrincipal, NumberUtils.INTEGER_ZERO, NumberUtils.INTEGER_ZERO);
    }

    @Test
    public void testGetPatientPrescriptions() throws Exception {
        PrescriptionMinInfoVO minInfoVO = generateRequestObject("PrescriptionsMinInfo", PrescriptionMinInfoVO.class, SERVICE_RESPONSE_FILE);
        Mockito.when(mockPrescriptionService.getPrescriptions(Mockito.anyString(), Mockito.any(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(minInfoVO);
        ResponseEntity<RestResponse> responseEntity = prescriptionController.getPatientPrescriptions("1", providerUserPrincipal, 0, 0);
        WSPrescriptionMinInfoResponse response = (WSPrescriptionMinInfoResponse) responseEntity.getBody();
        AssertUtils.assertGetPrescriptionsResponse(response, minInfoVO);
    }

    @Test
    public void testGetPrescriptionError() throws Exception {
        DataValidationException dve = new DataValidationException(IServiceFieldNames.PRESCRIPTION_ID, APIErrorKeys.ERROR_PRESCRIPTION_DETAILS_NOT_FOUND_FOR_ID);
        Mockito.when(mockPrescriptionService.getPrescriptionInfo(Mockito.anyLong(), Mockito.any(UserPrincipal.class))).thenThrow(new ApplicationException(dve));
        validateGetPrescriptionExceptions(IServiceFieldNames.PRESCRIPTION_ID, APIErrorKeys.ERROR_PRESCRIPTION_DETAILS_NOT_FOUND_FOR_ID, IResponseCodes.DATA_VALIDATION_ERROR, 1L, providerUserPrincipal);
    }

    @Test
    public void testGetPrescription() throws Exception {
        PrescriptionInformationVO informationVO = generateRequestObject("PrescriptionInformationVO", PrescriptionInformationVO.class, SERVICE_RESPONSE_FILE);
        Mockito.when(mockPrescriptionService.getPrescriptionInfo(Mockito.anyLong(), Mockito.any(UserPrincipal.class))).thenReturn(informationVO);
        ResponseEntity<RestResponse> responseEntity = prescriptionController.getPrescription(NumberUtils.LONG_ONE, providerUserPrincipal);
        WSGetPrescriptionResponse response = (WSGetPrescriptionResponse) responseEntity.getBody();
        assertEquals(true, response != null);
        AssertUtils.assertGetPrescriptionResponse(response, informationVO);
    }

    @Test
    public void testSavePrescription_Error() throws Exception {
        WSSavePrescriptionRequest request = generateRequestObject("SavePrescription", WSSavePrescriptionRequest.class, WS_REQUESTS_FILE);
        DataValidationException dve = new DataValidationException("patientId", APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID);
        Mockito.when(mockPrescriptionService.savePrescription(Mockito.any(), Mockito.any(UserPrincipal.class))).thenThrow(new ApplicationException(dve));
        validateSavePrescriptionExceptions("patientId", APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID, IResponseCodes.DATA_VALIDATION_ERROR, request, providerUserPrincipal);
    }

    @Test
    public void testSavePrescription() throws Exception {
        WSSavePrescriptionRequest request = generateRequestObject("SavePrescription", WSSavePrescriptionRequest.class, WS_REQUESTS_FILE);
        PrescriptionInformationVO informationVO = generateRequestObject("PrescriptionInformationVO", PrescriptionInformationVO.class, SERVICE_RESPONSE_FILE);
        Mockito.when(mockPrescriptionService.savePrescription(Mockito.any(), Mockito.any())).thenReturn(Stream.of(informationVO).collect(Collectors.toList()));
        ResponseEntity<RestResponse> responseEntity = prescriptionController.savePrescription(request, providerUserPrincipal);
        WSGetPrescriptionsResponse response = (WSGetPrescriptionsResponse) responseEntity.getBody();
        assertNotNull(response);
        assertEquals(IResponseCodes.SUCCESSFUL, response.getResult());
    }

    private void validateGetPrescriptionsExceptions(String fieldName, String errorKey, String result, String patientId, UserPrincipal userPrincipal, Integer limit, Integer offset) {
        try {
            prescriptionController.getPatientPrescriptions(patientId, userPrincipal, limit, offset);
        } catch (RestApiException e) {
            assertNotNull(e.getFaultInfo());
            RestFault restFault = e.getFaultInfo();
            assertEquals(fieldName, restFault.getFieldName());
            assertEquals(errorKey, restFault.getErrorKey());
            assertEquals(result, restFault.getResult());
        }
    }

    private void validateGetPrescriptionExceptions(String fieldName, String errorKey, String result, Long prescriptionId, UserPrincipal userPrincipal) {
        try {
            prescriptionController.getPrescription(prescriptionId, userPrincipal);
        } catch (RestApiException e) {
            assertNotNull(e.getFaultInfo());
            RestFault restFault = e.getFaultInfo();
            assertEquals(fieldName, restFault.getFieldName());
            assertEquals(errorKey, restFault.getErrorKey());
            assertEquals(result, restFault.getResult());
        }
    }

    private void validateSavePrescriptionExceptions(String fieldName, String errorKey, String result, WSSavePrescriptionRequest request, UserPrincipal userPrincipal) {
        try {
            prescriptionController.savePrescription(request, userPrincipal);
        } catch (RestApiException e) {
            assertNotNull(e.getFaultInfo());
            RestFault restFault = e.getFaultInfo();
            assertEquals(fieldName, restFault.getFieldName());
            assertEquals(errorKey, restFault.getErrorKey());
            assertEquals(result, restFault.getResult());
        }
    }


}
