package com.sg.dayamed.rest.web;

import com.sg.dayamed.commons.component.CommonContext;
import com.sg.dayamed.persistence.repository.ErrorLogRepository;
import com.sg.dayamed.util.BaseTestCase;
import org.mockito.Mock;
import org.springframework.context.MessageSource;

/**
 * Created by Eresh Gorantla on 17/Apr/2019
 **/

public class BaseRestImplTest extends BaseTestCase {

    @Mock
    private ErrorLogRepository mockErrorLogRepository;

    @Mock
    private CommonContext mockCommonContext;

    @Mock
    private MessageSource mockMessageSource;

    public BaseRestImplTest() {
        mockCryptoUtilEncryptIdField();
    }


}
