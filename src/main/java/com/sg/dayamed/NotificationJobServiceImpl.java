package com.sg.dayamed;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.domain.PrescriptionDetailsHistory;
import com.sg.dayamed.persistence.domain.PrescriptionHistory;
import com.sg.dayamed.persistence.domain.PrescriptionHistoryDetails;
import com.sg.dayamed.persistence.domain.view.NotificationData;
import com.sg.dayamed.persistence.domain.view.NotificationProcessingV;
import com.sg.dayamed.persistence.repository.NotificationProcessingVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailsHistoryRepository;
import com.sg.dayamed.util.NotificationType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 20/Feb/2019
 **/
@Service
public class NotificationJobServiceImpl implements NotificationJobService {

    @Autowired
    NotificationProcessingVRepository processingVRepository;

    @Autowired
    @Qualifier("emailNotifications")
    ExecutorService emailNotifications;

    @Autowired
    @Qualifier("smsNotifications")
    ExecutorService smsNotifications;

    @Autowired
    PrescriptionDetailsHistoryRepository prescriptionDetailsHistoryRepository;

    @Override
    public void processPrescriptionNotifications() throws ApplicationException {
        try {
            List<NotificationProcessingV> notificationProcessingData = processingVRepository.findByTriggerTimeAndNpCreatedDateBetween(Date.from(ZonedDateTime.now().toInstant()), ZonedDateTime.now().with(LocalTime.MIN), ZonedDateTime.now().with(LocalTime.MAX));
            List<NotificationProcessingV> emailNotificationData = notificationProcessingData.stream().filter(data -> data.getNotificationTypeId().equals(NotificationType.EMAIL.getId())).collect(Collectors.toList());
            processEmailNotifications(emailNotificationData);

            List<NotificationProcessingV> smsNotificationData = notificationProcessingData.stream().filter(data -> data.getNotificationTypeId().equals(NotificationType.SMS.getId())).collect(Collectors.toList());
            processSmsNotifications(smsNotificationData);

        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private void processEmailNotifications(List<NotificationProcessingV> emailNotificationsData) {
        AtomicInteger count = new AtomicInteger();
        Integer size = 500;
        Collection<List<NotificationProcessingV>> emailAsyncCollection = emailNotificationsData.stream().collect(Collectors.groupingBy(it -> count.getAndIncrement() / size)).values();
        for (Collection emailData : emailAsyncCollection) {
            CompletableFuture.runAsync(() -> processEmailsAsynchronously(new ArrayList<NotificationProcessingV>(emailData)), emailNotifications);
        }
    }

    private void processEmailsAsynchronously(List<NotificationProcessingV> emailData) {
        // Collect and send notifications based on status. If status is 0 ie., initialized then that means No Action, Other than that if any
        // other status is sent, the same is sent to concerned parties.
    }

    private void process(List<NotificationProcessingV> processingVS) {
        List<Long> prescriptionIds = processingVS.stream().map(NotificationProcessingV::getPrescriptionId).distinct().collect(Collectors.toList());
        List<PrescriptionDetailsHistory> detailsHistories = prescriptionDetailsHistoryRepository.findByPrescriptionIdIn(prescriptionIds);
        for (Long prescriptionId : prescriptionIds) {
            List<NotificationProcessingV> notificationProcessingVS =
                    processingVS.stream().filter(processing -> processing.getPrescriptionId().equals(prescriptionId)).collect(Collectors.toList());
            List<PrescriptionDetailsHistory> prescriptionDetailsHistories = detailsHistories.stream().filter(history -> history.getPrescriptionId().equals(prescriptionId)).collect(Collectors.toList());
        }
    }

    private PrescriptionDetailsHistory generatePrescriptionDetailsHistory(List<NotificationProcessingV> processingVS, List<PrescriptionDetailsHistory> detailsHistories) {
        if (CollectionUtils.isEmpty(detailsHistories)) {
            // new entry
            return generatePrescriptionDetails(processingVS, null);
        }
        List<Long> prescriptionInfoIds = processingVS.stream().map(NotificationProcessingV::getPrescriptionInfoId).distinct().collect(Collectors.toList());
        for (Long prescriptionInfoId : prescriptionInfoIds) {
            PrescriptionDetailsHistory detailHistory =
                    detailsHistories.stream().filter(detail -> detail.getPrescriptionInfoId().equals(prescriptionInfoId)).findAny().orElse(null);
            if (detailHistory == null) {
                List<NotificationProcessingV> processingVList = processingVS.stream().filter(data -> data.getPrescriptionInfoId().equals(prescriptionInfoId)).collect(Collectors.toList());
                return generatePrescriptionDetails(processingVList, null);
            } else {

            }
        }
        return null;
    }

    private PrescriptionDetailsHistory generatePrescriptionDetails(List<NotificationProcessingV> processingVS, PrescriptionDetailsHistory prescriptionDetailsHistory) {
        NotificationProcessingV processingV = processingVS.get(0);
        if (prescriptionDetailsHistory == null) {
            prescriptionDetailsHistory = new PrescriptionDetailsHistory();
            if (processingV != null) {
                prescriptionDetailsHistory.setPatientName(processingV.getPatientName());
                prescriptionDetailsHistory.setPatientMobileNumber(processingV.getPatientMobileNumber());
                prescriptionDetailsHistory.setPatientId(processingV.getPatientId());
                prescriptionDetailsHistory.setPatientEmailId(processingV.getPatientEmailId());
                prescriptionDetailsHistory.setPrescriptionId(processingV.getPrescriptionId());
                prescriptionDetailsHistory.setPrescriptionInfoId(processingV.getPrescriptionInfoId());
                List<PrescriptionHistoryDetails> historyDetails = processingVS.stream().map(this::generatePrescriptionHistory).collect(Collectors.toList());
                PrescriptionHistory prescriptionHistory = new PrescriptionHistory();
                prescriptionHistory.setDetails(historyDetails);
                prescriptionDetailsHistory.setHistory(prescriptionHistory);
                return prescriptionDetailsHistory;
            }
        }
        return null;
    }

    private PrescriptionHistoryDetails generatePrescriptionHistory(NotificationProcessingV processingV) {
        PrescriptionHistoryDetails prescriptionHistoryDetails = new PrescriptionHistoryDetails();
        prescriptionHistoryDetails.setConsumptionDate(processingV.getActualDate());
        //prescriptionHistoryDetails.setObservedTime(processingV.getObservedTime());
        prescriptionHistoryDetails.setStatus(processingV.getPdpStatusId());
        prescriptionHistoryDetails.setZoneId(processingV.getZoneId());
        //prescriptionHistoryDetails.setPrescribedTime(processingV.getPrescribedTime());
        NotificationData notificationData = generateNotificationData(processingV);
        prescriptionHistoryDetails.setNotificationData(notificationData);
        return prescriptionHistoryDetails;
    }

    private NotificationData generateNotificationData(NotificationProcessingV notificationProcessingV) {
        NotificationData notificationData = new NotificationData();
        notificationData.setNotificationType(notificationProcessingV.getNotificationType());
        notificationData.setRecipients(StringUtils.isNotBlank(notificationProcessingV.getRecipients()) ?
                Arrays.stream(notificationProcessingV.getRecipients().split(",")).collect(Collectors.toList()) : null);
        return notificationData;
    }

    private void processSmsNotifications(List<NotificationProcessingV> smsNotificationData) {
        AtomicInteger count = new AtomicInteger();
        Integer size = 500;
        Collection<List<NotificationProcessingV>> smsAsyncCollection = smsNotificationData.stream().collect(Collectors.groupingBy(it -> count.getAndIncrement() / size)).values();
        for (Collection smsData : smsAsyncCollection) {
            CompletableFuture.runAsync(() -> processSmsAsynchronously(new ArrayList<NotificationProcessingV>(smsData)), smsNotifications);
        }
    }

    private void processSmsAsynchronously(List<NotificationProcessingV> smsData) {
        // Collect and send notifications based on status. If status is 0 ie., initialized then that means No Action, Other than that if any
        // other status is sent, the same is sent to concerned parties.
    }
}
