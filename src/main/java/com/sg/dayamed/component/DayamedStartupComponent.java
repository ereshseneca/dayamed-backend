package com.sg.dayamed.component;

import com.sg.dayamed.commons.exception.ApplicationException;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 29/Mar/2019
 **/
@Component
public class DayamedStartupComponent {

    public void saveDefaultUsers() throws ApplicationException {
        try {

        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }
}
