package com.sg.dayamed.jobs;

/**
 * Created by Eresh Gorantla on 09/Mar/2019
 **/
@FunctionalInterface
public interface IJobService {
    void execute() throws Exception;
}
