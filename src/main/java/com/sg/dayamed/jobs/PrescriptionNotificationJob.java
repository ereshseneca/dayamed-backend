package com.sg.dayamed.jobs;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.repository.SchedulerJobsRepository;
import com.sg.dayamed.prescription.PrescriptionNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 11/Mar/2019
 **/
@Component
public class PrescriptionNotificationJob extends BaseSchedulerJobs {

    @Autowired
    SchedulerJobsRepository schedulerJobsRepository;

    @Autowired
    PrescriptionNotificationService prescriptionNotificationService;

    private String JOB_NAME = "PrescriptionNotificationJob";

    @Override
    public SchedulerJobsRepository getSchedulerRepository() {
        return schedulerJobsRepository;
    }

    public void persistPrescriptionNotifications() throws ApplicationException {
        processJob(JOB_NAME , () -> {
           prescriptionNotificationService.persistNotificationData(null);
        });
    }
}
