package com.sg.dayamed.jobs;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.repository.SchedulerJobsRepository;
import com.sg.dayamed.prescription.PrescriptionDetailsProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 09/Mar/2019
 **/
@Component
public class PrescriptionDetailsJob extends BaseSchedulerJobs {

    @Autowired
    SchedulerJobsRepository schedulerJobsRepository;

    @Autowired
    PrescriptionDetailsProcessingService processingService;

    private String JOB_NAME = "PrescriptionDetailsJob";

    @Override
    public SchedulerJobsRepository getSchedulerRepository() {
        return schedulerJobsRepository;
    }

    public void processPrescriptionDetails() throws ApplicationException {
        processJob(JOB_NAME, () -> {
            processingService.persistPrescriptionDetails();
        });

    }

    public String getJobName() {
        return JOB_NAME;
    }
 }
