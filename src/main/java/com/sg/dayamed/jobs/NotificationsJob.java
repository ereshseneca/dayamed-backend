package com.sg.dayamed.jobs;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.repository.SchedulerJobsRepository;
import com.sg.dayamed.NotificationJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Component
public class NotificationsJob extends BaseSchedulerJobs {

    @Autowired
    SchedulerJobsRepository schedulerJobsRepository;

    @Autowired
    NotificationJobService notificationJobService;

    private String JOB_NAME = "NotificationsJob";

    @Override
    public SchedulerJobsRepository getSchedulerRepository() {
        return schedulerJobsRepository;
    }

    public void processNotifications() throws ApplicationException {
        processJob(JOB_NAME, () -> {
            notificationJobService.processPrescriptionNotifications();
        });
    }



}
