package com.sg.dayamed.jobs;

import com.sg.dayamed.persistence.domain.SchedulerJobs;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Duration;
import java.time.Instant;

/**
 * Created by Eresh Gorantla on 09/Mar/2019
 **/
public abstract class BaseSchedulerJobs {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    String SUCCESS = "Success";
    String FAILURE = "Failure";

    protected void processJob(String jobName, IJobService service) {
        Instant start = Instant.now();
        String errorStacktrace = null;
        String status = null;
        try {
            service.execute();
            status = SUCCESS;
        } catch (Exception e) {
            logger.error("Error while processing Scheduling Job" + jobName, e);
            errorStacktrace = ExceptionUtils.getStackTrace(e);
            status = FAILURE;
        } finally {
            Instant end = Instant.now();
            Duration interval = Duration.between(start, end);
            SchedulerJobs schedulerJobs = generateSchedulerJobs(jobName, errorStacktrace, status, interval.toMillis());
            if (getSchedulerRepository() != null) {
                getSchedulerRepository().save(schedulerJobs);
            }
        }
    }

    protected abstract JpaRepository getSchedulerRepository();

    private SchedulerJobs generateSchedulerJobs(String jobName, String errorStacktrace, String status, Long processingTime) {
        SchedulerJobs schedulerJobs = new SchedulerJobs();
        schedulerJobs.setJobName(jobName);
        schedulerJobs.setErrorStacktrace(errorStacktrace);
        schedulerJobs.setProcessingTime(processingTime);
        schedulerJobs.setStatus(status);
        return schedulerJobs;
    }
}
