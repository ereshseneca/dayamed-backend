package com.sg.dayamed.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by Eresh Gorantla on 11/Mar/2019
 **/
@Configuration
@EnableScheduling
public class DayaMedSchedulingConfiguration implements SchedulingConfigurer {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    PrescriptionDetailsJob prescriptionDetailsJob;

    @Autowired
    PrescriptionNotificationJob prescriptionNotificationJob;

    @Autowired
    NotificationsJob notificationJob;

    @Bean(destroyMethod = "shutdown")
    public Executor taskExecutor() {
        return Executors.newScheduledThreadPool(20);
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskExecutor());
        taskRegistrar.addTriggerTask(() -> {
            try {
                prescriptionDetailsJob.processPrescriptionDetails();
            } catch (Exception e) {
                logger.error("Error while executing PrescriptionDetailsJob -->",  e);
            }
        }, triggerContext -> {
            String cronExp = "0 0 23 ? * *";
            return new CronTrigger(cronExp).nextExecutionTime(triggerContext);
        });
        taskRegistrar.addTriggerTask(() -> {
            try {
                prescriptionNotificationJob.persistPrescriptionNotifications();
            } catch (Exception e) {
                logger.error("Error while executing PrescriptionNotificationJob -->",  e);
            }
        }, triggerContext -> {
            String cronExp = "0 31 23 ? * *";
            return new CronTrigger(cronExp).nextExecutionTime(triggerContext);
        });
        /*taskRegistrar.addTriggerTask(() -> {
            try {
                notificationJob.processNotifications();
            } catch (Exception e) {
                logger.error("Error while executing NotificationsJob -->",  e);
            }
        }, triggerContext -> {
            String cronExp = "0 * * ? * *";
            return new CronTrigger(cronExp).nextExecutionTime(triggerContext);
        });*/
    }
}
