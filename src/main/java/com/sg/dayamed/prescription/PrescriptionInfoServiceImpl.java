package com.sg.dayamed.prescription;

import com.sg.dayamed.commons.constants.IGlobalErrorKeys;
import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.commons.util.ValidationUtils;
import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.handlers.FrequencyHandler;
import com.sg.dayamed.handlers.FrequencyHandlerFactory;
import com.sg.dayamed.handlers.vo.PrescriptionInfoHandlerData;
import com.sg.dayamed.persistence.domain.FrequencyDetails;
import com.sg.dayamed.persistence.domain.PrescriptionDetailsProcessing;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import com.sg.dayamed.persistence.domain.PrescriptionNotifications;
import com.sg.dayamed.persistence.domain.view.NotificationSubscriptionDetailsV;
import com.sg.dayamed.persistence.domain.view.PatientAssociationContactDetailsV;
import com.sg.dayamed.persistence.domain.view.PresNotificationInformationV;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import com.sg.dayamed.persistence.domain.view.PrescriptionInfoV;
import com.sg.dayamed.persistence.repository.NotificationSubscriptionDetailsVRepository;
import com.sg.dayamed.persistence.repository.NotificationTypeRepository;
import com.sg.dayamed.persistence.repository.PatientAssociationContactDetailsVRepository;
import com.sg.dayamed.persistence.repository.PresNotificationInformationVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailInfoVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailsProcessingRepository;
import com.sg.dayamed.persistence.repository.PrescriptionInfoRepository;
import com.sg.dayamed.persistence.repository.PrescriptionInfoVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionNotificationsRepository;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.prescription.vo.PrescriptionDetailInfoVO;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.AdherenceStatus;
import com.sg.dayamed.util.NotificationType;
import com.sg.dayamed.validation.IServiceFieldNames;
import com.sg.dayamed.vo.NotificationSubscriptionDetailsVO;
import com.sg.dayamed.vo.StartPrescriptionVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Service
public class PrescriptionInfoServiceImpl implements PrescriptionInfoService {

    @Autowired
    PrescriptionDetailsProcessingRepository processingRepository;

    @Autowired
    PrescriptionDetailInfoVRepository prescriptionDetailInfoVRepository;

    @Autowired
    PrescriptionInfoVRepository prescriptionInfoVRepository;

    @Autowired
    PrescriptionInfoRepository prescriptionInfoRepository;

    @Autowired
    NotificationSubscriptionDetailsVRepository subscriptionDetailsVRepository;

    @Autowired
    PatientAssociationContactDetailsVRepository associationContactDetailsVRepository;

    @Autowired
    NotificationTypeRepository notificationTypeRepository;

    @Autowired
    PrescriptionNotificationsRepository notificationsRepository;

    @Autowired
    PresNotificationInformationVRepository notificationInformationVRepository;

    @Autowired
    PrescriptionDetailInfoVRepository detailInfoVRepository;

    @Autowired
    PrescriptionDetailsProcessingService detailsProcessingService;

    @Autowired
    PrescriptionNotificationService prescriptionNotificationService;

    @Autowired
    FrequencyHandlerFactory frequencyHandlerFactory;


    @Override
    public void addPrescriptionData() throws ApplicationException {
        try {
            List<PrescriptionDetailInfoV> detailInfoVS = prescriptionDetailInfoVRepository.findAllByActivePrescriptionInfoAndPresEndDateGreaterThanEqual(true, ZonedDateTime.now());
            processingRepository.saveAll(generatePrescriptionDetailsProcessings(detailInfoVS));
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    @Transactional
    public void startPrescription(StartPrescriptionVO startPrescriptionVO, UserPrincipal userPrincipal) throws ApplicationException {
        try {
            List<PrescriptionInfoV> prescriptionInfoVS = validateStartPrescription(startPrescriptionVO, userPrincipal);
            List<Long> prescriptionInfoIds = prescriptionInfoVS.stream().map(PrescriptionInfoV::getPrescriptionInfoId).distinct().collect(Collectors.toList());
            List<PrescriptionInfo> prescriptionInfoS = prescriptionInfoRepository.findAllById(prescriptionInfoIds);
            prescriptionInfoS = prescriptionInfoS.stream().map(pres -> {
                    try {
                        return modifyPrescriptionInfo(pres, startPrescriptionVO);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
            }).collect(Collectors.toList());
            prescriptionInfoS = prescriptionInfoRepository.saveAll(prescriptionInfoS);
            PatientAssociationContactDetailsV contactDetailsV = associationContactDetailsVRepository.findByPatientId(userPrincipal.getId());
            List<PrescriptionNotifications> prescriptionNotifications = prescriptionInfoS.stream().flatMap(info -> generatePrescriptionNotifications(info, true, contactDetailsV).stream()).collect(Collectors.toList());
            notificationsRepository.saveAll(prescriptionNotifications);
            List<PrescriptionDetailsProcessing> detailsProcessingS = savePrescriptionDetailsProcessing(prescriptionInfoIds, startPrescriptionVO.getIsUpdated());
            prescriptionNotificationService.persistNotificationData(detailsProcessingS);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private List<PrescriptionDetailsProcessing> savePrescriptionDetailsProcessing(List<Long> prescriptionInfoIds, Boolean isUpdate) throws ApplicationException {
        if (isUpdate) {
            List<PrescriptionDetailsProcessing> detailsProcessingS = detailsProcessingService.getPrescriptionInfoSForCreatedDate(prescriptionInfoIds, ZonedDateTime.now());
            List<String> processingIds = detailsProcessingS.stream().map(PrescriptionDetailsProcessing::getId).collect(Collectors.toList());
            prescriptionNotificationService.deleteByPrescriptionProcessingIds(processingIds);
        }
        List<PrescriptionDetailInfoV> prescriptionDetailInfoVS = prescriptionDetailInfoVRepository.findByPrescriptionInfoIdIn(prescriptionInfoIds);
        return detailsProcessingService.persistPrescriptionDetails(prescriptionDetailInfoVS);
    }

    private List<PrescriptionInfoV> validateStartPrescription(StartPrescriptionVO startPrescriptionVO, UserPrincipal userPrincipal) throws DataValidationException {
        List<IdIndexVO> presIdIndexes = startPrescriptionVO.getPresIdIndexes();
        if (CollectionUtils.isEmpty(presIdIndexes)) {
            throw new DataValidationException(IServiceFieldNames.PRESCRIPTION_IDS, IGlobalErrorKeys.ERRORS_GENERAL_VALUE_REQUIRED);
        }
        List<Long> requestedIds = startPrescriptionVO.getPrescriptionIds();
        List<PrescriptionInfoV> prescriptions = prescriptionInfoVRepository.findByPrescriptionIdIn(requestedIds);
        List<Long> prescriptionIds = prescriptions.stream().map(PrescriptionInfoV::getPrescriptionId).distinct().collect(Collectors.toList());
        Long invalidPrescriptionId = requestedIds.stream().filter(pres -> !prescriptionIds.contains(pres)).findFirst().orElse(null);
        if (invalidPrescriptionId != null) {
            throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.START_PRESCRIPTION_PRESCRIPTION_IDS, true, ValidationUtils.getIndex(presIdIndexes, invalidPrescriptionId)), APIErrorKeys.ERROR_PRESCRIPTION_ID_NOT_FOUND);
        }
        Long userId = userPrincipal.getId();
        PrescriptionInfoV prescriptionInfoV = prescriptions.stream().filter(pres -> !userId.equals(pres.getUserId())).findAny().orElse(null);
        if (prescriptionInfoV != null) {
            throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.START_PRESCRIPTION_PRESCRIPTION_IDS, true, ValidationUtils.getIndex(presIdIndexes, prescriptionInfoV.getPrescriptionId())), APIErrorKeys.ERROR_PRESCRIPTION_ID_DOES_NOT_BELONG_TO_REQUESTED_PATIENT);
        }
        return prescriptions;
    }

    private List<PrescriptionNotifications> generatePrescriptionNotifications(PrescriptionInfo prescriptionInfo, Boolean defaultFlag, PatientAssociationContactDetailsV contactDetailsV) {
        List<PrescriptionNotifications> prescriptionNotifications = new ArrayList<>();
        List<PresNotificationInformationV> notificationInformationVS = notificationInformationVRepository.findAllByPrescriptionInfoIdAndPresEndDateGreaterThanEqual(prescriptionInfo.getId(), ZonedDateTime.now());
        if (CollectionUtils.isEmpty(notificationInformationVS)) {
            // This will be in the cache
            List<NotificationSubscriptionDetailsV> subscriptionDetailsVS = subscriptionDetailsVRepository.findByDefaultFlag(true);
            List<NotificationSubscriptionDetailsVO> subscriptionDetailsVOS = subscriptionDetailsVS.stream().map(NotificationSubscriptionDetailsVO::new).collect(Collectors.toList());
            List<NotificationSubscriptionDetailsVO> detailsVOS = new ArrayList<>();
            prescriptionNotifications.addAll(generatePrescriptionNotifications(subscriptionDetailsVOS, prescriptionInfo, defaultFlag, contactDetailsV));
        }
        return prescriptionNotifications;
    }

    private List<PrescriptionNotifications> generatePrescriptionNotifications(List<NotificationSubscriptionDetailsVO> subscriptionDetailsVOS, PrescriptionInfo prescriptionInfo, Boolean defaultFlag, PatientAssociationContactDetailsV contactDetailsV) {
        List<PrescriptionNotifications> notifications = subscriptionDetailsVOS.stream().map(details -> mapPrescriptionNotifications(details, contactDetailsV, prescriptionInfo, defaultFlag)).collect(Collectors.toList());
        return notifications;
    }

    private PrescriptionNotifications mapPrescriptionNotifications(NotificationSubscriptionDetailsVO subscriptionDetailsVO, PatientAssociationContactDetailsV contactDetailsV, PrescriptionInfo prescriptionInfo, Boolean defaultFlag) {
        PrescriptionNotifications notifications = new PrescriptionNotifications();
        Optional<com.sg.dayamed.persistence.domain.NotificationType> type = notificationTypeRepository.findById(subscriptionDetailsVO.getNotificationTypeId());
        notifications.setNotificationType(type.get());
        notifications.setDefaultFlag(defaultFlag);
        notifications.setDelayInMinutes(subscriptionDetailsVO.getNotificationDelay());
        notifications.setRecipients(contactDetailsV.getEmailIds());
        Optional<PrescriptionInfo> prescriptionInfoOptional = prescriptionInfoRepository.findById(prescriptionInfo.getId());
        notifications.setPrescriptionInfo(prescriptionInfoOptional.get());
        return notifications;
    }


    private List<NotificationSubscriptionDetailsVO> getNotificationSubscriptionDetailsVOS(List<NotificationSubscriptionDetailsVO> subscriptionDetailsVOS, NotificationType notificationType) {
        return subscriptionDetailsVOS.stream().filter(details -> details.getNotificationTypeId().equals(notificationType.getId())).sorted(Comparator.comparing(NotificationSubscriptionDetailsVO::getDefaultFlag, Comparator.nullsLast(Comparator.naturalOrder()))).collect(Collectors.toList());
    }

    private List<PrescriptionDetailsProcessing> generatePrescriptionDetailsProcessings(List<PrescriptionDetailInfoV> detailInfoVS) {
        List<PrescriptionDetailsProcessing> detailsProcessings = new ArrayList<>();
        return detailInfoVS.stream().map(detail -> mapPrescriptionDetailsProcessing(detail)).collect(Collectors.toList());
    }

    private PrescriptionDetailsProcessing mapPrescriptionDetailsProcessing(PrescriptionDetailInfoV detailInfoV) {
        PrescriptionDetailsProcessing detailsProcessing = new PrescriptionDetailsProcessing();
        detailsProcessing.setStatusId(AdherenceStatus.INITIALIZE.getId());
        detailsProcessing.setStatus(AdherenceStatus.INITIALIZE.getValue());
        // detailsProcessing.setMedicineName(detailInfoV.getMedicineName());
        detailsProcessing.setPatientMobileNumber(detailInfoV.getPatientMobileNumber());
        detailsProcessing.setPatientEmailId(detailInfoV.getPatientEmailId());
        //detailsProcessing.setPrescribedTime(detailInfoV.getPrescribedTime());
        detailsProcessing.setPatientId(detailInfoV.getPatientId());
        detailsProcessing.setActualAttempts(0);
        detailsProcessing.setPatientName(detailInfoV.getPatientName());
        return detailsProcessing;
    }

    private List<PrescriptionInfoV> validatePrescriptionDetails(PrescriptionDetailInfoVO detailInfo) throws DataValidationException {
        List<PrescriptionInfoV> prescriptionInfoVS = prescriptionInfoVRepository.findByPrescriptionIdAndUserId(detailInfo.getPrescriptionId(), detailInfo.getUserId());
        if (CollectionUtils.isEmpty(prescriptionInfoVS)) {
            throw new DataValidationException("prescriptionId", APIErrorKeys.ERROR_PATIENT_PRESCRIPTION_DETAILS_NOT_FOUND);
        }
        List<PrescriptionDetailInfoV> detailInfoS = detailInfoVRepository.findByPatientIdAndPrescriptionIdAndPresEndDateGreaterThan(detailInfo.getUserId(), detailInfo.getPrescriptionId(), ZonedDateTime.now());
        if (CollectionUtils.isNotEmpty(detailInfoS)) {
            throw new DataValidationException("prescriptionId", APIErrorKeys.ERROR_PATIENT_PRESCRIPTION_IS_ALREADY_ACTIVE);
        }
        return prescriptionInfoVS;
    }

    private PrescriptionInfo modifyPrescriptionInfo(PrescriptionInfo prescriptionInfo, StartPrescriptionVO startPrescriptionVO) throws ApplicationException {
        try {
            FrequencyDetails details = prescriptionInfo.getFrequency();
            FrequencyHandler frequencyHandler = frequencyHandlerFactory.createFrequencyHandler(details.getTypeId());
            PrescriptionInfoHandlerData handlerData = frequencyHandler.getPrescriptionInfoHandlerData(prescriptionInfo, startPrescriptionVO.getForceStart());
            prescriptionInfo.setStartDate(handlerData.getStartDate());
            prescriptionInfo.setEndDate(handlerData.getEndDate());
            return prescriptionInfo;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private PrescriptionInfo modifyPrescriptionInfoDates(PrescriptionInfo prescriptionInfo, StartPrescriptionVO startPrescriptionVO) {
        String zoneId = StringUtils.isNotBlank(startPrescriptionVO.getZoneId()) ? startPrescriptionVO.getZoneId() : ZoneId.systemDefault().getId();
        if (StringUtils.isNotBlank(zoneId)) {
            prescriptionInfo.setStartDate(ZonedDateTime.now(ZoneId.of(zoneId)));
            prescriptionInfo.setEndDate(ZonedDateTime.now(ZoneId.of(zoneId)).plusDays(new Long(prescriptionInfo.getCourseDuration())));
            prescriptionInfo.setZoneId(zoneId);
        }
        return prescriptionInfo;
    }

    /*private List<PrescriptionDetailsProcessing> generatePrescriptionInfoEvenHour(List<ActiveUserPrescriptionDetailsV> detailsVS) throws ApplicationException {
        try {
            List<PrescriptionDetailsProcessing> infoEvenHours = new ArrayList<PrescriptionDetailsProcessing>();
            infoEvenHours = detailsVS.stream().map(detail -> this.mapPrescriptionInfoEvenHour(detail)).collect(Collectors.toList());
            return infoEvenHours;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }*/

    private List<PrescriptionDetailsProcessing> generatePrescriptionDetailsProcessings() {
        return null;
    }

    private Integer generateNextHour(Integer hour) {
        if (hour == 23) {
            return 0;
        }
        return hour + 1;
    }

    private PrescriptionDetailsProcessing mapPrescriptionDetailsProcessingRandom() {
        PrescriptionDetailsProcessing detailsProcessing = new PrescriptionDetailsProcessing();
        detailsProcessing.setStatusId(AdherenceStatus.INITIALIZE.getId());
        detailsProcessing.setStatus(AdherenceStatus.INITIALIZE.getValue());
        //detailsProcessing.setMedicineName(RandomStringUtils.randomAlphabetic(20));
        detailsProcessing.setPatientMobileNumber(generateRandomMobileNumber());
        detailsProcessing.setPatientEmailId(RandomStringUtils.randomAlphabetic(15) + "@gmail.com");
        ZonedDateTime dateTime = ZonedDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
        String currentTime = dateTime.format(dateTimeFormatter);
        String currentHour = StringUtils.substring(currentTime, 0, StringUtils.indexOf(currentTime, ":"));
        detailsProcessing.setPrescribedTime(generateRandomPrescribedTime(currentHour));
        detailsProcessing.setPatientId(new Random().nextLong());
        detailsProcessing.setActualAttempts(0);
        detailsProcessing.setPatientName(RandomStringUtils.randomAlphabetic(25));
        return detailsProcessing;
    }


    public void generateData() throws ApplicationException {
        List<PrescriptionDetailsProcessing> infoEvenHours = new ArrayList<PrescriptionDetailsProcessing>();
        infoEvenHours = IntStream.range(0, 1000).mapToObj(index -> mapPrescriptionDetailsProcessingRandom()).collect(Collectors.toList());
        processingRepository.saveAll(infoEvenHours);
    }

    private Date addMinutesToDate(String prescribedTime, Integer minutesToAdd) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String currentDate = formatter.format(ZonedDateTime.now());
        String prescribedDateTime = currentDate + " " + prescribedTime;
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(prescribedDateTime, timeFormatter);
        LocalDateTime localDateTime = dateTime.plusMinutes(minutesToAdd.longValue());
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private Date generateRandomPrescribedTime(String currentHour) {
        Random random = new Random();
        Integer randomMin = random.nextInt(59);
        String minString = randomMin.toString();
        minString = minString.length() == 1 ? "0" + minString : minString;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String date = formatter.format(ZonedDateTime.now());
        String dateTime1 = date + " " + currentHour + ":" + minString;
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime zonedDateTime = LocalDateTime.parse(dateTime1, timeFormatter);
        Date randomTime = Date.from(zonedDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return randomTime;
    }

    private Date generateNotificationTime(String prescribedTime, NotificationType notificationType) {
        String prescribedHours = StringUtils.substring(prescribedTime, 0, StringUtils.indexOf(prescribedTime, ":"));
        String prescribedMinutes = StringUtils.substring(prescribedTime, StringUtils.indexOf(prescribedTime, ":") + 1, StringUtils.length(prescribedTime));
        Integer prescribedMinutesValue = StringUtils.isNotBlank(prescribedMinutes) ? NumberUtils.toInt(prescribedMinutes) : null;
        if (prescribedMinutes != null) {
            if (StringUtils.equalsIgnoreCase(notificationType.getValue(), NotificationType.EMAIL.getValue())) {
                Integer emailTime = getRandomMinutes();
                return addMinutesToDate(prescribedTime, emailTime);
            }
            if (StringUtils.equalsIgnoreCase(notificationType.getValue(), NotificationType.SMS.getValue())) {
                Integer smsTime = getRandomMinutes();
                return addMinutesToDate(prescribedTime, smsTime);
            }
            if (StringUtils.equalsIgnoreCase(notificationType.getValue(), NotificationType.VIDEO_CALL.getValue())) {
                Integer videoTime = getRandomMinutes();
                return addMinutesToDate(prescribedTime, videoTime);
            }
        }
        return null;
    }

    private String generateRandomTime(Integer hour) {
        ZonedDateTime dateTime = ZonedDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
        String currentTime = dateTime.format(dateTimeFormatter);
        String currentHour = StringUtils.substring(currentTime, 0, StringUtils.indexOf(currentTime, ":"));
        Integer currentHourValue = hour != null ? hour : NumberUtils.toInt(currentHour);
        Integer minValue = NumberUtils.toInt(currentHourValue + "00");
        Integer maxValue = NumberUtils.toInt(currentHourValue + "59");
        Random random = new Random();
        Integer result = random.nextInt(maxValue - minValue) + minValue;
        String resultString = String.valueOf(result);
        return StringUtils.substring(resultString, 0, StringUtils.length(resultString) - 2) + ":" + StringUtils.substring(resultString, StringUtils.length(resultString) - 2, StringUtils.length(resultString));
    }

    private Integer getRandomMinutes() {
        Integer min = 1;
        Integer max = 60;
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    private String generateRandomMobileNumber() {
        int m = (int) Math.pow(10, 10 - 1);
        int x = m + new Random().nextInt(9 * m);
        return String.valueOf(x);
    }
}
