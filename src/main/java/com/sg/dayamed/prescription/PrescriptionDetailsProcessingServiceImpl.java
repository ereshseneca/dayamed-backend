package com.sg.dayamed.prescription;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.domain.PrescriptionDetailsProcessing;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import com.sg.dayamed.persistence.repository.PrescriptionDetailInfoVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailsProcessingRepository;
import com.sg.dayamed.util.AdherenceStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 26/Feb/2019
 **/
@Service
public class PrescriptionDetailsProcessingServiceImpl implements PrescriptionDetailsProcessingService {

    @Autowired
    PrescriptionDetailInfoVRepository detailInfoVRepository;

    @Autowired
    PrescriptionDetailsProcessingRepository detailsProcessingRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    @Qualifier("prescriptionDetailsProcessing")
    ExecutorService prescriptionDetailsProcessing;


    @Override
    public void persistPrescriptionDetails() throws ApplicationException {
        try {
            List<PrescriptionDetailInfoV> detailInfoVS = detailInfoVRepository.findAllByPresEndDateGreaterThanEqual(ZonedDateTime.now());
            List<PrescriptionDetailsProcessing> detailsProcessingS = detailInfoVS.stream().flatMap(info -> mapPrescriptionDetailsProcessing((info)).stream()).collect(Collectors.toList());
            AtomicInteger count = new AtomicInteger();
            Integer size = 1000;
            Collection<List<PrescriptionDetailsProcessing>> collection = detailsProcessingS.stream().collect(Collectors.groupingBy(it -> count.getAndIncrement() / size)).values();
            for (Collection coll : collection) {
                CompletableFuture.runAsync(() -> persistAsynchronously(new ArrayList<PrescriptionDetailsProcessing>(coll)), prescriptionDetailsProcessing);
            }
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public List<PrescriptionDetailsProcessing> persistPrescriptionDetails(List<PrescriptionDetailInfoV> detailInfoVS) throws ApplicationException {
        try {
            List<PrescriptionDetailsProcessing> detailsProcessingS = detailInfoVS.stream().flatMap(info -> mapPrescriptionDetailsProcessing((info)).stream()).collect(Collectors.toList());
            return detailsProcessingRepository.saveAll(detailsProcessingS);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public List<PrescriptionDetailsProcessing> getPrescriptionInfoSForCreatedDate(List<Long> prescriptionInfoIds, ZonedDateTime createdDate) throws ApplicationException {
        return detailsProcessingRepository.findByPrescriptionInfoIdInAndCreatedDate(prescriptionInfoIds, createdDate);
    }

    private void persistAsynchronously(List<PrescriptionDetailsProcessing> detailsProcessings) {
        detailsProcessingRepository.saveAll(detailsProcessings);
    }

    private List<PrescriptionDetailsProcessing> mapPrescriptionDetailsProcessing(PrescriptionDetailInfoV detailInfoV) {
        //String prescribedTime = detailInfoV.getIntakeTime();
        List<String> times = null;
        /*try {
            PrescriptionTimeVO timeVO = objectMapper.readValue(prescribedTime, PrescriptionTimeVO.class);
            times = timeVO.getTime();
        } catch (Exception e) {

        }*/
        List<PrescriptionDetailsProcessing> detailsProcessingData = times.stream().map(time -> generatePrescriptionDetailsProcessing(detailInfoV, time)).collect(Collectors.toList());
        return detailsProcessingData;
    }

    private PrescriptionDetailsProcessing generatePrescriptionDetailsProcessing(PrescriptionDetailInfoV detailInfoV, String prescribedTime) {
        PrescriptionDetailsProcessing processing = new PrescriptionDetailsProcessing();
        processing.setPatientName(detailInfoV.getPatientName());
        processing.setPatientId(detailInfoV.getPatientId());
        processing.setPrescribedTime(getTime(prescribedTime, detailInfoV.getZoneId()));
        processing.setPatientEmailId(detailInfoV.getPatientEmailId());
        processing.setPatientMobileNumber(detailInfoV.getPatientMobileNumber());
        processing.setPresInfoTypeId(detailInfoV.getPresInfoTypeId());
        //processing.setMedicineName(detailInfoV.getMedicineName());
        processing.setStatus(AdherenceStatus.INITIALIZE.getValue());
        processing.setStatusId(AdherenceStatus.INITIALIZE.getId());
        processing.setActualDate(ZonedDateTime.now());
        processing.setActualTime(prescribedTime);
        processing.setPrescriptionInfoId(detailInfoV.getPrescriptionInfoId());
        return processing;
    }

    private Date getTime(String time, String zone) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String date = formatter.format(ZonedDateTime.now());
        String dateTime = date + " " + time;
        ZoneId zoneId = StringUtils.isNotBlank(zone) ? ZoneId.of(zone) : ZoneId.systemDefault();
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone(zoneId);
        ZonedDateTime zoned = ZonedDateTime.parse(dateTime, timeFormatter);
        zoned = zoned.withZoneSameInstant(ZoneId.systemDefault());
        return Date.from(zoned.toInstant());
    }
}
