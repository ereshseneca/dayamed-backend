package com.sg.dayamed.prescription;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.domain.NotificationProcessing;
import com.sg.dayamed.persistence.domain.PrescriptionDetailsProcessing;
import com.sg.dayamed.persistence.domain.view.PresNotificationInformationV;
import com.sg.dayamed.persistence.repository.NotificationProcessingRepository;
import com.sg.dayamed.persistence.repository.PatientAssociationContactDetailsVRepository;
import com.sg.dayamed.persistence.repository.PresNotificationInformationVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailsProcessingRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 26/Feb/2019
 **/
@Service
public class PrescriptionNotificationServiceImpl implements PrescriptionNotificationService {

    @Autowired
    PrescriptionDetailsProcessingRepository detailsProcessingRepository;

    @Autowired
    PresNotificationInformationVRepository notificationInformationVRepository;

    @Autowired
    PatientAssociationContactDetailsVRepository associationContactDetailsVRepository;

    @Autowired
    NotificationProcessingRepository processingRepository;

    @Autowired
    @Qualifier("prescriptionNotifications")
    ExecutorService prescriptionNotifications;

    @Autowired
    NotificationProcessingRepository notificationProcessingRepository;

    public void persistNotificationData(List<PrescriptionDetailsProcessing> detailsProcessingS) throws ApplicationException {
        try {
            if (CollectionUtils.isEmpty(detailsProcessingS)) {
                detailsProcessingS = detailsProcessingRepository.findByCreatedDateBetween(ZonedDateTime.now().with(LocalTime.MIN), ZonedDateTime.now().with(LocalTime.MAX));
            }
            List<Long> patientIds = detailsProcessingS.stream().map(PrescriptionDetailsProcessing::getPatientId).distinct().collect(Collectors.toList());
            AtomicInteger count = new AtomicInteger();
            Integer size  = 250;
            Collection<List<Long>> patientAsyncCollection = patientIds.stream().collect(Collectors.groupingBy(it -> count.getAndIncrement() / size)).values();
            for (Collection patients : patientAsyncCollection) {
                List<PrescriptionDetailsProcessing> processingData = detailsProcessingS.stream().filter(detail -> patients.contains(detail.getPatientId())).collect(Collectors.toList());
                CompletableFuture.runAsync(() -> persistAsynchronously(processingData, new ArrayList<Long>(patients)), prescriptionNotifications);
            }
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public List<NotificationProcessing> getPrescriptionNotificationsByProcessingIds(List<String> processingIds) throws ApplicationException {
        try {
            return processingRepository.findByPrescriptionProcessingIdIn(processingIds);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public void deleteByPrescriptionProcessingIds(List<String> processingIds) throws ApplicationException {
        List<NotificationProcessing> notificationProcessingS = getPrescriptionNotificationsByProcessingIds(processingIds);
        processingRepository.deleteAll(notificationProcessingS);
    }

    private List<NotificationProcessing> getNotificationProcessingData(List<PrescriptionDetailsProcessing> detailsProcessings, List<PresNotificationInformationV> notificationInformationVS) {
        return detailsProcessings.stream().flatMap(details -> generateNotificationProcessing(notificationInformationVS, details).stream()).collect(Collectors.toList());
    }

    private List<NotificationProcessing> generateNotificationProcessing(List<PresNotificationInformationV> informationVS, PrescriptionDetailsProcessing detailsProcessing) {
        return informationVS.stream().map(info -> mapNotificationProcessing(info, detailsProcessing)).collect(Collectors.toList());
    }

    private NotificationProcessing mapNotificationProcessing(PresNotificationInformationV informationV, PrescriptionDetailsProcessing detailsProcessing) {
        NotificationProcessing processing = new NotificationProcessing();
        processing.setRecipients(informationV.getRecipients());
        processing.setPatientName(informationV.getPatientName());
        processing.setPatientId(informationV.getPatientId());
        processing.setPatientMobileNumber(informationV.getPatientMobileNumber());
        processing.setPatientEmailId(informationV.getPatientEmailId());
        processing.setNotificationTypeId(informationV.getNotificationTypeId());
        processing.setNotificationType(informationV.getNotificationType());
        processing.setMedicineName(informationV.getMedicineName());
        //processing.setMedicineId(informationV.getMedicineId());
        processing.setPrescribedTime(detailsProcessing.getPrescribedTime());
        processing.setTriggerTime(addMinutes(detailsProcessing.getPrescribedTime(), informationV.getDelayInMinutes()));
        processing.setPrescriptionProcessingId(detailsProcessing.getId());
        return  processing;
    }

    private Date addMinutes(Date prescribedTime, Integer minutesToAdd) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String currentDate = formatter.format(ZonedDateTime.now());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        String time = simpleDateFormat.format(prescribedTime);
        String date = currentDate + " " + time;
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, timeFormatter);
        LocalDateTime localDateTime = dateTime.plusMinutes(minutesToAdd.longValue());
        return  Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private void persistAsynchronously(List<PrescriptionDetailsProcessing> detailsProcessingData, List<Long> patientIds) {
        List<PresNotificationInformationV> notificationInformationVS = notificationInformationVRepository.findByPatientIdIn(patientIds);
        List<NotificationProcessing> processingData = patientIds.stream().flatMap(id -> getNotificationProcessingData(detailsProcessingData.stream().filter(detail ->
                detail.getPatientId().equals(id)).collect(Collectors.toList()), notificationInformationVS.stream().filter(info ->
                info.getPatientId().equals(id)).collect(Collectors.toList())).stream()).collect(Collectors.toList());
        processingRepository.saveAll(processingData);
        //updatePrescriptionDetailsProcessing(detailsProcessings);
    }
}
