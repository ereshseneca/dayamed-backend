package com.sg.dayamed.prescription;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.persistence.domain.PrescriptionDetailInfo;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import com.sg.dayamed.persistence.domain.PrescriptionNotifications;
import com.sg.dayamed.persistence.domain.view.NotificationSubscriptionDetailsV;
import com.sg.dayamed.persistence.domain.view.PatientAssociationContactDetailsV;
import com.sg.dayamed.persistence.domain.view.PresNotificationInformationV;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import com.sg.dayamed.persistence.domain.view.PrescriptionInfoV;
import com.sg.dayamed.persistence.repository.NotificationSubscriptionDetailsVRepository;
import com.sg.dayamed.persistence.repository.NotificationTypeRepository;
import com.sg.dayamed.persistence.repository.PatientAssociationContactDetailsVRepository;
import com.sg.dayamed.persistence.repository.PresNotificationInformationVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailInfoVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailsInfoRepository;
import com.sg.dayamed.persistence.repository.PrescriptionInfoRepository;
import com.sg.dayamed.persistence.repository.PrescriptionInfoVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionNotificationsRepository;
import com.sg.dayamed.persistence.repository.PrescriptionRepository;
import com.sg.dayamed.prescription.vo.PrescriptionDetailInfoVO;
import com.sg.dayamed.prescription.vo.PrescriptionTimeVO;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.NotificationType;
import com.sg.dayamed.vo.NotificationSubscriptionDetailsVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Service
public class PrescriptionDetailInfoServiceImpl implements PrescriptionDetailInfoService {

    @Autowired
    PrescriptionRepository prescriptionRepository;

    @Autowired
    PrescriptionInfoRepository prescriptionInfoRepository;

    @Autowired
    PrescriptionDetailsInfoRepository prescriptionDetailsInfoRepository;

    @Autowired
    PrescriptionInfoVRepository prescriptionInfoVRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    PrescriptionDetailInfoVRepository detailInfoVRepository;

    @Autowired
    NotificationSubscriptionDetailsVRepository subscriptionDetailsVRepository;

    @Autowired
    PatientAssociationContactDetailsVRepository associationContactDetailsVRepository;

    @Autowired
    NotificationTypeRepository notificationTypeRepository;

    @Autowired
    PrescriptionNotificationsRepository notificationsRepository;

    @Autowired
    PresNotificationInformationVRepository notificationInformationVRepository;

    private List<NotificationType> notificationTypes = Stream.of(NotificationType.EMAIL, NotificationType.SMS).collect(Collectors.toList());


    @Override
    @Transactional
    public void startPrescription(PrescriptionDetailInfoVO detailInfoVO) throws ApplicationException {
        try {
            List<PrescriptionInfoV> prescriptionInfoVS = validatePrescriptionDetails(detailInfoVO);
            List<Long> presDetailsInfoIds = prescriptionInfoVS.stream().map(PrescriptionInfoV::getPrescriptionInfoId).distinct().collect(Collectors.toList());
            List<PrescriptionInfo> prescriptionInfoS = prescriptionInfoRepository.findAllById(presDetailsInfoIds);
            prescriptionInfoS = prescriptionInfoS.stream().map(pres -> modifyPrescriptionInfo(pres, detailInfoVO.getZoneId())).collect(Collectors.toList());
            prescriptionInfoS = prescriptionInfoRepository.saveAll(prescriptionInfoS);
            List<PrescriptionNotifications> prescriptionNotifications = prescriptionInfoS.stream().flatMap(info -> generatePrescriptionNotifications(info, detailInfoVO).stream()).collect(Collectors.toList());
            notificationsRepository.saveAll(prescriptionNotifications);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private List<PrescriptionNotifications> generatePrescriptionNotifications(PrescriptionInfo prescriptionInfo, PrescriptionDetailInfoVO detailInfoVO) {
        List<PrescriptionNotifications> prescriptionNotifications = new ArrayList<>();
        List<PresNotificationInformationV> notificationInformationVS = notificationInformationVRepository.findAllByPrescriptionInfoIdAndPresEndDateGreaterThanEqual(prescriptionInfo.getId(), ZonedDateTime.now());
        if (CollectionUtils.isEmpty(notificationInformationVS)) {
            // This will be in the cache
            List<NotificationSubscriptionDetailsV> subscriptionDetailsVS = subscriptionDetailsVRepository.findByDefaultFlag(true);
            List<NotificationSubscriptionDetailsVO> subscriptionDetailsVOS = subscriptionDetailsVS.stream().map(NotificationSubscriptionDetailsVO::new).collect(Collectors.toList());
            List<NotificationSubscriptionDetailsVO> detailsVOS = new ArrayList<>();
            prescriptionNotifications.addAll(generatePrescriptionNotifications(subscriptionDetailsVOS, detailInfoVO, prescriptionInfo));
        }
        return prescriptionNotifications;
    }

    private List<PrescriptionNotifications> generatePrescriptionNotifications(List<NotificationSubscriptionDetailsVO> subscriptionDetailsVOS, PrescriptionDetailInfoVO detailInfoVO, PrescriptionInfo prescriptionInfo) {
        PatientAssociationContactDetailsV contactDetailsV = associationContactDetailsVRepository.findByPatientId(detailInfoVO.getUserId());
        List<PrescriptionNotifications> notifications = subscriptionDetailsVOS.stream().map(details -> mapPrescriptionNotifications(details, contactDetailsV, prescriptionInfo)).collect(Collectors.toList());
        return notifications;
    }

    private PrescriptionNotifications mapPrescriptionNotifications(NotificationSubscriptionDetailsVO subscriptionDetailsVO, PatientAssociationContactDetailsV contactDetailsV, PrescriptionInfo prescriptionInfo) {
        PrescriptionNotifications notifications = new PrescriptionNotifications();
        Optional<com.sg.dayamed.persistence.domain.NotificationType> type = notificationTypeRepository.findById(subscriptionDetailsVO.getNotificationTypeId());
        notifications.setNotificationType(type.get());
        notifications.setDefaultFlag(true);
        notifications.setDelayInMinutes(subscriptionDetailsVO.getNotificationDelay());
        notifications.setRecipients(contactDetailsV.getEmailIds());
        Optional<PrescriptionInfo> prescriptionInfoOptional = prescriptionInfoRepository.findById(prescriptionInfo.getId());
        notifications.setPrescriptionInfo(prescriptionInfoOptional.get());
        return notifications;
    }


    private List<NotificationSubscriptionDetailsVO> getNotificationSubscriptionDetailsVOS(List<NotificationSubscriptionDetailsVO> subscriptionDetailsVOS, NotificationType notificationType) {
        return subscriptionDetailsVOS.stream().filter(details -> details.getNotificationTypeId().equals(notificationType.getId())).sorted(Comparator.comparing(NotificationSubscriptionDetailsVO::getDefaultFlag, Comparator.nullsLast(Comparator.naturalOrder()))).collect(Collectors.toList());
    }

    private PrescriptionInfo modifyPrescriptionInfo(PrescriptionInfo prescriptionInfo, String zoneId) {
        if (StringUtils.isNotBlank(zoneId)) {
            prescriptionInfo.setStartDate(ZonedDateTime.now(ZoneId.of(zoneId)));
            prescriptionInfo.setEndDate(ZonedDateTime.now(ZoneId.of(zoneId)).plusDays(new Long(prescriptionInfo.getCourseDuration())));
            prescriptionInfo.setZoneId(zoneId);
        }
        return prescriptionInfo;
    }

    @Override
    public void savePrescriptionDetailInfo(PrescriptionDetailInfoVO detailInfoVO) throws ApplicationException {
        try {
            List<PrescriptionInfoV> prescriptionInfoVS = validatePrescriptionDetails(detailInfoVO);
            List<Long> presDetailsInfoIds = prescriptionInfoVS.stream().map(PrescriptionInfoV::getPrescriptionInfoId).distinct().collect(Collectors.toList());
            List<PrescriptionInfo> prescriptionInfoS = prescriptionInfoRepository.findAllById(presDetailsInfoIds);
            List<PrescriptionDetailInfo> prescriptionDetailInfoS = new ArrayList<>();
            prescriptionInfoVS.forEach(info -> {
                try {
                    prescriptionDetailInfoS.addAll(mapToPrescriptionDetailInfo(info, prescriptionInfoS, detailInfoVO.getZoneId()));
                } catch (Exception e) {

                }
            });
            prescriptionDetailsInfoRepository.saveAll(prescriptionDetailInfoS);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private List<PrescriptionDetailInfo> mapToPrescriptionDetailInfo(PrescriptionInfoV prescriptionInfoV, List<PrescriptionInfo> prescriptionInfos, String zoneId) throws ApplicationException {
        try {
            List<PrescriptionDetailInfo> detailInfoS = new ArrayList<PrescriptionDetailInfo>();
            PrescriptionTimeVO timeVO = objectMapper.readValue(prescriptionInfoV.getIntakeTime(), PrescriptionTimeVO.class);
            timeVO.getTime().forEach(time -> {
                PrescriptionDetailInfo prescriptionDetailInfo = new PrescriptionDetailInfo();
                if (StringUtils.isNotBlank(zoneId)) {
                    prescriptionDetailInfo.setStartDate(ZonedDateTime.now(ZoneId.of(zoneId)));
                    prescriptionDetailInfo.setEndDate(ZonedDateTime.now(ZoneId.of(zoneId)).plusDays(new Long(prescriptionInfoV.getCourseDuration())));
                }
                prescriptionDetailInfo.setPrescriptionInfo(prescriptionInfos.stream().filter(info -> info.getId().equals(prescriptionInfoV.getPrescriptionInfoId())).findAny().orElse(null));
                Date date = getTime(time, zoneId);
                prescriptionDetailInfo.setPrescribedTime(date);
                detailInfoS.add(prescriptionDetailInfo);
            });
            return detailInfoS;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private Date getTime(String time, String zone) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String date = formatter.format(ZonedDateTime.now());
        String dateTime = date + " " + time;
        ZoneId zoneId = StringUtils.isNotBlank(zone) ? ZoneId.of(zone) : ZoneId.systemDefault();
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone(zoneId);
        ZonedDateTime zoned = ZonedDateTime.parse(dateTime, timeFormatter);
        return Date.from(zoned.toInstant());
    }

    private List<PrescriptionInfoV> validatePrescriptionDetails(PrescriptionDetailInfoVO detailInfo) throws DataValidationException {
        List<PrescriptionInfoV> prescriptionInfoVS = prescriptionInfoVRepository.findByPrescriptionIdAndUserId(detailInfo.getPrescriptionId(), detailInfo.getUserId());
        if (CollectionUtils.isEmpty(prescriptionInfoVS)) {
            throw new DataValidationException("prescriptionId", APIErrorKeys.ERROR_PATIENT_PRESCRIPTION_DETAILS_NOT_FOUND);
        }
        List<PrescriptionDetailInfoV> detailInfoS = detailInfoVRepository.findByPatientIdAndPrescriptionIdAndPresEndDateGreaterThan(detailInfo.getUserId(), detailInfo.getPrescriptionId(), ZonedDateTime.now());
        if (CollectionUtils.isNotEmpty(detailInfoS)) {
            throw new DataValidationException("prescriptionId", APIErrorKeys.ERROR_PATIENT_PRESCRIPTION_IS_ALREADY_ACTIVE);
        }
        return prescriptionInfoVS;
    }
}
