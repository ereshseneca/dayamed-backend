package com.sg.dayamed.prescription;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.prescription.vo.PrescriptionDetailInfoVO;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/

public interface PrescriptionDetailInfoService {
    public void savePrescriptionDetailInfo(PrescriptionDetailInfoVO detailInfoVO) throws ApplicationException;
    public void startPrescription(PrescriptionDetailInfoVO detailInfoVO) throws ApplicationException;
}
