package com.sg.dayamed.prescription;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.domain.PrescriptionDetailsProcessing;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Eresh Gorantla on 26/Feb/2019
 **/

public interface PrescriptionDetailsProcessingService {
    void persistPrescriptionDetails() throws ApplicationException;
    List<PrescriptionDetailsProcessing> persistPrescriptionDetails(List<PrescriptionDetailInfoV> detailInfoVS) throws ApplicationException;
    List<PrescriptionDetailsProcessing> getPrescriptionInfoSForCreatedDate(List<Long> prescriptionInfoIds, ZonedDateTime createdDate) throws ApplicationException;
}
