package com.sg.dayamed.prescription;

import com.sg.dayamed.commons.constants.IGlobalErrorKeys;
import com.sg.dayamed.commons.constants.IWSGlobalApiErrorKeys;
import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.commons.util.ValidationUtils;
import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.commons.vo.ValueIndexVO;
import com.sg.dayamed.persistence.domain.Prescription;
import com.sg.dayamed.persistence.domain.PrescriptionNotifications;
import com.sg.dayamed.persistence.domain.UserDetails;
import com.sg.dayamed.persistence.domain.UserRoleMapping;
import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.persistence.repository.PatientAssociationInformationVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionDetailInfoVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionNotificationsRepository;
import com.sg.dayamed.persistence.repository.PrescriptionRepository;
import com.sg.dayamed.persistence.repository.UserDetailsRepository;
import com.sg.dayamed.persistence.repository.UserInformationVRepository;
import com.sg.dayamed.persistence.repository.UserRoleMappingRepository;
import com.sg.dayamed.prescription.vo.PrescriptionBasicInfoVO;
import com.sg.dayamed.prescription.vo.PrescriptionInfoVO;
import com.sg.dayamed.prescription.vo.PrescriptionInformationVO;
import com.sg.dayamed.prescription.vo.PrescriptionMinInfoVO;
import com.sg.dayamed.prescription.vo.PrescriptionVO;
import com.sg.dayamed.prescription.vo.SavePrescriptionVO;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.FrequencyType;
import com.sg.dayamed.util.ServiceUtils;
import com.sg.dayamed.util.UserRole;
import com.sg.dayamed.util.WeekDays;
import com.sg.dayamed.validation.IServiceFieldNames;
import com.sg.dayamed.vo.NotificationInformationVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@Service
public class PrescriptionServiceImpl implements PrescriptionService {

    @Autowired
    UserInformationVRepository informationVRepository;

    @Autowired
    UserDetailsRepository userDetailsRepository;

    @Autowired
    PrescriptionRepository prescriptionRepository;

    @Autowired
    UserRoleMappingRepository userRoleMappingRepository;

    @Autowired
    PatientAssociationInformationVRepository associationInformationVRepository;

    @Autowired
    PrescriptionDetailInfoVRepository prescriptionDetailInfoVRepository;

    @Autowired
    PrescriptionNotificationsRepository notificationsRepository;

    //@Transactional(Transactional.TxType.REQUIRES_NEW)
    public List<PrescriptionInformationVO> savePrescription(SavePrescriptionVO savePrescriptionVO, UserPrincipal userPrincipal) throws ApplicationException {
        try {
            List<PrescriptionVO> prescriptionVOS = savePrescriptionVO.getPrescriptions();
            prescriptionVOS = validatePrescription(prescriptionVOS, userPrincipal, savePrescriptionVO.getPatientId(), savePrescriptionVO);
            //List<Prescription> prescriptions = prescriptionVOS.stream().map(pres -> pres.toPrescription(savePrescriptionVO.getZoneId())).collect(Collectors.toList());

            // Delete Prescriptions if any.
            deActivatePrescriptions(savePrescriptionVO.getDeletedPrescriptionIds());

            List<Long> prescriptionIds = prescriptionVOS.stream().filter(pres -> pres.getId() != null).map(PrescriptionVO::getId).distinct().collect(Collectors.toList());

            for (Long prescriptionId : prescriptionIds) {
                PrescriptionVO prescriptionVO = prescriptionVOS.stream().filter(pres -> pres.getId().equals(prescriptionId)).findAny().orElse(null);
                List<IdIndexVO> idIndexVOS = prescriptionVO.getDeletedPrescriptionInfoIds();
                deActivatePrescriptionInfos(prescriptionId, idIndexVOS);
            }

            List<Prescription> prescriptions = getModifiedPrescriptions(prescriptionVOS, savePrescriptionVO.getZoneId());

            PrescriptionVO prescriptionVO = prescriptionVOS.get(0);
            Optional<UserRoleMapping> optionalUserRoleMapping = userRoleMappingRepository.findById(prescriptionVO.getCreatorId());
            if (optionalUserRoleMapping.isPresent()) {
                prescriptions.stream().forEach(pres -> {
                    pres.setUserRoleMapping(optionalUserRoleMapping.get());
                });
            }
            Optional<UserDetails> userDetailsOptional = userDetailsRepository.findById(savePrescriptionVO.getPatientId());
            if (userDetailsOptional.isPresent()) {
                prescriptions.stream().forEach(pres -> {
                    pres.setPatient(userDetailsOptional.get());
                });
            }
            prescriptions = prescriptionRepository.saveAll(prescriptions);
            prescriptionIds = prescriptions.stream().map(Prescription::getId).distinct().collect(Collectors.toList());
            prescriptions = prescriptionRepository.findByIdIn(prescriptionIds);
            return prescriptions.stream().map(pres -> new PrescriptionInformationVO(pres)).collect(Collectors.toList());
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private void validateActualValues(List<String> actualValues, Integer typeId, Integer prescriptionIdIndex, Integer presInfoIdIndex) throws DataValidationException {
        List<ValueIndexVO> indexVOS = IntStream.range(0, actualValues.size()).mapToObj(index -> new ValueIndexVO(index, actualValues.get(index))).collect(Collectors.toList());
        FrequencyType type = FrequencyType.getFrequencyType(typeId);

        if (type != null) {
            if (!FrequencyType.DAILY.equals(type) && CollectionUtils.isEmpty(indexVOS)) {
                throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES, true, prescriptionIdIndex, presInfoIdIndex), IGlobalErrorKeys.ERRORS_GENERAL_VALUE_REQUIRED);
            }
            for (int index = 0; index < indexVOS.size(); index++) {
                ValueIndexVO vo = indexVOS.get(index);
                if (StringUtils.isBlank(vo.getValue())) {
                    throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES_VALUE, true, prescriptionIdIndex, presInfoIdIndex, index), APIErrorKeys.ERROR_NULL_IS_NOT_ACCEPTED);
                }
                if (FrequencyType.WEEKLY.equals(type)) {
                    if (WeekDays.getWeekDay(vo.getValue()) == null) {
                        throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES_VALUE, true, prescriptionIdIndex, presInfoIdIndex, index), APIErrorKeys.ERROR_INVALID_WEEK_DAY_VALUE, new Object[]{vo.getValue()});
                    }
                }
                if (FrequencyType.MONTHLY.equals(type)) {
                    Integer value = NumberUtils.toInt(vo.getValue());
                    if (!(value >= NumberUtils.INTEGER_ONE && value <= 31)) {
                        throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES_VALUE, true, prescriptionIdIndex, presInfoIdIndex, index), APIErrorKeys.ERROR_INVALID_DAY_VALUE_FOR_MONTH, new Object[]{vo.getValue()});
                    }
                }
            }
        }
    }


    public PrescriptionMinInfoVO getPrescriptions(String patientId, UserPrincipal userPrincipal, Integer limit, Integer offset) throws ApplicationException {
        try {
            PrescriptionMinInfoVO minInfoVO = new PrescriptionMinInfoVO();
            limit = limit == -1 ? Integer.MAX_VALUE : limit;
            Long patientNumberId = ServiceUtils.getId(patientId, "patientId");
            Long roleMapId = userPrincipal.getRoleMappingId();
            UserDetails userDetails = userDetailsRepository.getOne(patientNumberId);
            if (userDetails == null) {
                throw new DataValidationException("patientId", APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID, new Object[]{patientId});
            }
            UserRoleMapping userRoleMapping = userRoleMappingRepository.getOne(roleMapId);
            if (userRoleMapping == null) {
                throw new DataValidationException("mapId", APIErrorKeys.ERROR_USER_ROLE_MAP_ID_NOT_EXISTS);
            }
            PatientAssociationInformationV associationInformationV = associationInformationVRepository.findByPatientIdAndUserRoleMapId(userDetails.getId(),
                    userRoleMapping.getId());
            if (associationInformationV == null) {
                throw new DataValidationException("patientId", APIErrorKeys.ERROR_NO_PRIVILEGES_TO_VIEW_PATIENT_DATA);
            }
            Page<Prescription> prescriptionPage = prescriptionRepository.findByPatientAndUserRoleMappingOrderByActiveDesc(userDetails, userRoleMapping, PageRequest.of(offset,
                    limit));
            List<Prescription> prescriptions = prescriptionPage.getContent();
            List<PrescriptionBasicInfoVO> basicInfoVOS = prescriptions.stream().map(PrescriptionBasicInfoVO::new).collect(Collectors.toList());
            minInfoVO.setPrescriptions(basicInfoVOS);
            minInfoVO.setTotalRecords(prescriptionPage.getTotalElements());
            return minInfoVO;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private List<Prescription> getModifiedPrescriptions(List<PrescriptionVO> prescriptionVOS, String zoneId) {
        List<Long> prescriptionIds = prescriptionVOS.stream().filter(pres -> pres.getId() != null).map(PrescriptionVO::getId).distinct().collect(Collectors.toList());
        List<Prescription> prescriptions = prescriptionRepository.findByIdIn(prescriptionIds);
        List<Prescription> prescriptionList = new ArrayList<>();
        for (PrescriptionVO prescriptionVO : prescriptionVOS) {
            if (prescriptionVO.getId() != null) {
                Prescription prescription = prescriptions.stream().filter(pres -> pres.getId().equals(prescriptionVO.getId())).findAny().orElse(null);
                if (prescription != null) {
                    prescription = prescriptionVO.toPrescription(zoneId, prescription);
                    prescriptionList.add(prescription);
                }
            } else {
                prescriptionList.add(prescriptionVO.toPrescription(zoneId));
            }
        }
        return prescriptionList;
    }

    private void deActivatePrescriptions(List<IdIndexVO> prescriptionIds) {
        List<Long> toBeDeletedPresIds = prescriptionIds.stream().filter(id -> id.getId() != null).map(IdIndexVO::getId).distinct().collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(toBeDeletedPresIds)) {
            List<Prescription> deletedPrescriptions = prescriptionRepository.findByIdIn(toBeDeletedPresIds);
            deletedPrescriptions.stream().forEach(pres -> {
                pres.setActive(false);
                pres.getPrescriptionInfoS().stream().forEach(info -> {
                    info.setActive(false);
                    List<PrescriptionNotifications> prescriptionNotifications = info.getNotifications();
                    notificationsRepository.deleteInBatch(prescriptionNotifications);
                });
            });
            prescriptionRepository.saveAll(deletedPrescriptions);
        }
    }

    private void deActivatePrescriptionInfos(Long prescriptionId, List<IdIndexVO> prescriptionInfoIds) {
        List<Long> ids = prescriptionInfoIds.stream().filter(id -> id.getId() != null).map(IdIndexVO::getId).distinct().collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(ids)) {
            Optional<Prescription> prescriptionOptional = prescriptionRepository.findById(prescriptionId);
            if (prescriptionOptional.isPresent()) {
                Prescription prescription = prescriptionOptional.get();
                prescription.getPrescriptionInfoS().stream().forEach(info -> {
                    if (ids.contains(info.getId())) {
                        info.setActive(false);
                        List<PrescriptionNotifications> prescriptionNotifications = info.getNotifications();
                        notificationsRepository.deleteInBatch(prescriptionNotifications);
                    }
                });
                prescriptionRepository.save(prescription);
            }
        }
    }

    public PrescriptionInformationVO getPrescriptionInfo(Long prescriptionId, UserPrincipal userPrincipal) throws ApplicationException {
        try {
            List<PrescriptionDetailInfoV> detailInfoVS = prescriptionDetailInfoVRepository.findByPrescriptionId(prescriptionId);
            if (CollectionUtils.isEmpty(detailInfoVS)) {
                throw new DataValidationException(IServiceFieldNames.PRESCRIPTION_ID, APIErrorKeys.ERROR_PRESCRIPTION_DETAILS_NOT_FOUND_FOR_ID, new Object[]{prescriptionId});
            }
            detailInfoVS = detailInfoVS.stream().filter(info -> info.getCreatorId().equals(userPrincipal.getRoleMappingId())).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(detailInfoVS)) {
                throw new DataValidationException(IServiceFieldNames.PRESCRIPTION_ID, IWSGlobalApiErrorKeys.ERRORS_AUTHORIZATION_FAILED);
            }
            return new PrescriptionInformationVO(detailInfoVS);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private List<PrescriptionVO> validatePrescription(List<PrescriptionVO> prescriptionVOS, UserPrincipal userPrincipal, Long patientId, SavePrescriptionVO savePrescriptionVO) throws DataValidationException {
        List<UserInformationV> informationVS = informationVRepository.findByUserId(patientId);
        if (CollectionUtils.isEmpty(informationVS)) {
            throw new DataValidationException(IServiceFieldNames.PATIENT_ID, APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID, new Object[]{patientId});
        }

        PatientAssociationInformationV associationInformationV = associationInformationVRepository.findByPatientIdAndUserRoleMapId(patientId,
                userPrincipal.getRoleMappingId());
        if (associationInformationV == null) {
            throw new DataValidationException(IServiceFieldNames.PATIENT_ID, APIErrorKeys.ERROR_PATIENT_NOT_ASSOCIATED_WITH_USER);
        }

        GrantedAuthority grantedAuthority = userPrincipal.getAuthorities().stream().findFirst().get();
        UserRole userRole = UserRole.getUserRole(grantedAuthority.getAuthority());
        if (userRole == null) {
            throw new DataValidationException(IServiceFieldNames.ROLE, APIErrorKeys.ERROR_USER_ROLE_INVALID);
        }

        if (UserRole.PATIENT.equals(userRole)) {
            throw new DataValidationException(IServiceFieldNames.ROLE, APIErrorKeys.ERROR_PATIENT_CANNOT_ADD_PRESCRIPTION_FOR_ANOTHER_PATIENT);
        }

        UserInformationV informationV = informationVS.stream().filter(info -> StringUtils.equalsIgnoreCase(UserRole.PATIENT.getValue(), info.getRoleName())).findAny().orElse(null);
        if (informationV == null) {
            throw new DataValidationException(IServiceFieldNames.PATIENT_ID, APIErrorKeys.ERROR_PRESCRIPTION_CANNOT_BE_ADDED_OTHER_THAN_PATIENTS, new Object[]{patientId});
        }

        Long roleMap = userPrincipal.getRoleMappingId();
        UserInformationV roleInformation = informationVRepository.findByRoleMappingId(roleMap);
        if (roleInformation == null) {
            throw new DataValidationException(IServiceFieldNames.ROLE_MAP_ID, APIErrorKeys.ERROR_USER_ROLE_MAP_ID_NOT_EXISTS);
        }

        List<IdIndexVO> presIdIndexes =
                IntStream.range(NumberUtils.INTEGER_ZERO, prescriptionVOS.size()).mapToObj(index -> new IdIndexVO(index, prescriptionVOS.get(index).getId())).filter(idIndex -> idIndex.getId() != null).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(presIdIndexes)) {
            List<Long> prescriptionIds = presIdIndexes.stream().map(IdIndexVO::getId).collect(Collectors.toList());
            List<PrescriptionDetailInfoV> prescriptionDetailInfoVS = prescriptionDetailInfoVRepository.findByPrescriptionIdIn(prescriptionIds);
            List<Long> availableIds = prescriptionDetailInfoVS.stream().map(PrescriptionDetailInfoV::getPrescriptionId).distinct().collect(Collectors.toList());
            Long unAvailableId = prescriptionIds.stream().filter(pres -> !availableIds.contains(pres)).findAny().orElse(null);
            if (unAvailableId != null) {
                throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.SAVE_PRESCRIPTION_PRESCRIPTIONS_ID, true, ValidationUtils.getIndex(presIdIndexes,
                        unAvailableId)), APIErrorKeys.ERROR_PRESCRIPTION_ID_NOT_FOUND);
            }
            for (Long prescriptionId : prescriptionIds) {
                List<PrescriptionDetailInfoV> filteredDetailInfoVS = prescriptionDetailInfoVS.stream().filter(pres -> pres.getPrescriptionId().equals(prescriptionId)).collect(Collectors.toList());
                PrescriptionVO prescriptionVO = prescriptionVOS.stream().filter(pres -> pres.getId().equals(prescriptionId)).findAny().orElse(null);
                validatePrescriptionInfoS(prescriptionVO, presIdIndexes, filteredDetailInfoVS, prescriptionId);
            }
        }
        validatePrescriptionDeletions(savePrescriptionVO);
        for (int index = 0; index < prescriptionVOS.size(); index++) {
            PrescriptionVO prescriptionVO = prescriptionVOS.get(index);
            prescriptionVO.setCreatorId(roleMap);
        }
        return prescriptionVOS;
    }

    private void validatePrescriptionDeletions(SavePrescriptionVO savePrescriptionVO) throws DataValidationException {
        List<IdIndexVO> deletedPrescriptionIndexes = savePrescriptionVO.getDeletedPrescriptionIds();
        if (CollectionUtils.isNotEmpty(deletedPrescriptionIndexes)) {
            List<Long> toBeDeletedPresIds = deletedPrescriptionIndexes.stream().filter(id -> id.getId() != null).map(IdIndexVO::getId).distinct().collect(Collectors.toList());
            List<PrescriptionDetailInfoV> tobeDeletedPrescriptionDetailInfoVS = prescriptionDetailInfoVRepository.findByPrescriptionIdIn(toBeDeletedPresIds);
            List<Long> availablePresIds = tobeDeletedPrescriptionDetailInfoVS.stream().map(PrescriptionDetailInfoV::getPrescriptionId).distinct().collect(Collectors.toList());
            Long unAvailablePresId = toBeDeletedPresIds.stream().filter(id -> !availablePresIds.contains(id)).findFirst().orElse(null);
            if (unAvailablePresId != null) {
                throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.DELETED_PRESCRIPTION_IDS, true,
                        ValidationUtils.getIndex(deletedPrescriptionIndexes, unAvailablePresId)), APIErrorKeys.ERROR_PRESCRIPTION_ID_NOT_FOUND);
            }
            Long alreadyDeletedId =
                    tobeDeletedPrescriptionDetailInfoVS.stream().filter(pres -> !pres.getActivePrescription()).map(PrescriptionDetailInfoV::getPrescriptionId).findFirst().orElse(null);
            if (alreadyDeletedId != null) {
                throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.DELETED_PRESCRIPTION_IDS, true,
                        ValidationUtils.getIndex(deletedPrescriptionIndexes, alreadyDeletedId)), APIErrorKeys.ERROR_PRESCRIPTION_ID_ALREADY_DELETED);
            }
        }
    }

    private PrescriptionVO validatePrescriptionInfoS(PrescriptionVO prescriptionVO, List<IdIndexVO> presIdIndexes, List<PrescriptionDetailInfoV> prescriptionDetailInfoVS,
                                                     Long prescriptionId) throws DataValidationException {
        List<PrescriptionDetailInfoV> detailInfoVS =
                prescriptionDetailInfoVS.stream().filter(pres -> pres.getPrescriptionId().equals(prescriptionId)).collect(Collectors.toList());
        List<Long> availableInfoIds = detailInfoVS.stream().map(PrescriptionDetailInfoV::getPrescriptionInfoId).distinct().collect(Collectors.toList());
        List<PrescriptionInfoVO> prescriptionInfoVOS = prescriptionVO.getPrescriptionInfoS();
        List<IdIndexVO> infoIdIndexes = IntStream.range(NumberUtils.INTEGER_ZERO, prescriptionInfoVOS.size()).mapToObj(index -> new IdIndexVO(index,
                prescriptionInfoVOS.get(index).getId())).collect(Collectors.toList());
        List<Long> providedInfoIds = infoIdIndexes.stream().filter(id -> id.getId() != null).map(IdIndexVO::getId).collect(Collectors.toList());
        Long unAvailableInfoId = providedInfoIds.stream().filter(id -> id != null).filter(id -> !availableInfoIds.contains(id)).findAny().orElse(null);
        if (unAvailableInfoId != null) {
            throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.SAVE_PRESCRIPTION_INFO_ID, true, ValidationUtils.getIndex(presIdIndexes,
                    prescriptionId), ValidationUtils.getIndex(infoIdIndexes, unAvailableInfoId)), APIErrorKeys.ERROR_PRESCRIPTION_INFO_ID_NOT_FOUND);
        }

        List<IdIndexVO> tobeDeletedInfoIds = prescriptionVO.getDeletedPrescriptionInfoIds();
        if (CollectionUtils.isNotEmpty(tobeDeletedInfoIds)) {
            List<Long> deletedIds = tobeDeletedInfoIds.stream().filter(id -> id.getId() != null).map(IdIndexVO::getId).distinct().collect(Collectors.toList());
            Long unAvailableDeletedInfoId = deletedIds.stream().filter(ids -> !availableInfoIds.contains(ids)).findFirst().orElse(null);
            if (unAvailableDeletedInfoId != null) {
                throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.DELETED_PRESCRIPTION_INFO_IDS, true, ValidationUtils.getIndex(presIdIndexes,
                        prescriptionId), ValidationUtils.getIndex(tobeDeletedInfoIds, unAvailableDeletedInfoId)), APIErrorKeys.ERROR_PRESCRIPTION_INFO_ID_NOT_FOUND);
            }
            PrescriptionDetailInfoV detailInfoV =
                    prescriptionDetailInfoVS.stream().filter(pres -> deletedIds.contains(pres.getPrescriptionInfoId())).filter(pres -> !pres.getActivePrescriptionInfo()).findFirst().orElse(null);
            if (detailInfoV != null) {
                throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.DELETED_PRESCRIPTION_INFO_IDS, true, ValidationUtils.getIndex(presIdIndexes,
                        prescriptionId), ValidationUtils.getIndex(tobeDeletedInfoIds, detailInfoV.getPrescriptionInfoId())), APIErrorKeys.ERROR_PRESCRIPTION_INFO_ID_ALREADY_DELETED);
            }
        }
        for (int index = 0; index < prescriptionInfoVOS.size(); index++) {
            PrescriptionInfoVO prescriptionInfoVO = prescriptionInfoVOS.get(index);
            if (prescriptionInfoVO.getFrequency() != null) {
                validateActualValues(prescriptionInfoVO.getFrequency().getActualValues(), prescriptionInfoVO.getFrequency().getTypeId(), ValidationUtils.getIndex(presIdIndexes, prescriptionId), index);
            }
        }
        /*for (PrescriptionInfoVO prescriptionInfoVO : prescriptionInfoVOS) {
            if (prescriptionInfoVO.getFrequency() != null) {
                validateActualValues(prescriptionInfoVO.getFrequency().getActualValues(), prescriptionInfoVO.getFrequency().getTypeId(),ValidationUtils.getIndex(presIdIndexes,
                        prescriptionId), 0);
            }
        }*/

        for (Long presInfoId : providedInfoIds) {
            List<PrescriptionDetailInfoV> filteredDetailInfoVS = prescriptionDetailInfoVS.stream().filter(pres -> pres.getPrescriptionInfoId().equals(presInfoId)).collect(Collectors.toList());
            validatePrescriptionNotifications(presIdIndexes, filteredDetailInfoVS, prescriptionId, prescriptionInfoVOS, infoIdIndexes, presInfoId);
        }
        return prescriptionVO;
    }

    private void validatePrescriptionNotifications(List<IdIndexVO> presIdIndexes, List<PrescriptionDetailInfoV> prescriptionDetailInfoVS, Long prescriptionId, List<PrescriptionInfoVO> prescriptionInfoVOS, List<IdIndexVO> infoIdIndexes, Long presInfoId) throws DataValidationException {
        List<PrescriptionDetailInfoV> notificationsInfoS =
                prescriptionDetailInfoVS.stream().filter(pres -> pres.getPrescriptionInfoId().equals(presInfoId)).collect(Collectors.toList());
        List<Long> availableNotificationIds = notificationsInfoS.stream().map(PrescriptionDetailInfoV::getNotificationId).distinct().collect(Collectors.toList());
        PrescriptionInfoVO prescriptionInfoVO = prescriptionInfoVOS.stream().filter(pres -> pres.getId().equals(presInfoId)).findAny().orElse(null);
        List<NotificationInformationVO> notificationInformationVOS = prescriptionInfoVO.getNotifications();
        List<IdIndexVO> notificationIndexes =
                IntStream.range(NumberUtils.INTEGER_ZERO, notificationInformationVOS.size()).mapToObj(index -> new IdIndexVO(index,
                        notificationInformationVOS.get(index).getId())).collect(Collectors.toList());
        List<Long> providedNotificationIds = notificationIndexes.stream().filter(id -> id.getId() != null).map(IdIndexVO::getId).collect(Collectors.toList());
        Long unavailableNotificationId = providedNotificationIds.stream().filter(id -> !availableNotificationIds.contains(id)).findFirst().orElse(null);
        if (unavailableNotificationId != null) {
            throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.SAVE_PRESCRIPTION_NOTIFICATION_IDS, true, ValidationUtils.getIndex(presIdIndexes,
                    prescriptionId), ValidationUtils.getIndex(infoIdIndexes, presInfoId), ValidationUtils.getIndex(notificationIndexes, unavailableNotificationId)),
                    APIErrorKeys.ERROR_NOTIFICATION_ID_NOT_FOUND);
        }
        List<IdIndexVO> tobeDeletedNotificationIds = prescriptionInfoVO.getDeletedNotificationIds();
        if (CollectionUtils.isNotEmpty(tobeDeletedNotificationIds)) {
            List<Long> availableIds = prescriptionDetailInfoVS.stream().map(PrescriptionDetailInfoV::getNotificationId).distinct().collect(Collectors.toList());
            IdIndexVO unAvailableIndex = tobeDeletedNotificationIds.stream().filter(id -> !availableIds.contains(id.getId())).findFirst().orElse(null);
            if (unAvailableIndex != null) {
                throw new DataValidationException(ValidationUtils.getFieldName(IServiceFieldNames.DELETED_NOTIFICATIONS_IDS, true, ValidationUtils.getIndex(presIdIndexes,
                        prescriptionId), ValidationUtils.getIndex(infoIdIndexes, presInfoId), unAvailableIndex.getIndex()), APIErrorKeys.ERROR_NOTIFICATION_ID_NOT_FOUND);
            }
            List<PrescriptionNotifications> prescriptionNotifications =
                    notificationsRepository.findAllById(tobeDeletedNotificationIds.stream().filter(id -> id.getId() != null).map(IdIndexVO::getId).distinct().collect(Collectors.toList()));
            notificationsRepository.deleteInBatch(prescriptionNotifications);
        }
    }

}
