package com.sg.dayamed.prescription;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.domain.NotificationProcessing;
import com.sg.dayamed.persistence.domain.PrescriptionDetailsProcessing;

import java.util.List;

/**
 * Created by Eresh Gorantla on 26/Feb/2019
 **/

public interface PrescriptionNotificationService {
    void persistNotificationData(List<PrescriptionDetailsProcessing> detailsProcessingS) throws ApplicationException;
    List<NotificationProcessing> getPrescriptionNotificationsByProcessingIds(List<String> processingIds) throws ApplicationException;
    void deleteByPrescriptionProcessingIds(List<String> processingIds) throws ApplicationException;

}
