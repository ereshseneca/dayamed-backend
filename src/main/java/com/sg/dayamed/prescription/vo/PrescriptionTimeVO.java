package com.sg.dayamed.prescription.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Getter
@Setter
public class PrescriptionTimeVO {
    private List<String> time;
}
