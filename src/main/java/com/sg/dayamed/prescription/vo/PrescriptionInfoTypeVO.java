package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.persistence.domain.PrescriptionInfoType;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import com.sg.dayamed.vo.AdditionalInfoVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionInfoTypeVO {
    private Long id;
    private String name;
    private String description;
    private AdditionalInfoVO additionalInfo;
    private PrescriptionTypeVO prescriptionType;
    private ZonedDateTime createdDate = ZonedDateTime.now();
    private ZonedDateTime updatedDate;

    public PrescriptionInfoTypeVO(PrescriptionDetailInfoV detailInfoV) {
        if (detailInfoV != null) {
            this.id = detailInfoV.getPresInfoTypeId();
            this.name = detailInfoV.getPresInfoType();
            this.prescriptionType = new PrescriptionTypeVO(detailInfoV);
        }
    }

    public PrescriptionInfoTypeVO(PrescriptionInfoType infoType) {
        if (infoType != null) {
            this.id = infoType.getId();
            this.name = infoType.getName();
            this.description = infoType.getDescription();
            if (infoType.getPrescriptionType() != null)
                this.prescriptionType = new PrescriptionTypeVO(infoType.getPrescriptionType().getId(), infoType.getPrescriptionType().getName());
        }
    }


}
