package com.sg.dayamed.prescription.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 04/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionMinInfoVO {
    private List<PrescriptionBasicInfoVO> prescriptions = new ArrayList<>();
    private Long totalRecords;
}
