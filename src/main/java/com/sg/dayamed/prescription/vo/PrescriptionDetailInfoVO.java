package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.rest.ws.WSStartPrescriptionRequest;
import com.sg.dayamed.vo.NotificationInformationVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionDetailInfoVO {
    private Long prescriptionId;
    private Long userId;
    private boolean active;
    private String zoneId;
    private List<NotificationInformationVO> notifications;

    public PrescriptionDetailInfoVO(WSStartPrescriptionRequest request) {
        /*this.prescriptionId = request.getPrescriptionId();
        this.userId = request.getUserId();
        this.active = request.isActive();
        this.zoneId = request.getZoneId();*/
    }
}
