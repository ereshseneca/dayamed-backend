package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.persistence.domain.Prescription;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionVO {
    private Long id;
    private String name;
    private String providerComment;
    private String illnessDesc;
    private String diagnosis;
    private Boolean active;
    private Long creatorId;
    private Integer prescriptionTypeId;
    private Boolean isUpdate;
    private List<PrescriptionInfoVO> prescriptionInfoS = new ArrayList<>();
    private List<IdIndexVO> deletedPrescriptionInfoIds = new ArrayList<>();


    public Prescription toPrescription(String zoneId) {
        Prescription prescription = new Prescription();
        prescription.setActive(this.active);
        prescription.setId(this.id);
        prescription.setName(this.name);
        prescription.setProviderComment(this.providerComment);
        prescription.setPatientIllnessDesc(this.illnessDesc);
        prescription.setPatientIllnessDiagnosis(this.diagnosis);
        prescription.setPrescriptionInfoS(this.prescriptionInfoS.stream().map(info -> info.toPrescriptionInfo(zoneId, prescription)).collect(Collectors.toList()));
        return prescription;
    }

    public Prescription toPrescription(String zoneId, Prescription prescription) {
        if (prescription != null) {
            prescription.setActive(this.active);
            prescription.setId(this.id);
            prescription.setName(this.name);
            prescription.setProviderComment(this.providerComment);
            prescription.setPatientIllnessDesc(this.illnessDesc);
            prescription.setPatientIllnessDiagnosis(this.diagnosis);
            prescription.setPrescriptionInfoS(this.prescriptionInfoS.stream().map(info -> info.toPrescriptionInfo(zoneId, prescription)).collect(Collectors.toList()));
            return prescription;
        }
        return null;
    }

}
