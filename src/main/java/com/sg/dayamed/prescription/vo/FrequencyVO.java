package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.persistence.domain.FrequencyDetails;
import com.sg.dayamed.rest.ws.WSFrequencyInfo;
import com.sg.dayamed.util.FrequencyType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 09/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class FrequencyVO {
    private Integer typeId;
    private List<String> actualValues = new ArrayList<>();
    private Integer recurrence;
    private Integer duration;
    private String startDate;

    public FrequencyVO(WSFrequencyInfo info) {
        if (info != null) {
            this.typeId = info.getTypeId();
            this.actualValues = info.getActualValues();
            this.recurrence = info.getRecurrence();
            this.duration = info.getDuration();
            this.startDate = info.getStartDate();
        }
    }

    public FrequencyDetails toFrequencyDetails() {
        FrequencyDetails details = new FrequencyDetails();
        details.setActualValues(returnActualValues());
        details.setDuration(this.duration);
        details.setRecurrence(this.recurrence);
        details.setStartDate(this.startDate);
        details.setTypeId(this.typeId);
        return details;
    }

    public FrequencyVO(FrequencyDetails frequencyDetails) {
        this.actualValues = returnActualValues(frequencyDetails.getActualValues());
        this.startDate = frequencyDetails.getStartDate();
        this.typeId = frequencyDetails.getTypeId();
        this.duration = frequencyDetails.getDuration();
        this.recurrence = frequencyDetails.getRecurrence();
    }

    public List<Object> returnActualValues() {
        FrequencyType type = FrequencyType.getFrequencyType(this.typeId);
        if (type != null) {
            if (FrequencyType.MONTHLY.equals(type)) {
                List<Integer> integers = this.actualValues.stream().filter(str -> StringUtils.isNotBlank(str)).map(Integer::valueOf).collect(Collectors.toList());
                return (List) integers;
            } else {
                return (List) this.actualValues;
            }
        }
        return null;
    }

    public List<String> returnActualValues(List<Object> values) {
        return values.stream().filter(obj -> obj != null).map(Object :: toString).collect(Collectors.toList());
    }
}
