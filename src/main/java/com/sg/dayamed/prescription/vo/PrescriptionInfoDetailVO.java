package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import com.sg.dayamed.vo.NotificationDetailsVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionInfoDetailVO {
    private Long id;
    private String comment;
    private Integer duration;
    private Boolean active;
    private List<String> details;
    private PrescriptionInfoTypeVO prescriptionInfoType;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
    private FrequencyVO frequency;
    private List<NotificationDetailsVO> notifications = new ArrayList<>();

    public PrescriptionInfoDetailVO(List<PrescriptionDetailInfoV> detailInfoVS) {
        if (CollectionUtils.isNotEmpty(detailInfoVS)) {
            PrescriptionDetailInfoV detailInfoV = detailInfoVS.get(0);
            this.id = detailInfoV.getPrescriptionInfoId();
            this.comment = detailInfoV.getPresInfoComment();
            this.duration = detailInfoV.getCourseDuration();
            if (detailInfoV.getIntakeTime() != null) {
                this.details = detailInfoV.getIntakeTime().getDetails();
            }
            this.prescriptionInfoType = new PrescriptionInfoTypeVO(detailInfoV);
            List<Long> notificationIds = detailInfoVS.stream().map(PrescriptionDetailInfoV::getNotificationId).distinct().collect(Collectors.toList());
            List<NotificationDetailsVO> notifications = new ArrayList<>();
            for (Long id : notificationIds) {
                List<PrescriptionDetailInfoV> prescriptionDetailInfoVS = detailInfoVS.stream().filter(detail -> detail.getNotificationId().equals(id)).collect(Collectors.toList());
                notifications.add(new NotificationDetailsVO(prescriptionDetailInfoVS));
            }
            this.notifications = notifications;
        }
    }

    public PrescriptionInfoDetailVO(PrescriptionInfo prescriptionInfo) {
        this.id = prescriptionInfo.getId();
        this.duration = prescriptionInfo.getCourseDuration();
        this.details = prescriptionInfo.getIntakeTime() != null ? prescriptionInfo.getIntakeTime().getDetails() : null;
        this.prescriptionInfoType = new PrescriptionInfoTypeVO(prescriptionInfo.getPrescriptionInfoType());
        this.active = prescriptionInfo.getActive();
        this.notifications = prescriptionInfo.getNotifications().stream().map(NotificationDetailsVO :: new).collect(Collectors.toList());
        this.createdDate = prescriptionInfo.getCreatedDate();
        this.updatedDate = prescriptionInfo.getUpdatedDate();
        this.frequency = new FrequencyVO(prescriptionInfo.getFrequency());
        this.comment = prescriptionInfo.getComment();
    }
 }
