package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionTypeVO {
    private Integer id;
    private String name;
    private String description;
    private ZonedDateTime createdDate;

    public PrescriptionTypeVO(PrescriptionDetailInfoV detailInfoV) {
        this.id = detailInfoV.getPresTypeId();
        this.name = detailInfoV.getPresType();
    }

    public PrescriptionTypeVO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
 }
