package com.sg.dayamed.prescription.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.config.BeanUtil;
import com.sg.dayamed.persistence.domain.IntakeTime;
import com.sg.dayamed.persistence.domain.Prescription;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import com.sg.dayamed.persistence.domain.PrescriptionInfoType;
import com.sg.dayamed.persistence.repository.PrescriptionInfoTypeRepository;
import com.sg.dayamed.vo.NotificationInformationVO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@Getter
@Setter
public class PrescriptionInfoVO {
    private Long id;
    private String name;
    private String comment;
    private Integer duration;
    private String identityCode;
    private String pharmacistComment;
    private List<String> details;
    private Long prescriptionInfoTypeId;
    private Boolean active;
    private List<NotificationInformationVO> notifications = new ArrayList<>();
    private FrequencyVO frequency;
    private List<IdIndexVO> deletedNotificationIds = new ArrayList<>();

    @JsonIgnore
    private PrescriptionInfoTypeRepository getPrescriptionInfoTypeRepository() {
        return BeanUtil.getBean(PrescriptionInfoTypeRepository.class);
    }

    public PrescriptionInfo toPrescriptionInfo(String zoneId, Prescription prescription) {
        PrescriptionInfo info = new PrescriptionInfo();
        info.setZoneId(zoneId);
        info.setComment(this.comment);
        info.setCourseDuration(this.duration);
        info.setId(this.id);
        if (CollectionUtils.isNotEmpty(this.details)) {
            IntakeTime intakeTime = new IntakeTime();
            intakeTime.setDetails(this.details);
            info.setIntakeTime(intakeTime);
        }
        info.setIdentityCode(this.identityCode);
        if (getPrescriptionInfoTypeRepository() != null && this.prescriptionInfoTypeId != null) {
            Optional<PrescriptionInfoType> prescriptionInfoTypeOptional = getPrescriptionInfoTypeRepository().findById(this.prescriptionInfoTypeId);
            if (prescriptionInfoTypeOptional.isPresent()) {
                info.setPrescriptionInfoType(prescriptionInfoTypeOptional.get());
            }
        }
        if (CollectionUtils.isNotEmpty(this.notifications)) {
            info.setNotifications(this.notifications.stream().map(notification -> notification.toPrescriptionNotifications(info, false)).collect(Collectors.toList()));
        }
        info.setFrequency(this.frequency != null ? this.frequency.toFrequencyDetails() : null);
        info.setPrescription(prescription);
        return info;
    }
}
