package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.persistence.domain.NotificationType;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 04/Mar/2019
 **/
@Getter
@Setter
public class PrescriptionNotificationVO {
    private Long id;
    private PrescriptionInfo prescriptionInfo;
    private String recipients;
    private Integer delayInMinutes;
    private NotificationType notificationType;
    private ZonedDateTime createdDate = ZonedDateTime.now();
    private ZonedDateTime updatedDate;
}
