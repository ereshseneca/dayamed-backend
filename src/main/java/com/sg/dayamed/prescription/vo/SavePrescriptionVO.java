package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.rest.ws.WSSavePrescriptionRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Eresh Gorantla on 02/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class SavePrescriptionVO {
    private Long patientId;
    private String zoneId;
    private List<PrescriptionVO> prescriptions;
    private List<IdIndexVO> deletedPrescriptionIds = new ArrayList<>();

    public SavePrescriptionVO(WSSavePrescriptionRequest request) {
        this.patientId = request.getPatientId() != null ? NumberUtils.toLong(request.getPatientId()) : null;
        this.zoneId = request.getZoneId();
        this.prescriptions = request.getPrescriptions().stream().map(pres -> pres.toPrescriptionVO(this.zoneId)).collect(Collectors.toList());
        this.deletedPrescriptionIds = IntStream.range(NumberUtils.INTEGER_ZERO, request.getDeletedPrescriptionIds().size()).mapToObj(index -> new IdIndexVO(index,
                request.getDeletedPrescriptionIds().get(index))).collect(Collectors.toList());
    }

}
