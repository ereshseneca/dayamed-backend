package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.persistence.domain.Prescription;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 04/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionBasicInfoVO {
    private Long id;
    private String name;
    private String providerComment;
    private String illnessDesc;
    private String diagnosis;
    private Boolean active;
    private Long patientId;
    private Long creatorId;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;

    public PrescriptionBasicInfoVO(Prescription prescription) {
        this.id = prescription.getId();
        this.name = prescription.getName();
        this.providerComment = prescription.getProviderComment();
        this.illnessDesc = prescription.getPatientIllnessDesc();
        this.diagnosis = prescription.getPatientIllnessDiagnosis();
        this.active = prescription.getActive();
        this.createdDate = prescription.getCreatedDate();
        this.updatedDate = prescription.getUpdatedDate();
    }
}
