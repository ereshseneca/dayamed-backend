package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.rest.ws.WSStartPrescriptionRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 04/Mar/2019
 **/
@Getter
@Setter
public class StartPrescriptionVO {
   private List<Long> prescriptionIds;
   private String zoneId;
   private Boolean isUpdated;

   public StartPrescriptionVO(WSStartPrescriptionRequest request) {
       this.prescriptionIds = request.getPrescriptionIds();
       this.zoneId = request.getZoneId();
       this.isUpdated = request.getIsUpdated();
   }
}
