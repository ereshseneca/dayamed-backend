package com.sg.dayamed.prescription.vo;

import com.sg.dayamed.persistence.domain.Prescription;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionInformationVO {
    private Long id;
    private String name;
    private String providerComment;
    private String illnessDesc;
    private String diagnosis;
    private Boolean active;
    private Long patientId;
    private String zoneId;
    private Long creatorId;
    private ZonedDateTime createdDate = ZonedDateTime.now();
    private ZonedDateTime updatedDate;
    private List<PrescriptionInfoDetailVO> details = new ArrayList<>();

    public PrescriptionInformationVO(List<PrescriptionDetailInfoV> detailInfoVS) {
        if (CollectionUtils.isNotEmpty(detailInfoVS)) {
            PrescriptionDetailInfoV detailInfoV = detailInfoVS.get(0);
            this.id = detailInfoV.getPrescriptionId();
            this.name = detailInfoV.getPrescriptionName();
            this.active = detailInfoV.getActivePrescription();
            this.providerComment = detailInfoV.getProviderComment();
            this.illnessDesc = detailInfoV.getPatientIllnessDesc();
            this.patientId = detailInfoV.getPatientId();
            this.creatorId = detailInfoV.getCreatorId();
            this.diagnosis = detailInfoV.getDiagnosis();
            List<Long> notificationInfoIds = detailInfoVS.stream().map(PrescriptionDetailInfoV::getPrescriptionInfoId).distinct().collect(Collectors.toList());
            List<PrescriptionInfoDetailVO> details = new ArrayList<>();
            for (Long id : notificationInfoIds) {
                List<PrescriptionDetailInfoV> prescriptionDetailInfoVS = detailInfoVS.stream().filter(detail -> detail.getPrescriptionInfoId().equals(id)).collect(Collectors.toList());
                details.add(new PrescriptionInfoDetailVO(prescriptionDetailInfoVS));
            }
            this.details = details;
        }
    }

    public PrescriptionInformationVO(Prescription prescription) {
        if (prescription != null) {
            this.id = prescription.getId();
            this.name = prescription.getName();
            this.active = prescription.getActive();
            this.providerComment = prescription.getProviderComment();
            this.illnessDesc = prescription.getPatientIllnessDesc();
            this.patientId = prescription.getPatient() != null ? prescription.getPatient().getId() : null;
            this.creatorId = prescription.getUserRoleMapping() != null ? prescription.getUserRoleMapping().getId() : null;
            this.diagnosis = prescription.getPatientIllnessDiagnosis();
            this.details = prescription.getPrescriptionInfoS().stream().map(PrescriptionInfoDetailVO::new).collect(Collectors.toList());
            this.createdDate = prescription.getCreatedDate();
            this.updatedDate = prescription.getUpdatedDate();
        }
    }
}
