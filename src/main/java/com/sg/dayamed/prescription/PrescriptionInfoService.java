package com.sg.dayamed.prescription;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.vo.StartPrescriptionVO;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/

public interface PrescriptionInfoService {
    public void addPrescriptionData() throws ApplicationException;
    public void generateData() throws ApplicationException ;
    void startPrescription(StartPrescriptionVO startPrescriptionVO, UserPrincipal userPrincipal) throws ApplicationException;
}
