package com.sg.dayamed.prescription;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.prescription.vo.PrescriptionInformationVO;
import com.sg.dayamed.prescription.vo.PrescriptionMinInfoVO;
import com.sg.dayamed.prescription.vo.SavePrescriptionVO;
import com.sg.dayamed.security.UserPrincipal;

import java.util.List;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/

public interface PrescriptionService {
    List<PrescriptionInformationVO> savePrescription(SavePrescriptionVO savePrescriptionVO, UserPrincipal userPrincipal) throws ApplicationException;
    PrescriptionMinInfoVO getPrescriptions(String patientId, UserPrincipal userPrincipal, Integer limit, Integer offset) throws ApplicationException;
    PrescriptionInformationVO getPrescriptionInfo(Long prescriptionId, UserPrincipal userPrincipal) throws ApplicationException;
}
