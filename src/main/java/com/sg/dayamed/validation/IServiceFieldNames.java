package com.sg.dayamed.validation;

/**
 * Created by Eresh Gorantla on 05/Apr/2019
 **/

public interface IServiceFieldNames {
    String DOT = ".";
    String ARRAY_INDEX = "[%d]";
    String PATIENT_ID = "patientId";
    String ROLE_MAP_ID = "mapId";
    String PRESCRIPTION_ID = "prescriptionId";
    String ROLE = "role";
    String ID = "id";
    String SAVE_PRESCRIPTION_PRESCRIPTIONS = new StringBuilder("prescriptions").append(ARRAY_INDEX).toString();
    String SAVE_PRESCRIPTION_PRESCRIPTIONS_ID = new StringBuilder(SAVE_PRESCRIPTION_PRESCRIPTIONS).append(DOT).append(PRESCRIPTION_ID).toString();
    String DELETED_PRESCRIPTION_IDS = new StringBuilder("deletedPrescriptionIds").append(ARRAY_INDEX).toString();
    String SAVE_PRESCRIPTION_PRESCRIPTION_INFOS = new StringBuilder(SAVE_PRESCRIPTION_PRESCRIPTIONS).append(DOT).append("prescriptionInformationS").append(ARRAY_INDEX).toString();
    String SAVE_PRESCRIPTION_INFO_ID = new StringBuilder(SAVE_PRESCRIPTION_PRESCRIPTION_INFOS).append(DOT).append(ID).toString();
    String DELETED_PRESCRIPTION_INFO_IDS = new StringBuilder(SAVE_PRESCRIPTION_PRESCRIPTIONS).append(DOT).append("deletedPrescriptionInfoIds").append(ARRAY_INDEX).toString();
    String NOTIFICATIONS = new StringBuilder("notifications").append(ARRAY_INDEX).toString();
    String SAVE_PRESCRIPTION_NOTIFICATION_IDS = new StringBuilder(SAVE_PRESCRIPTION_PRESCRIPTION_INFOS).append(DOT).append(NOTIFICATIONS).append(DOT).append(ID).toString();
    String DELETED_NOTIFICATIONS_IDS = new StringBuilder(SAVE_PRESCRIPTION_PRESCRIPTION_INFOS).append(DOT).append("deletedNotifications").append(ARRAY_INDEX).toString();
    String PRESCRIPTION_IDS = "prescriptionIds";
    String START_PRESCRIPTION_PRESCRIPTION_IDS = new StringBuilder(PRESCRIPTION_IDS).append(ARRAY_INDEX).toString();
    String FREQUENCY = "frequency";
    String PRESCRIPTION_INFO_FREQUENCY = new StringBuilder(SAVE_PRESCRIPTION_PRESCRIPTION_INFOS).append(DOT).append(FREQUENCY).toString();
    String ACTUAL_VALUES = "actualValues";
    String PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES = new StringBuilder(PRESCRIPTION_INFO_FREQUENCY).append(DOT).append(ACTUAL_VALUES).toString();
    String PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES_VALUE = new StringBuilder(PRESCRIPTION_INFO_FREQUENCY_ACTUAL_VALUES).append(ARRAY_INDEX).toString();

}
