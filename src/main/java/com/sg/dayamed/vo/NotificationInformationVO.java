package com.sg.dayamed.vo;

import com.sg.dayamed.config.BeanUtil;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import com.sg.dayamed.persistence.domain.PrescriptionNotifications;
import com.sg.dayamed.persistence.repository.NotificationTypeRepository;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 04/Mar/2019
 **/
@Getter
@Setter
public class NotificationInformationVO {
    private Integer notificationTypeId;
    private List<String> recipients = new ArrayList<>();
    private Integer delayInMinutes;
    private Long id;

    private NotificationTypeRepository getNotificationTypeRepository() {
        return BeanUtil.getBean(NotificationTypeRepository.class);
    }

    public PrescriptionNotifications toPrescriptionNotifications(PrescriptionInfo prescriptionInfo, Boolean defaultFlag) {
        PrescriptionNotifications notifications = new PrescriptionNotifications();
        notifications.setDelayInMinutes(this.delayInMinutes);
        notifications.setRecipients(this.recipients.stream().collect(Collectors.joining(",")));
        notifications.setDefaultFlag(defaultFlag);
        if (this.notificationTypeId != null) {
            notifications.setNotificationType(getNotificationTypeRepository().findById(notificationTypeId).get());
        }
        notifications.setPrescriptionInfo(prescriptionInfo);
        notifications.setId(this.id);
        return notifications;
    }
}
