package com.sg.dayamed.vo;

import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.rest.ws.WSRoleMappingInformation;
import com.sg.dayamed.util.UserRole;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@Getter
@Setter
public class UserRoleMappingVO {
    private Long userId;
    private Integer roleId;
    private String role;
    private UserInformationVO userRoleInformation;

    /*public UserRoleMappingVO(UserRoleMapping userRoleMapping) {
        if (userRoleMapping != null) {
            *//*this.roleId = userRoleMapping.getRoleId();
            this.userId = userRoleMapping.getUserId();*//*
            this.userRoleInformation = new UserRoleInformationVO(userRoleMapping.getUserRoleInformation());
        }
    }*/

    public UserRoleMappingVO(UserInformationV userInformationV) {
        UserRole userRole = UserRole.getUserRole(userInformationV.getRoleId());
        if (userRole != null) {
            this.roleId = userInformationV.getRoleId();
            this.role = userInformationV.getRoleName();
            this.userRoleInformation = new UserInformationVO(userInformationV, userRole);
        }
    }

    public WSRoleMappingInformation toWsUserRoleMapping() {
        WSRoleMappingInformation roleMapping = new WSRoleMappingInformation();
        UserRole userRole = UserRole.getUserRole(this.roleId);
        if (userRole != null) {
            roleMapping.setRoleId(this.roleId);
            roleMapping.setRole(this.role);
            roleMapping.setUserId(this.userId);
            //roleMapping.setUserRoleInformation(this.userRoleInformation != null ? this.userRoleInformation.toWSUserInformation() : null);
        }
        return roleMapping;
    }
}
