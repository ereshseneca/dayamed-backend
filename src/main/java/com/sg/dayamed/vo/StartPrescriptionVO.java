package com.sg.dayamed.vo;

import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.rest.ws.WSStartPrescriptionRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Eresh Gorantla on 04/Mar/2019
 **/
@Getter
@Setter
public class StartPrescriptionVO {
   private List<IdIndexVO> presIdIndexes;
   private List<Long> prescriptionIds;
   private String zoneId;
   private Boolean isUpdated;
   private Boolean forceStart;

   public StartPrescriptionVO(WSStartPrescriptionRequest request) {
       this.presIdIndexes = request.getPrescriptionIds() != null ? IntStream.range(0, request.getPrescriptionIds().size()).mapToObj(index -> new IdIndexVO(index,
               request.getPrescriptionIds().get(index))).collect(Collectors.toList()) : null;
       this.prescriptionIds = request.getPrescriptionIds();
       this.zoneId = request.getZoneId();
       this.isUpdated = request.getIsUpdated();
       this.forceStart = request.getForceStart();
   }
}
