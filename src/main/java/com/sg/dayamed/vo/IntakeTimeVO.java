package com.sg.dayamed.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
public class IntakeTimeVO {
    private List<String> details;
}
