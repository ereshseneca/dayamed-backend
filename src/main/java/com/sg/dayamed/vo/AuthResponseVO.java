package com.sg.dayamed.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class AuthResponseVO {
    private String accessKey;
    private UserDetailInformationVO information;
    private List<String> roles = new ArrayList<>();
    private List<UserDetailsVO> patients = new ArrayList<>();
}
