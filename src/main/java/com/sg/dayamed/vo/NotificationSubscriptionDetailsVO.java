package com.sg.dayamed.vo;

import com.sg.dayamed.persistence.domain.view.NotificationSubscriptionDetailsV;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@Getter
@Setter
public class NotificationSubscriptionDetailsVO {
    private String id;
    private Integer notificationTypeId;
    private String notificationType;
    private String notificationSubscriptionId;
    private String notificationSubscription;
    private Boolean defaultFlag;
    private ZonedDateTime nsCreatedDate;
    private Long nsdId;
    private Integer notificationDelay;
    private Integer priority;
    private ZonedDateTime nsdCreatedDate;

    public NotificationSubscriptionDetailsVO(NotificationSubscriptionDetailsV detailsV) {
        this.id = detailsV.getId();
        this.notificationTypeId = detailsV.getNotificationTypeId();
        this.notificationType = detailsV.getNotificationType();
        this.notificationSubscriptionId = detailsV.getNotificationSubscriptionId();
        this.notificationSubscription = detailsV.getNotificationSubscription();
        this.defaultFlag = detailsV.getDefaultFlag();
        this.nsCreatedDate = detailsV.getNsCreatedDate();
        this.nsdId = detailsV.getNsdId();
        this.notificationDelay = detailsV.getNotificationDelay();
        this.priority = detailsV.getPriority();
        this.nsdCreatedDate = detailsV.getNsdCreatedDate();
    }
}
