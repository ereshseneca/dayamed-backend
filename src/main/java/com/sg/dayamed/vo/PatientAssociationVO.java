package com.sg.dayamed.vo;

import com.sg.dayamed.persistence.domain.PatientAssociations;
import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import com.sg.dayamed.rest.ws.WSPatientAssociation;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 23/Feb/2019
 **/
@Getter
@Setter
public class PatientAssociationVO {
    private Long id;
    private Long roleMappingId;
    private String role;
    private Boolean active;
    private ZonedDateTime createdDate;

    public PatientAssociationVO(PatientAssociations associations) {
        this.id = associations.getId();
        this.roleMappingId = associations.getUserRoleMapId();
        this.active = associations.getActive();
        this.createdDate = associations.getCreatedDate();
    }

    public PatientAssociationVO(PatientAssociationInformationV informationV) {
        this.id = informationV.getAssociationId();
        this.roleMappingId = informationV.getUserRoleMapId();
        this.role = informationV.getRole();
        this.active = informationV.getActive();
        this.createdDate = informationV.getAssociationCreatedDate();
    }

    public WSPatientAssociation toWSPatientAssociation() {
        WSPatientAssociation association = new WSPatientAssociation();
        association.setActive(this.active);
        association.setCreatedDate(this.createdDate);
        association.setId(this.id);
        association.setRole(this.role);
        association.setRoleMappingId(this.roleMappingId);
        return association;
    }
}
