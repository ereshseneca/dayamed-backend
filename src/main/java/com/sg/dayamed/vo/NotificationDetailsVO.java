package com.sg.dayamed.vo;

import com.sg.dayamed.persistence.domain.PrescriptionNotifications;
import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class NotificationDetailsVO {
    private Long id;
    private String recipients;
    private Integer delayInMinutes;
    private Boolean defaultNotification;
    private NotificationTypeVO type;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;


    public NotificationDetailsVO(List<PrescriptionDetailInfoV> detailInfoVS) {
        if (CollectionUtils.isNotEmpty(detailInfoVS)) {
            PrescriptionDetailInfoV detailInfoV = detailInfoVS.get(0);
            this.id = detailInfoV.getNotificationId();
            this.recipients = detailInfoV.getRecipients();
            this.delayInMinutes = detailInfoV.getNotificationDelay();
            this.defaultNotification = detailInfoV.getDefaultNotification();
            this.createdDate = detailInfoV.getNotificationCreatedDate();
            this.type = new NotificationTypeVO(detailInfoV);
        }
    }

    public NotificationDetailsVO(PrescriptionNotifications notifications) {
        if (notifications != null) {
            this.id = notifications.getId();
            this.recipients = notifications.getRecipients();
            this.defaultNotification = notifications.getDefaultFlag();
            if (notifications.getNotificationType() != null)
                this.type = new NotificationTypeVO(notifications.getNotificationType().getId(), notifications.getNotificationType().getName());
            this.createdDate = notifications.getCreatedDate();
            this.updatedDate = notifications.getUpdatedDate();
        }
    }
}
