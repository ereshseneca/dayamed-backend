package com.sg.dayamed.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Eresh Gorantla on 22/Feb/2019
 **/
@Getter
@Setter
@ToString
public class PatientAssociationError {
    private String caretakers = "Caretakers => ";
    private String pharmacists = "Pharmacists => ";
    private String providers = "Providers => ";


}
