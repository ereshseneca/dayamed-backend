package com.sg.dayamed.vo;

import com.sg.dayamed.persistence.domain.State;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@Setter
@Getter
public class StateVO {
    private Long id;
    private String name;
    private CountryVO country;
    private ZonedDateTime createdDate;

    public StateVO(State state, boolean includeCountry) {
        if (state != null) {
            if (includeCountry) {
                this.country = new CountryVO(state.getCountry());
            }
            this.createdDate = state.getCreatedDate();
            this.id = state.getId();
            this.name = state.getName();
        }
    }
}
