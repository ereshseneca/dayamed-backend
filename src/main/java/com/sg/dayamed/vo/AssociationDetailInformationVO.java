package com.sg.dayamed.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 01/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class AssociationDetailInformationVO {
    private String type;
    private List<Long> ids;
    private List<String> names;
    private List<String> recipients;
}
