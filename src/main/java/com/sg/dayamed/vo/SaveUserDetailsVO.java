package com.sg.dayamed.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 22/Feb/2019
 **/
@Getter
@Setter
public class SaveUserDetailsVO {
    private UserDetailsVO userDetails;
    private UserInformationVO userInformation;
    private List<Long> patientAssociations;
    private Integer roleId;
    private Boolean isUpdate;

    public Boolean getIsUpdate() {
        return userDetails.getId() != null;
    }
}
