package com.sg.dayamed.vo;

import com.sg.dayamed.rest.ws.WSLoginRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class AuthRequestVO {
    private String emailId;
    private String password;
    private Integer roleId;

    public AuthRequestVO(WSLoginRequest request) {
        this.emailId = request.getEmailId();
        this.password = request.getPassword();
        this.roleId = request.getRoleId();
    }
}
