package com.sg.dayamed.vo;

import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class NotificationTypeVO {
    private Integer id;
    private String type;

    public NotificationTypeVO(PrescriptionDetailInfoV detailInfoV) {
        this.id = detailInfoV.getNotificationTypeId();
        this.type = detailInfoV.getNotificationType();
    }

    public NotificationTypeVO(Integer id, String name) {
        this.id = id;
        this.type = name;
    }
}
