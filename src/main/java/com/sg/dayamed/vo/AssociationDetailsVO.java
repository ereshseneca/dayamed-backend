package com.sg.dayamed.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 01/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class AssociationDetailsVO {
    private AssociationDetailInformationVO provider;
    private AssociationDetailInformationVO caretaker;
}
