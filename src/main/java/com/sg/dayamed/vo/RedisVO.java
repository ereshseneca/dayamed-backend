package com.sg.dayamed.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 18/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RedisVO {
    private Integer id;
    private String name;
}
