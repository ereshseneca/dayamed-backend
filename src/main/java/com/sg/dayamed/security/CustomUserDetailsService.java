package com.sg.dayamed.security;

import com.sg.dayamed.commons.constants.IWSGlobalApiErrorKeys;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.persistence.repository.UserInformationVRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserInformationVRepository informationVRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        try {
            List<UserInformationV> userInformationVS = informationVRepository.findByUserEmailId(usernameOrEmail);
            if (CollectionUtils.isEmpty(userInformationVS)) {
                throw new DataValidationException("username", IWSGlobalApiErrorKeys.ERRORS_AUTHENTICATION_FAILED);
            }
            User user = getUser(userInformationVS);
            return UserPrincipal.create(user);
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        List<UserInformationV> informationVS = informationVRepository.findByUserId(id);
        if (CollectionUtils.isEmpty(informationVS)) {
            //throw new ResourceNotFoundException("User", "id", id);
        }
        User user = getUser(informationVS);
        return UserPrincipal.create(user);
    }

    private User getUser(List<UserInformationV> informationVS) {
        UserInformationV informationV = informationVS.get(0);
        User user = new User();
        user.setEmail(informationV.getUserEmailId());
        user.setId(informationV.getUserId());
        user.setName(informationV.getUserFirstName());
        user.setPassword(informationV.getUserPassword());
        user.setRoleMappingId(informationV.getRoleMappingId());
        user.setRoles(informationVS.stream().map(Role::new).limit(1).collect(Collectors.toList()));
        return user;
    }

}
