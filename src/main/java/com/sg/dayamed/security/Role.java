package com.sg.dayamed.security;

import com.sg.dayamed.persistence.domain.view.UserInformationV;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class Role {
    private Long mapId;
    private Integer roleId;
    private String name;

    public Role(UserInformationV informationV) {
        this.mapId = informationV.getRoleMappingId();
        this.roleId = informationV.getRoleId();
        this.name = informationV.getRoleName();
    }
}
