package com.sg.dayamed;

import com.sg.dayamed.commons.constants.IWSGlobalApiErrorKeys;
import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.persistence.repository.PatientAssociationInformationVRepository;
import com.sg.dayamed.persistence.repository.UserInformationVRepository;
import com.sg.dayamed.security.JwtTokenProvider;
import com.sg.dayamed.security.Role;
import com.sg.dayamed.security.User;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.vo.AuthRequestVO;
import com.sg.dayamed.vo.AuthResponseVO;
import com.sg.dayamed.vo.UserDetailInformationVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    UserInformationVRepository informationVRepository;

    @Autowired
    PatientAssociationInformationVRepository associationInformationVRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public AuthResponseVO login(AuthRequestVO requestVO) throws ApplicationException {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(requestVO.getEmailId(), requestVO.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = tokenProvider.generateToken(authentication);
            List<UserInformationV> informationVS = informationVRepository.findByUserEmailId(requestVO.getEmailId());
            UserInformationV informationV = informationVS.get(0);
            UserDetailInformationVO informationVO = new UserDetailInformationVO(informationVS, informationV.getUserId(), null);
            AuthResponseVO authResponseVO = new AuthResponseVO();
            authResponseVO.setAccessKey(jwt);
            authResponseVO.setInformation(informationVO);
            authResponseVO.setRoles(authentication.getAuthorities().stream().map(auth -> ((GrantedAuthority) auth).getAuthority()).collect(Collectors.toList()));
            return authResponseVO;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    public AuthResponseVO signIn(AuthRequestVO requestVO) throws ApplicationException {
        try {
            List<UserInformationV> informationVS = informationVRepository.findByUserEmailId(requestVO.getEmailId());
            if (CollectionUtils.isEmpty(informationVS)) {
                throw new DataValidationException("emailId", IWSGlobalApiErrorKeys.ERRORS_AUTHENTICATION_FAILED);
            }
            UserInformationV informationV = null;
            if (requestVO.getRoleId() == null) {
                informationV = informationVS.get(0);
            } else {
                informationVS = informationVS.stream().filter(info -> info.getRoleId().equals(requestVO.getRoleId())).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(informationVS)) {
                    throw new DataValidationException("emailId", APIErrorKeys.ERROR_USER_DOES_NOT_EXISTS_FOR_ROLE);
                }
                informationV = informationVS.get(0);
            }
            if (!passwordEncoder.matches(requestVO.getPassword(), informationV.getUserPassword())) {
                throw new DataValidationException("password", IWSGlobalApiErrorKeys.ERRORS_AUTHENTICATION_FAILED);
            }
            User user = getUser(informationV);
            UserPrincipal userPrincipal = UserPrincipal.create(user);
            String jwt = tokenProvider.generateToken(userPrincipal);
            UserDetailInformationVO informationVO = new UserDetailInformationVO(informationVS, informationV.getUserId(), null);
            AuthResponseVO authResponseVO = new AuthResponseVO();
            authResponseVO.setAccessKey(jwt);
            authResponseVO.setInformation(informationVO);
            authResponseVO.setRoles(userPrincipal.getAuthorities().stream().map(auth -> auth.getAuthority()).collect(Collectors.toList()));
            return authResponseVO;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private User getUser(UserInformationV informationV) {
        User user = new User();
        user.setEmail(informationV.getUserEmailId());
        user.setId(informationV.getUserId());
        user.setName(informationV.getUserFirstName());
        user.setPassword(informationV.getUserPassword());
        user.setRoleMappingId(informationV.getRoleMappingId());
        user.setRoles(Stream.of(new Role(informationV)).collect(Collectors.toList()));
        return user;
    }

}
