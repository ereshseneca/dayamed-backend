package com.sg.dayamed.crypto;

import com.sg.dayamed.jpa.converters.CipherInitializer;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.util.Base64;

/**
 * Created by Eresh Gorantla on 20/Mar/2019
 **/
@Component
public class CryptoUtil {

    CipherInitializer cipherInitializer = new CipherInitializer();

    protected String encrypt(Cipher cipher, String value) {
        try {
            byte[] bytesToEncrypt = value.getBytes();
            byte[] encryptedBytes = callCipherDoFinal(cipher, bytesToEncrypt);
            return Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (Exception e) {
            return null;
        }
    }

    byte[] callCipherDoFinal(Cipher cipher, byte[] bytes) throws IllegalBlockSizeException, BadPaddingException {
        return cipher.doFinal(bytes);
    }

    public String decrypt(Cipher cipher, String dbData) {
        try {
            byte[] encryptedBytes = Base64.getDecoder().decode(dbData);
            byte[] decryptedBytes = callCipherDoFinal(cipher, encryptedBytes);
            return new String(decryptedBytes);
        } catch (Exception e) {
            return null;
        }
    }

    public String encryptData(String text) {
        try {
            Cipher cipher = cipherInitializer.prepareAndInitCipher(Cipher.ENCRYPT_MODE, "ZY6PiJQi9AM8ohNIx4YPbC0iFbLIRInw");
            return encrypt(cipher, text);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String decryptData(String text) {
        try {
            Cipher cipher = cipherInitializer.prepareAndInitCipher(Cipher.DECRYPT_MODE, "ZY6PiJQi9AM8ohNIx4YPbC0iFbLIRInw");
            return decrypt(cipher, text);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
