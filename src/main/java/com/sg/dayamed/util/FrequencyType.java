package com.sg.dayamed.util;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 10/Apr/2019
 **/
@Getter
public enum FrequencyType {
    DAILY(1, "Daily"), WEEKLY(2, "Weekly"), MONTHLY(3, "Monthly"), YEARLY(4, "Yearly");

    Integer id;
    String value;

    private FrequencyType(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static FrequencyType getFrequencyType(Integer id) {
        FrequencyType[] frequencyTypes = FrequencyType.values();
        return Arrays.stream(frequencyTypes).filter(status -> status.getId().equals(id)).findAny().orElse(null);
    }
}
