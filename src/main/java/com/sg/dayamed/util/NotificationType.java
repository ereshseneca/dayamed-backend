package com.sg.dayamed.util;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Getter
public enum NotificationType {
    EMAIL(1, "Email"), SMS(2, "Sms"), VIDEO_CALL(3, "Video call"), PUSH_NOTIFICATION(4, "Push notification");
    Integer id;
    String value;

    private NotificationType(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static NotificationType getNotificationType(String type) {
        NotificationType[] notificationTypes = NotificationType.values();
        return Arrays.stream(notificationTypes).filter(value -> StringUtils.equalsIgnoreCase(type, value.getValue())).findAny().orElse(null);
    }
    public static NotificationType getNotificationType(Integer id) {
        NotificationType[] notificationTypes = NotificationType.values();
        return Arrays.stream(notificationTypes).filter(value -> value.equals(id)).findAny().orElse(null);
    }

}