package com.sg.dayamed.util;

import com.sg.dayamed.commons.constants.IWSGlobalApiErrorKeys;
import com.sg.dayamed.commons.crypto.CryptoUtil;
import com.sg.dayamed.commons.exception.DataValidationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/

public final class ServiceUtils {

    private ServiceUtils() {

    }

    public static Long getId(String decryptedText, String idText) throws DataValidationException {
        String id = CryptoUtil.decryptIdField(decryptedText);
        if (StringUtils.isBlank(id)) {
            throw new DataValidationException(idText, IWSGlobalApiErrorKeys.ERRORS_GENERAL_ERROR_UNEXPECTED);
        }
        return NumberUtils.toLong(id);
    }

}
