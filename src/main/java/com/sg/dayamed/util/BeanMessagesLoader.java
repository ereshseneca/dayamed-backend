package com.sg.dayamed.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Component
public class BeanMessagesLoader {

    @Autowired
    private LocalValidatorFactoryBean validatorFactoryBean;

    @Autowired
    private MessageSource messageSource;

    public String getMessage(String key) {
        return validatorFactoryBean.getValidationPropertyMap().get(key);
    }
}
