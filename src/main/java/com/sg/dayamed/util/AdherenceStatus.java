package com.sg.dayamed.util;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 20/Feb/2019
 **/
@Getter
public enum AdherenceStatus {
    INITIALIZE(0, "Initialize"), CONSUME(1, "Consume"), DELAY(2, "Delay"), AUTO_DELAY(3, "AutoDelay"), SKIP(4, "Skip");
    Integer id;
    String value;

    private AdherenceStatus(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static AdherenceStatus getAdherenceStatus(Integer id) {
        AdherenceStatus[] adherenceStatuses = AdherenceStatus.values();
        return Arrays.stream(adherenceStatuses).filter(status -> status.getId().equals(id)).findAny().orElse(null);
    }
}
