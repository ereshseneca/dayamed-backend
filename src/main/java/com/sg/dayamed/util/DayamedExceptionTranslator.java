package com.sg.dayamed.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.commons.component.CommonContext;
import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.commons.exception.ExceptionLogUtil;
import com.sg.dayamed.commons.exception.UnexpectedException;
import com.sg.dayamed.commons.rest.RestApiException;
import com.sg.dayamed.commons.rest.RestFault;
import com.sg.dayamed.commons.rest.ServiceMethod;
import com.sg.dayamed.commons.util.ErrorKeyToResponseStatusMapper;
import com.sg.dayamed.commons.util.RestApiExceptionGenerator;
import com.sg.dayamed.persistence.domain.ErrorLog;
import com.sg.dayamed.persistence.repository.ErrorLogRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 17/Feb/2019
 **/

@ControllerAdvice
public class DayamedExceptionTranslator {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("jsonMapper")
    ObjectMapper mapper;

    private final String DEFAULT_SERVICE = "SYSTEM ERRORS";
    private final String DEFAULT_SERVICE_METHOD = "SYSTEM ERRORS";

    @Autowired
    MessageSource messageSource;

    @Autowired
    CommonContext context;

    @Autowired
    ErrorLogRepository errorLogRepository;

    @Autowired
    @Qualifier("errorLogs")
    ExecutorService errorLogs;


    @ExceptionHandler({ConstraintViolationException.class})
    protected ResponseEntity<RestFault> handleConstraintViolation(ConstraintViolationException ex, HttpServletRequest request) {
        RestApiException restApiException = null;
        List<ConstraintViolation> violations = ex.getConstraintViolations().stream().collect(Collectors.toList());
        ConstraintViolation constraintViolation = CollectionUtils.isNotEmpty(violations) ? violations.get(0) : null;
        Path path = constraintViolation.getPropertyPath();
        PathImpl node = (PathImpl) path;
        DataValidationException validationException = new DataValidationException(node.getLeafNode().asString(), null);
        String errorMessage = constraintViolation.getMessage();
        validationException.setMessage(errorMessage);
        restApiException = new RestApiException(validationException);
        if (restApiException == null) {
            restApiException = new RestApiException(ex.getMessage(), RestApiException.createUnexpectedFault(new Exception(ex)));
        }

        RestFault fault = restApiException.getFaultInfo();
        if (StringUtils.isBlank(fault.getErrorKey())) {
            fault.setErrorKey(ErrorKeyToResponseStatusMapper.ResponseStatus.BAD_REQUEST.getResult());
        }
        int status = ErrorKeyToResponseStatusMapper.getResponseStatus(fault.getErrorKey()).getStatus();
        fault.setUid(UUID.randomUUID().toString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("error_hdr", this.toJson(fault));
        RestApiException apiException = restApiException;
        CompletableFuture.runAsync(() -> saveErrorLog(apiException, fault, status, request), errorLogs);
        return new ResponseEntity(fault, headers, HttpStatus.valueOf(status));
    }


    @ExceptionHandler({MethodArgumentNotValidException.class})
    protected ResponseEntity<RestFault> handleConflict(MethodArgumentNotValidException ex, HttpServletRequest request) {
        RestApiException restApiException = null;
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        fieldErrors = fieldErrors.stream().sorted(Comparator.comparing(FieldError::getField)).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(fieldErrors)) {
            FieldError error = (FieldError) fieldErrors.get(0);
            DataValidationException validationException = new DataValidationException(error.getField(), null);
            String errorMessage;
            if (StringUtils.contains(error.getDefaultMessage(), "{0}")) {
                errorMessage = MessageFormat.format(error.getDefaultMessage(), new Object[]{error.getRejectedValue().toString()});
            } else {
                errorMessage = error.getDefaultMessage();
            }
            validationException.setMessage(errorMessage);
            restApiException = new RestApiException(validationException);
        }

        if (restApiException == null) {
            restApiException = new RestApiException(ex.getMessage(), RestApiException.createUnexpectedFault(new Exception(ex)));
        }

        RestFault fault = restApiException.getFaultInfo();
        if (StringUtils.isBlank(fault.getErrorKey())) {
            fault.setErrorKey(ErrorKeyToResponseStatusMapper.ResponseStatus.BAD_REQUEST.getResult());
        }
        int status = ErrorKeyToResponseStatusMapper.getResponseStatus(fault.getErrorKey()).getStatus();
        fault.setUid(UUID.randomUUID().toString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("error_hdr", this.toJson(fault));
        RestApiException apiException = restApiException;
        CompletableFuture.runAsync(() -> saveErrorLog(apiException, fault, status, request), errorLogs);
        return new ResponseEntity(fault, headers, HttpStatus.valueOf(status));
    }

    @ExceptionHandler(value = {RestApiException.class})
    protected ResponseEntity<RestFault> handleException(RestApiException ex, Locale locale, HttpServletRequest request) throws Exception {
        RestApiException restApiException = null;
        Throwable rootCause = ExceptionLogUtil.getRootCause(ex);

        if (rootCause instanceof RestApiException) {
            restApiException = (RestApiException) rootCause;
            if (StringUtils.isBlank(restApiException.getFaultInfo().getErrorMessage())) {
                String message = getErrorMessageFromResource(restApiException, locale);
                restApiException.getFaultInfo().setErrorMessage(message);
            }
        } else if (rootCause instanceof DataValidationException) {
            restApiException = new RestApiException((DataValidationException) rootCause);
        } else if (rootCause instanceof ApplicationException) {
            restApiException = new RestApiException(rootCause.getMessage(), RestApiException.createApplicationExceptionFault((ApplicationException) rootCause));
        }
        // Exception is unknown.
        if (restApiException == null) {
            restApiException = new RestApiException(ex.getMessage(), RestApiException.createUnexpectedFault(new Exception(ex)));
        }
        RestFault fault = restApiException.getFaultInfo();
        if (ArrayUtils.isNotEmpty(fault.getErrorParameters())) {
            fault.setErrorMessage(MessageFormat.format(fault.getErrorMessage(), fault.getErrorParameters()));
            fault.setErrorParameters(null);
        }
        if (StringUtils.isBlank(fault.getErrorMessage())) {
            fault.setErrorMessage("UnExpected System Exception Occurred");
        }

        // Map error key into status.
        int status = ErrorKeyToResponseStatusMapper.getResponseStatus(fault.getErrorKey()).getStatus();
        fault.setUid(UUID.randomUUID().toString());

        // Add content type and x-cub-hdr to Http headers.
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("error_hdr", toJson(fault));
        RestApiException apiException = restApiException;
        CompletableFuture.runAsync(() -> saveErrorLog(apiException, fault, status, request), errorLogs);

        // Return the response entity with fault response, headers and status.
        return new ResponseEntity<RestFault>(fault, headers, HttpStatus.valueOf(status));
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<RestFault> handleRootException(Exception ex, HttpServletRequest request) throws Exception {
        RestApiException restApiException = RestApiExceptionGenerator.generateRestApiException(ex, true);
        RestFault fault = restApiException.getFaultInfo();
        // Map error key into status.
        int status = ErrorKeyToResponseStatusMapper.getResponseStatus(fault.getErrorKey()).getStatus();
        fault.setUid(UUID.randomUUID().toString());
        // Add content type and x-cub-hdr to Http headers.
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("error_hdr", toJson(fault));
        RestApiException apiException = restApiException;
        CompletableFuture.runAsync(() -> saveErrorLog(apiException, fault, status, request), errorLogs);
        // Return the response entity with fault response, headers and status.
        return new ResponseEntity<RestFault>(fault, headers, HttpStatus.valueOf(status));
    }

    public String toJson(Object entity) {
        try {
            return mapper.writeValueAsString(entity);
        } catch (JsonProcessingException e) {
            throw new UnexpectedException("Error converting entity to JSON string", e);
        }
    }

    @Async
    public void saveErrorLog(RestApiException exception, RestFault restFault, Integer statusCode, HttpServletRequest request) {
        try {
            ServiceMethod serviceMethod = exception.getMethod();
            ErrorLog errorLog = new ErrorLog();
            errorLog.setHttpMethod(context.getHttpMethod(request));
            errorLog.setServiceName(serviceMethod != null ? serviceMethod.getServiceName() : DEFAULT_SERVICE);
            errorLog.setMethodName(serviceMethod != null ? serviceMethod.getMethodName() : DEFAULT_SERVICE_METHOD);
            errorLog.setRequestHeader(context.getClientHttpHeaders(request));
            errorLog.setRequestUri(context.getUri(request));
            errorLog.setResponse(toJson(restFault));
            errorLog.setStacktrace(ExceptionUtils.getStackTrace(exception));
            String message = ExceptionUtils.getMessage(exception);
            message = StringUtils.length(message) > 250 ? StringUtils.substring(message, 0, 249) : message;
            errorLog.setErrorMessage(message);
            errorLog.setStatusCode(statusCode);
            errorLog.setUserAgent(context.getHttpHeader("User-Agent", request));
            errorLogRepository.save(errorLog);
        } catch (Exception e) {
            logger.error("Error while saving to error_log", ExceptionUtils.getStackTrace(e));
        }
    }

    private String getErrorMessageFromResource(RestApiException restApiException, Locale locale) {
        try {
            return messageSource.getMessage(restApiException.getFaultInfo().getErrorKey(), null, locale);
        } catch (Exception e) {
            return null;
        }
    }
}
