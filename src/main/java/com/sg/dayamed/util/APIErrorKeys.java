package com.sg.dayamed.util;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/

public interface APIErrorKeys {
    String ERROR_PATIENT_PRESCRIPTION_DETAILS_NOT_FOUND = "patient.prescription.details.not.matched";
    String ERROR_PRESCRIPTION_DETAIL_NOT_FOUND = "error.prescription.detail.not.found";
    String ERROR_USER_DETAILS_EMAIL_ALREADY_REGISTERED="error.user.details.email.already.registered";
    String ERROR_PATIENT_MAPPING_ASSOCIATIONS_ALREADY_EXISTS="error.patient.associated.mappings.already.exist";
    String ERROR_USER_DETAILS_NOT_FOUND_FOR_ID="error.user.details.not.found.for.id";
    String ERROR_USER_ROLE_INVALID="error.user.role.invalid";
    String ERROR_PATIENT_CANNOT_BE_ASSOCIATED_WITH_ANOTHER_PATIENT = "error.patient.cannot.be.associated.with.another.patient";
    String ERROR_PATIENT_ASSOCIATIONS_ARE_REQUIRED="user.patient.associations.required";
    String ERROR_USER_IS_ALREADY_MAPPED_WITH_THIS_ROLE = "errors.user.is.already.mapped.with.this.role";
    String ERROR_PATIENT_PRESCRIPTION_IS_ALREADY_ACTIVE = "errors.patient.prescription.is.already.active";
    String ERROR_PRESCRIPTION_CANNOT_BE_ADDED_OTHER_THAN_PATIENTS="errors.prescription.cannot.be.added.other.than.patients";
    String ERROR_USER_AUTHENTICATION_FAILED = "errors.user.authentication.failed";
    String ERROR_NO_PRIVILEGES_TO_VIEW_PATIENT_DATA = "errors.no.privileges.to.view.patient.data";
    String ERROR_USER_DOES_NOT_EXISTS_FOR_ROLE = "errors.user.does.not.exist.for.the.role";
    String ERROR_PATIENT_CANNOT_ADD_PRESCRIPTION_FOR_ANOTHER_PATIENT="errors.patient.cannot.add.prescription.for.another.patient";
    String ERROR_USER_ROLE_MAP_ID_NOT_EXISTS="errors.user.role.map.id.not.exists";
    String ERROR_PATIENT_NOT_ASSOCIATED_WITH_USER = "errors.patient.not.associated.with.user";
    String ERROR_ACTIVE_PRESCRIPTION_DETAILS_NOT_FOUND_FOR_PATIENT = "errors.no.active.prescriptions.for.patient";
    String ERROR_PRESCRIPTION_DETAILS_NOT_FOUND_FOR_ID = "error.prescription.details.not.found.for.id";
    String ERROR_PRESCRIPTION_ID_NOT_FOUND = "errors.prescription.id.not.found";
    String ERROR_PRESCRIPTION_ID_DOES_NOT_BELONG_TO_REQUESTED_PATIENT = "errors.prescription.id.not.belong.to.requested.patient";
    String ERROR_PRESCRIPTION_INFO_ID_NOT_FOUND = "errors.prescription.info.id.not.found";
    String ERROR_NOTIFICATION_ID_NOT_FOUND = "errors.notification.id.not.found";
    String ERROR_PRESCRIPTION_ID_ALREADY_DELETED = "errors.prescription.id.already.deleted";
    String ERROR_PRESCRIPTION_INFO_ID_ALREADY_DELETED = "errors.prescription.info.id.already.deleted";
    String ERROR_NULL_IS_NOT_ACCEPTED = "errors.null.not.accepted";
    String ERROR_INVALID_WEEK_DAY_VALUE = "errors.invalid.week.day.value";
    String ERROR_INVALID_DAY_VALUE_FOR_MONTH = "errors.invalid.day.in.month";
    String ERROR_INVALID_FREQUENCY_TYPE = "errors.invalid.frequency.type";
    String ERROR_INVALID_FREQUENCY_TYPE_FOR_IMPLEMENTATION = "errors.invalid.frequency.type.for.implementation";
}
