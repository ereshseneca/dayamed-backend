package com.sg.dayamed.util;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 11/Apr/2019
 **/
@Getter
public enum WeekDays {

    SUNDAY(1, "Sun"), MONDAY(2, "Mon"), TUESDAY(3, "Tue"), WEDNESDAY(4, "Wed"), THURSDAY(5, "Thu"), FRIDAY(6, "Fri"), SATURDAY(7, "Sat");

    private Integer id;
    private String day;

    private WeekDays(Integer id, String day) {
        this.day = day;
        this.id = id;
    }

    public static WeekDays getWeekDay(String day) {
        WeekDays[] weekDays = WeekDays.values();
        return Arrays.stream(weekDays).filter(week -> week.getDay().equalsIgnoreCase(day)).findFirst().orElse(null);
    }
}
