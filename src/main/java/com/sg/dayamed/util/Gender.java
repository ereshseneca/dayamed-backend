package com.sg.dayamed.util;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 21/Feb/2019
 **/
@Getter
public enum Gender {

    MALE(1, "Male"), FEMALE(2, "Female"), TRANS_GENDER(3, "Transgender"), CANNOT_SAY(4, "CannotSay");

    private Integer id;
    private String value;

    private Gender(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static Gender getGender(Integer id) {
        Gender[] genders = Gender.values();
        return Arrays.stream(genders).filter(gender -> gender.getId().equals(id)).findAny().orElse(null);
    }

    public static Gender getGender(String value) {
        Gender[] genders = Gender.values();
        return Arrays.stream(genders).filter(gender -> StringUtils.equalsIgnoreCase(gender.getValue(), value)).findAny().orElse(null);
    }

}
