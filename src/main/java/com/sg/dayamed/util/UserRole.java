package com.sg.dayamed.util;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 22/Feb/2019
 **/
@Getter
public enum UserRole {

    ADMIN(0, "Admin"), PATIENT(1, "Patient"), PROVIDER(2, "Provider"), PHARMACIST(3, "Pharmacist"), CARE_TAKER(4, "Caretaker"), NO_ROLE(-1, "NoRole");

    private Integer id;
    private String value;

    private UserRole(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static UserRole getUserRole(Integer id) {
        UserRole[] userRoles = UserRole.values();
        return Arrays.stream(userRoles).filter(role -> role.getId().equals(id)).findAny().orElse(null);
    }

    public static UserRole getUserRole(String value) {
        UserRole[] userRoles = UserRole.values();
        return Arrays.stream(userRoles).filter(role -> role.getValue().equals(value)).findAny().orElse(null);
    }
}
