package com.sg.dayamed.validators;

import com.sg.dayamed.util.AdherenceStatus;
import com.sg.dayamed.util.Gender;
import com.sg.dayamed.util.NotificationType;
import com.sg.dayamed.util.UserRole;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Eresh Gorantla on 06/Apr/2019
 **/

public class EnumValueValidatorImpl implements ConstraintValidator<EnumIDValidator, String> {
    private EnumIDValidator annotation;


    @Override
    public void initialize(EnumIDValidator annotation) {
        this.annotation = annotation;
    }

    @Override
    public boolean isValid(String valueForValidation, ConstraintValidatorContext constraintValidatorContext) {
        if (valueForValidation == null) {
            return false;
        }
        Object[] enumValues = this.annotation.enumClass().getEnumConstants();

        if (enumValues != null) {
            for (Object enumValue : enumValues) {
                if (enumValue instanceof AdherenceStatus) {
                    AdherenceStatus adherenceStatus = (AdherenceStatus) enumValue;
                    return StringUtils.equals(adherenceStatus.getValue(), valueForValidation);
                } else if (enumValue instanceof Gender) {
                    Gender gender = (Gender) enumValue;
                    return StringUtils.equals(gender.getValue(), valueForValidation);
                } else if (enumValue instanceof UserRole) {
                    UserRole userRole = (UserRole) enumValue;
                    return StringUtils.equals(userRole.getValue(), valueForValidation);
                } else if (enumValue instanceof NotificationType) {
                    NotificationType type = (NotificationType) enumValue;
                    return StringUtils.equals(type.getValue(), valueForValidation);
                }
            }
        }

        return false;
    }
}
