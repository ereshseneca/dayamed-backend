package com.sg.dayamed.validators;

import com.sg.dayamed.util.AdherenceStatus;
import com.sg.dayamed.util.FrequencyType;
import com.sg.dayamed.util.Gender;
import com.sg.dayamed.util.NotificationType;
import com.sg.dayamed.util.UserRole;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Eresh Gorantla on 20/Feb/2019
 **/

public class EnumIDValidatorImpl implements ConstraintValidator<EnumIDValidator, Integer> {
    private EnumIDValidator annotation;


    @Override
    public void initialize(EnumIDValidator annotation) {
        this.annotation = annotation;
    }

    @Override
    public boolean isValid(Integer valueForValidation, ConstraintValidatorContext constraintValidatorContext) {
        if (valueForValidation == null) {
            return false;
        }

        Object[] enumValues = this.annotation.enumClass().getEnumConstants();

        if (enumValues != null) {
            for (Object enumValue : enumValues) {
                if (enumValue instanceof AdherenceStatus) {
                    AdherenceStatus adherenceStatus = (AdherenceStatus) enumValue;
                    if (adherenceStatus.getId().equals(valueForValidation)) {
                        return true;
                    }
                } else if (enumValue instanceof Gender) {
                    Gender gender = (Gender) enumValue;
                    if (gender.getId().equals(valueForValidation)) {
                        return true;
                    }
                } else if (enumValue instanceof UserRole) {
                    UserRole userRole = (UserRole) enumValue;
                    if (userRole.getId().equals(valueForValidation)) {
                        return true;
                    }
                } else if (enumValue instanceof NotificationType) {
                    NotificationType type = (NotificationType) enumValue;
                    if (type.getId().equals(valueForValidation)) {
                        return true;
                    }
                } else if (enumValue instanceof FrequencyType) {
                    FrequencyType type = (FrequencyType) enumValue;
                    if (type.getId().equals(valueForValidation)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
