package com.sg.dayamed.cache;

import com.sg.dayamed.vo.RedisVO;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 18/Mar/2019
 **/
@Component
public class RedisVOComponent {

    private final ReactiveRedisConnectionFactory factory;
    private final ReactiveRedisOperations<String, Object> redisOps;

    public RedisVOComponent(ReactiveRedisConnectionFactory factory, ReactiveRedisOperations<String, Object> coffeeOps) {
        this.factory = factory;
        this.redisOps = coffeeOps;
    }

    public void loadData(RedisVO redisVO) {
        try {
            redisOps.opsForValue().set("User", redisVO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
