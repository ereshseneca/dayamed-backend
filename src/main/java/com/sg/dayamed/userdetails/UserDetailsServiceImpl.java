package com.sg.dayamed.userdetails;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.persistence.domain.PatientAssociations;
import com.sg.dayamed.persistence.domain.UserDetails;
import com.sg.dayamed.persistence.domain.UserRoleInformation;
import com.sg.dayamed.persistence.domain.UserRoleMapping;
import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.persistence.repository.PatientAssociationInformationVRepository;
import com.sg.dayamed.persistence.repository.PatientAssociationsRepostory;
import com.sg.dayamed.persistence.repository.UserDetailsRepository;
import com.sg.dayamed.persistence.repository.UserInformationVRepository;
import com.sg.dayamed.persistence.repository.UserRoleInformationRepository;
import com.sg.dayamed.persistence.repository.UserRoleMappingRepository;
import com.sg.dayamed.persistence.repository.UserRoleRepository;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.UserRole;
import com.sg.dayamed.vo.PatientAssociationError;
import com.sg.dayamed.vo.SaveUserDetailsVO;
import com.sg.dayamed.vo.UserDetailInformationVO;
import com.sg.dayamed.vo.UserDetailsVO;
import com.sg.dayamed.vo.UserInformationVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserDetailsRepository userDetailsRepository;

    @Autowired
    UserInformationVRepository userInformationVRepository;

    @Autowired
    UserRoleInformationRepository userRoleInformationRepository;

    @Autowired
    UserRoleMappingRepository userRoleMappingRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    PatientAssociationsRepostory patientAssociationsRepostory;

    @Autowired
    PatientAssociationInformationVRepository associationInformationVRepository;

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public UserDetailInformationVO saveUserDetails(SaveUserDetailsVO saveUserDetailsVO, Boolean isPatient) throws ApplicationException {
        try {
            validateSaveUserDetails(saveUserDetailsVO, isPatient);
            UserDetailsVO userDetailsVO = saveUserDetails(saveUserDetailsVO.getUserDetails(), saveUserDetailsVO.getIsUpdate());
            if (!saveUserDetailsVO.getIsUpdate()) {
                mapUserRoleMapping(saveUserDetailsVO, userDetailsVO.getId());
            }
            if (isPatient) {
                List<Long> patientAssociationIds = saveUserDetailsVO.getPatientAssociations();
                List<UserInformationV> informationVS = userInformationVRepository.findByRoleMappingIdInAndRoleActive(patientAssociationIds, true);
                List<UserInformationV> patients = informationVS.stream().filter(info -> UserRole.PATIENT.getId().equals(info.getRoleId())).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(patients)) {
                    String error = patients.stream().map(UserInformationV::getUserFirstName).collect(Collectors.joining(", "));
                    throw new  DataValidationException("patientAssociations", APIErrorKeys.ERROR_PATIENT_CANNOT_BE_ASSOCIATED_WITH_ANOTHER_PATIENT, new Object[]{error});
                }
                List<PatientAssociationInformationV> associationInformationVS = associationInformationVRepository.findByPatientId(userDetailsVO.getId());
                List<Long> associationIds = associationInformationVS.stream().map(PatientAssociationInformationV::getUserRoleMapId).collect(Collectors.toList());
                List<Long> associationIdsNew = patientAssociationIds.stream().filter(associationId -> !associationIds.contains(associationId)).collect(Collectors.toList());
                savePatientAssociations(associationIdsNew, userDetailsVO.getId());
            }
            List<UserInformationV> informationVS = userInformationVRepository.findByUserId(userDetailsVO.getId());
            List<PatientAssociationInformationV> associationInformationVS = new ArrayList<>();
            if (isPatient) {
                associationInformationVS = associationInformationVRepository.findByPatientId(userDetailsVO.getId());
            }
            return new UserDetailInformationVO(informationVS, userDetailsVO.getId(), associationInformationVS);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public List<UserDetailInformationVO> searchUserDetails(UserRole userRole, String text, Integer limit, Integer offset) throws ApplicationException {
        List<UserDetailInformationVO> detailInformationVOS = new ArrayList<>();
        try {
            return detailInformationVOS;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }

    }

    private List<PatientAssociations> savePatientAssociations(List<Long> associationIds, Long userId) throws Exception {
        try {
            List<PatientAssociations> patientAssociations = associationIds.stream().map(association -> this.generatePatientAssociations(association, userId)).collect(Collectors.toList());
            return patientAssociationsRepostory.saveAll(patientAssociations);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private PatientAssociations generatePatientAssociations(Long roleMappingId, Long userId) {
        PatientAssociations associations = new PatientAssociations();
        associations.setUserRoleMapId(roleMappingId);
        associations.setPatientId(userId);
        associations.setActive(true);
        return associations;
    }

    private UserDetailsVO saveUserDetails(UserDetailsVO userDetailsVO, Boolean isUpdate) throws ApplicationException {
        try {
            if (isUpdate) {
                Optional<UserDetails> userDetailsOptional = userDetailsRepository.findById(userDetailsVO.getId());
                UserDetails userDetails = userDetailsOptional.get();
                userDetailsVO = new UserDetailsVO(userDetails);
            }
            UserDetails userDetails = userDetailsVO.toUserDetails();
            userDetails = userDetailsRepository.save(userDetails);
            return new UserDetailsVO(userDetails);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private UserRoleMapping mapUserRoleMapping(SaveUserDetailsVO userDetailsVO, Long userId) throws ApplicationException {
        try {
            UserInformationVO informationVO = userDetailsVO.getUserInformation();
            UserRole userRole = UserRole.getUserRole(userDetailsVO.getRoleId());
            UserRoleInformation information = getUserRoleInformation(informationVO, userRole);
            information.setActive(true);
            information = userRoleInformationRepository.save(information);
            return saveUserRoleMapping(userId, userRole, information, userDetailsVO.getIsUpdate());
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private UserRoleMapping saveUserRoleMapping(Long userId, UserRole userRole, UserRoleInformation information, Boolean isUpdate) throws DataValidationException {
        UserRoleMapping userRoleMapping = new UserRoleMapping();
        UserInformationV userInformationV = userInformationVRepository.findByUserIdAndRoleId(userId, userRole.getId());
        if (userInformationV == null) {
            Optional<com.sg.dayamed.persistence.domain.UserRole> roleOptional = userRoleRepository.findById(userRole.getId());
            if (roleOptional.isPresent()) {
                com.sg.dayamed.persistence.domain.UserRole role = roleOptional.get();
                userRoleMapping.setRole(role);
                Optional<UserDetails> detailsOptional = userDetailsRepository.findById(userId);
                userRoleMapping.setUser(detailsOptional.get());
                userRoleMapping.setUserRoleInformation(information);
                userRoleMapping.setActive(true);
                return userRoleMappingRepository.save(userRoleMapping);
            }
        } else {
            if (!isUpdate) {
                throw new DataValidationException("roleId", APIErrorKeys.ERROR_USER_IS_ALREADY_MAPPED_WITH_THIS_ROLE, new Object[]{userRole.getValue()});
            }
        }
        throw new DataValidationException("roleId", APIErrorKeys.ERROR_USER_ROLE_INVALID, new Object[]{userRole.getId()});
    }

    private UserRoleInformation getUserRoleInformation(UserInformationVO informationVO, UserRole userRole) {
        UserRoleInformation information = new UserRoleInformation();
        if (informationVO != null) {
           /* if (UserRole.PATIENT.getId().equals(userRole.getId())) {
                information.setEmoji(informationVO.getEmoji());
            } else if (UserRole.CARE_TAKER.getId().equals(userRole.getId())) {

            } else if (UserRole.PROVIDER.getId().equals(userRole.getId())) {
                information.setLicense(informationVO.getLicense());
                information.setProviderSpecializationOn(informationVO.getProviderSpecializationOn());
            } else if (UserRole.PHARMACIST.getId().equals(userRole.getId())) {
                information.setPharmaCompanyName(informationVO.getPharmaCompanyName());
            }*/
        }
        return information;
    }

    private void generatePatientAssociationError(List<UserInformationV> existingMappings) throws DataValidationException {
        if (CollectionUtils.isNotEmpty(existingMappings)) {
            PatientAssociationError associationError = new PatientAssociationError();
            String pharmacistsData = existingMappings.stream().filter(pharma -> UserRole.PHARMACIST.getId().equals(pharma.getRoleId())).map(UserInformationV::getUserFirstName).
                    collect(Collectors.joining(", "));
            associationError.setPharmacists(StringUtils.isNotBlank(pharmacistsData) ? associationError.getPharmacists() + pharmacistsData : null);

            String providerData = existingMappings.stream().filter(provider -> UserRole.PROVIDER.getId().equals(provider.getRoleId())).map(UserInformationV::getUserFirstName).
                    collect(Collectors.joining(", "));
            associationError.setProviders(StringUtils.isNotBlank(providerData) ? associationError.getProviders() + providerData : null);

            String caretakerData = existingMappings.stream().filter(caretaker -> UserRole.PHARMACIST.getId().equals(caretaker.getRoleId())).map(UserInformationV::getUserFirstName).
                    collect(Collectors.joining(", "));
            associationError.setCaretakers(StringUtils.isNotBlank(caretakerData) ? associationError.getCaretakers() + caretakerData : null);

            throw new DataValidationException("patientAssociations", APIErrorKeys.ERROR_PATIENT_MAPPING_ASSOCIATIONS_ALREADY_EXISTS, new Object[]{Stream.of(associationError.getCaretakers(), associationError.getPharmacists(), associationError.getPharmacists()).filter(value -> StringUtils.isNotBlank(value)).collect(Collectors.joining(","))});
        }
    }

    private void validateSaveUserDetails(SaveUserDetailsVO userDetailsVO, Boolean isPatient) throws DataValidationException {
        if (!userDetailsVO.getIsUpdate()) {
            List<UserInformationV> informationVS = userInformationVRepository.findByUserEmailId(userDetailsVO.getUserDetails().getEmailId());
            if (CollectionUtils.isNotEmpty(informationVS)) {
                throw new DataValidationException("emailId", APIErrorKeys.ERROR_USER_DETAILS_EMAIL_ALREADY_REGISTERED, new Object[]{userDetailsVO.getUserDetails().getEmailId()});
            }
        } else {
            Long userId = userDetailsVO.getUserDetails().getId();
            List<UserInformationV> informationVS = userInformationVRepository.findByUserId(userId);
            if (CollectionUtils.isEmpty(informationVS)) {
                throw new DataValidationException("id", APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID, new Object[]{userDetailsVO.getUserDetails().getId()});
            }
        }
        if (isPatient && CollectionUtils.isEmpty(userDetailsVO.getPatientAssociations())) {
            throw new DataValidationException("patientAssociations", APIErrorKeys.ERROR_PATIENT_ASSOCIATIONS_ARE_REQUIRED);
        }
        /*if (userDetailsVO.getUserDetails().getId() != null) {
            UserInformationV userInformationV = userInformationVRepository.findByUserIdAndRoleId(userDetailsVO.getUserDetails().getId(), userDetailsVO.getRoleId());
            if (userInformationV != null) {
                throw new DataValidationException("userId", APIErrorKeys.ERROR_USER_DEATILS_NOT_FOUND_FOR_ID, new Object[] {userDetailsVO.getUserDetails().getId()});
            }
            *//*Optional<UserDetails> userDetailsOptional = userDetailsRepository.findById(userDetailsVO.getUserDetails().getId());
            if (!userDetailsOptional.isPresent()) {
                throw new DataValidationException("userId", APIErrorKeys.ERROR_USER_DEATILS_NOT_FOUND_FOR_ID, new Object[] {userDetailsVO.getUserDetails().getId()});
            }
            if (userDetailsOptional.isPresent()) {
                UserDetails userDetails = userDetailsOptional.get();

            }*//*
        }*/
    }
}
