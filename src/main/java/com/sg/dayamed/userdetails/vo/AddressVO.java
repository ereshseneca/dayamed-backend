package com.sg.dayamed.userdetails.vo;

import com.sg.dayamed.persistence.domain.Address;
import com.sg.dayamed.vo.StateVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 14/Feb/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class AddressVO {
    private Long id;
    private String address1;
    private String address2;
    private String city;
    private StateVO state;
    private CountryVO country;
    private String zipCode;
    private String location;
    private ZonedDateTime createdDate;

    public AddressVO(Address address) {
        if (address != null) {
            this.address1 = address.getAddress1();
            this.address2 = address.getAddress2();
            this.city = address.getCity();
            this.country = new CountryVO(address.getCountry());
            this.id = address.getId();
            this.location = address.getLocation();
            this.state = new StateVO(address.getState(), false);
        }
    }
}
