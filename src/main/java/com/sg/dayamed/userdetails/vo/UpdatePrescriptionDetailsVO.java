package com.sg.dayamed.userdetails.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 20/Feb/2019
 **/
@Getter
@Setter
public class UpdatePrescriptionDetailsVO {
    private Long userId;
    private String id;
    private Integer statusId;
    private Integer delay = 0;
}
