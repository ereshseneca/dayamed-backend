package com.sg.dayamed.userdetails.vo;

import com.sg.dayamed.persistence.domain.UserRoleInformation;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@Getter
@Setter
public class UserRoleInformationVO {
    private Long id;
    private boolean adminFlag;
    private String deviceId;
    private String emoji;
    private Boolean active;
    private String pharmaCompanyName;
    private String license;
    private String providerSpecializationOn;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;

    public UserRoleInformationVO(UserRoleInformation roleInformation) {
        if (roleInformation != null) {
            this.id = roleInformation.getId();
            this.adminFlag = roleInformation.getAdminFlag();
            /*this.deviceId = roleInformation.getDeviceId();
            this.emoji = roleInformation.getEmoji();*/
            this.active = roleInformation.getActive();
            /*this.pharmaCompanyName = roleInformation.getPharmaCompanyName();
            this.license = roleInformation.getLicense();
            this.providerSpecializationOn = roleInformation.getProviderSpecializationOn();*/
            this.createdDate = roleInformation.getCreatedDate();
            this.updatedDate = roleInformation.getUpdatedDate();
        }
    }


}
