package com.sg.dayamed.userdetails.vo;

import com.sg.dayamed.persistence.domain.Country;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@Getter
@Setter
public class CountryVO {
    private Integer id;
    private String name;
    private String isoCode;
    private String countryCode;
    private ZonedDateTime createdDate;

    public CountryVO(Country country) {
        if (country != null) {
            this.id = country.getId();
            this.countryCode = country.getCountryCode();
            this.createdDate = country.getCreatedDate();
            this.isoCode = country.getIsoCode();
            this.name = country.getName();
        }
    }
}
