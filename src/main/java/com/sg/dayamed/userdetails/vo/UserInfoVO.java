package com.sg.dayamed.userdetails.vo;

import com.sg.dayamed.persistence.domain.view.UserInformationV;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 07/Apr/2019
 **/
@Getter
@Setter
public class UserInfoVO {
    private Long id;
    private String role;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private Integer age;
    private Boolean active = false;
    private String mobileNumber;
    private String race;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;

    public UserInfoVO(UserInformationV informationV) {
        if (informationV != null) {
            this.id = informationV.getUserId();
            this.role = informationV.getRoleName();
            this.firstName = informationV.getUserFirstName();
            this.lastName = informationV.getUserLastName();
            this.middleName = informationV.getUserMiddleName();
            this.gender = informationV.getGender();
            this.active = informationV.getUserActive();
            this.age = informationV.getUserAge();
            this.mobileNumber = informationV.getUserMobileNumber();
            this.race = informationV.getRace();
            this.createdDate = informationV.getUserCreatedDate();
            this.updatedDate = informationV.getUserUpdatedDate();
        }
    }
}
