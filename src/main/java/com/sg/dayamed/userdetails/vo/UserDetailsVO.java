package com.sg.dayamed.userdetails.vo;

import com.sg.dayamed.persistence.domain.UserDetails;
import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.rest.ws.WSUserDetails;
import com.sg.dayamed.rest.ws.WSUserDetailsResponse;
import com.sg.dayamed.util.Gender;
import com.sg.dayamed.vo.AddressVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 14/Feb/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserDetailsVO {
    private Long id;
    private Integer age;
    private String emailId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String imageUrl;
    private String imanticUserId;
    private String mobileNumber;
    private String password;
    private String pwdChangeToken;
    private String pwdTokenExpireDate;
    private String race;
    private String token;
    private String deviceType;
    private String voipToken;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
    private AddressVO address;
    private Boolean active;

    public UserDetailsVO(UserDetails userDetails) {
        if (userDetails != null) {
            this.id = userDetails.getId();
            this.age = userDetails.getAge();
            this.emailId = userDetails.getEmailId();
            this.firstName = userDetails.getFirstName();
            this.middleName = userDetails.getMiddleName();
            this.lastName = userDetails.getLastName();
            this.gender = userDetails.getGender();
            this.imageUrl = userDetails.getImageUrl();
            this.imanticUserId = userDetails.getImanticUserId();
            this.mobileNumber = userDetails.getMobileNumber();
            this.password = userDetails.getPassword();
            this.pwdChangeToken = userDetails.getPwdChangeToken();
            this.pwdTokenExpireDate = userDetails.getPwdTokenExpireDate();
            this.race = userDetails.getRace();
            this.token = userDetails.getToken();
            this.deviceType = userDetails.getDeviceType();
            this.voipToken = userDetails.getVoipToken();
            this.createdDate = userDetails.getCreatedDate();
            this.updatedDate = userDetails.getUpdatedDate();
            this.active = userDetails.getActive();
           // this.addresses = userDetails.getAddresses().stream().map(AddressVO::new).collect(Collectors.toList());
        }
    }

    public UserDetails toUserDetails() {
        UserDetails userDetails = new UserDetails();
        userDetails.setId(this.id);
        userDetails.setAge(this.age);
        userDetails.setDeviceType(this.deviceType);
        userDetails.setEmailId(this.emailId);
        userDetails.setFirstName(this.firstName);
        userDetails.setMiddleName(this.middleName);
        userDetails.setLastName(this.lastName);
        userDetails.setGender(this.gender);
        userDetails.setImageUrl(this.imageUrl);
        userDetails.setImanticUserId(this.imanticUserId);
        userDetails.setMobileNumber(this.mobileNumber);
        userDetails.setPassword(this.password);
        userDetails.setPwdChangeToken(this.pwdChangeToken);
        userDetails.setPwdTokenExpireDate(this.pwdTokenExpireDate);
        userDetails.setToken(this.token);
        userDetails.setVoipToken(this.voipToken);
        userDetails.setActive(this.active);
        userDetails.setCreatedDate(this.createdDate);
        userDetails.setUpdatedDate(this.updatedDate);
        if (this.id != null) {
            userDetails.setUpdatedDate(ZonedDateTime.now());
        }
        return userDetails;
    }

    public WSUserDetails toWSUserDetails() {
        WSUserDetails userDetails = new WSUserDetails();
        userDetails.setId(this.id.toString());
        userDetails.setAge(this.age);
        userDetails.setDeviceType(this.deviceType);
        userDetails.setEmailId(this.emailId);
        userDetails.setFirstName(this.firstName);
        userDetails.setMiddleName(this.middleName);
        userDetails.setLastName(this.lastName);
        Gender gender = Gender.getGender(this.gender);
        userDetails.setGender(gender != null ? gender.getId() : null);
        userDetails.setImageUrl(this.imageUrl);
        userDetails.setImanticUserId(this.imanticUserId);
        userDetails.setMobileNumber(this.mobileNumber);
        userDetails.setPassword(this.password);
        userDetails.setPwdChangeToken(this.pwdChangeToken);
        userDetails.setPwdTokenExpireDate(this.pwdTokenExpireDate);
        userDetails.setToken(this.token);
        userDetails.setVoipToken(this.voipToken);
        userDetails.setActive(this.active);
        userDetails.setCreatedDate(this.createdDate);
        userDetails.setUpdatedDate(this.updatedDate);
        if (this.id != null) {
            userDetails.setUpdatedDate(ZonedDateTime.now());
        }
        return userDetails;
    }

    public WSUserDetailsResponse toWSUserDetailsResponse() {
        WSUserDetailsResponse userDetails = new WSUserDetailsResponse();
        userDetails.setId(this.id.toString());
        userDetails.setAge(this.age != null ? this.age.toString() : null);
        userDetails.setDeviceType(this.deviceType);
        userDetails.setEmailId(this.emailId);
        userDetails.setFirstName(this.firstName);
        userDetails.setMiddleName(this.middleName);
        userDetails.setLastName(this.lastName);
        Gender gender = Gender.getGender(this.gender);
        userDetails.setGender(gender != null ? gender.getValue() : null);
        userDetails.setImageUrl(this.imageUrl);
        userDetails.setImanticUserId(this.imanticUserId);
        userDetails.setMobileNumber(this.mobileNumber);
        //userDetails.setPassword(this.password);
        userDetails.setPwdChangeToken(this.pwdChangeToken);
        userDetails.setPwdTokenExpireDate(this.pwdTokenExpireDate);
        userDetails.setToken(this.token);
        userDetails.setVoipToken(this.voipToken);
        userDetails.setActive(this.active);
        userDetails.setCreatedDate(this.createdDate);
        userDetails.setUpdatedDate(this.updatedDate);
        userDetails.setUpdatedDate(ZonedDateTime.now());
        return userDetails;
    }

    public UserDetailsVO(UserInformationV userDetails) {
        if (userDetails != null) {
            this.id = userDetails.getUserId();
            this.age = userDetails.getUserAge();
            this.emailId = userDetails.getUserEmailId();
            this.firstName = userDetails.getUserFirstName();
            this.middleName = userDetails.getUserMiddleName();
            this.lastName = userDetails.getUserLastName();
            this.gender = userDetails.getGender();
            this.imageUrl = userDetails.getUserImageUrl();
            this.imanticUserId = userDetails.getImanticUserId();
            this.mobileNumber = userDetails.getUserMobileNumber();
            this.password = userDetails.getUserPassword();
            this.pwdChangeToken = userDetails.getPwdChangeToken();
            this.pwdTokenExpireDate = userDetails.getPwdTokenExpiryDate();
            this.race = userDetails.getRace();
            this.token = userDetails.getUserToken();
            //this.deviceType = userDetails.getde;
            this.voipToken = userDetails.getVoipToken();
            this.createdDate = userDetails.getUserCreatedDate();
            this.updatedDate = userDetails.getUserUpdatedDate();
            this.active = userDetails.getUserActive();
        }
    }

    public UserDetailsVO(PatientAssociationInformationV informationV) {
        if (informationV != null) {
            this.id = informationV.getPatientId();
            this.age = informationV.getPatientAge();
            this.emailId = informationV.getPatientEmailId();
            this.mobileNumber = informationV.getPatientMobileNumber();
            this.firstName = informationV.getPatientFirstName();
            this.lastName = informationV.getPatientLastName();
            this.middleName = informationV.getPatientMiddleName();
            this.gender = informationV.getPatientGender();
            this.imageUrl = informationV.getPatientImageUrl();
            this.createdDate = informationV.getPatientCreatedDate();
        }
    }
}
