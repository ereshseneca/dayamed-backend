package com.sg.dayamed.userdetails.vo;

import com.sg.dayamed.persistence.domain.UserRoleInformation;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.rest.ws.WSUserInformation;
import com.sg.dayamed.util.UserRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 22/Feb/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserInformationVO {
    private Long id;
    private Boolean adminFlag;
    private String deviceId;
    private String emoji;
    private Boolean active;
    private String pharmaCompanyName;
    private String license;
    private String providerSpecializationOn;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;

    public UserRoleInformation toUserRoleInformation() {
        UserRoleInformation information = new UserRoleInformation();
        information.setAdminFlag(this.adminFlag);
        /*information.setDeviceId(this.deviceId);
        information.setEmoji(this.emoji);*/
        information.setId(this.id);
        /*information.setLicense(this.license);
        information.setPharmaCompanyName(this.pharmaCompanyName);
        information.setProviderSpecializationOn(this.providerSpecializationOn);*/
        information.setActive(this.active);
        return information;
    }

    public UserInformationVO(UserInformationV informationV) {
        this.id = informationV.getUserRoleInformationId();
        this.adminFlag = informationV.getAdminFlag();
        /*this.deviceId = informationV.getUriDeviceId();
        this.emoji = informationV.getEmoji();*/
        /*this.pharmaCompanyName = informationV.getPharmaConpmanyName();
        this.license = informationV.getLicense();*/
        this.createdDate = informationV.getUrlCreatedDate();
        this.active = informationV.getUriActive();
    }

    public WSUserInformation toWSUserInformation() {
        WSUserInformation information = new WSUserInformation();
        information.setAdminFlag(this.adminFlag);
        information.setDeviceId(this.deviceId);
        information.setEmoji(this.emoji);
        information.setLicense(this.license);
        information.setPharmaCompanyName(this.pharmaCompanyName);
        information.setProviderSpecializationOn(this.providerSpecializationOn);
        return information;
    }

    public UserInformationVO(UserInformationV informationV, UserRole userRole) {
        this.id = informationV.getUserRoleInformationId();
        /*if (UserRole.PATIENT.getId().equals(informationV.getRoleId())) {
            this.emoji = informationV.getEmoji();
            this.active = informationV.getUriActive();
            this.createdDate = informationV.getUrlCreatedDate();
        } else if (UserRole.PROVIDER.getId().equals(informationV.getRoleId())) {
            this.providerSpecializationOn = informationV.getProviderSpecializationOn();
            this.license = informationV.getLicense();
            this.active = informationV.getUriActive();
            this.createdDate = informationV.getUrlCreatedDate();
        } else if (UserRole.PHARMACIST.getId().equals(informationV.getRoleId())) {
            this.active = informationV.getUriActive();
            this.createdDate = informationV.getUrlCreatedDate();
            this.pharmaCompanyName = informationV.getPharmaConpmanyName();
        } else if (UserRole.CARE_TAKER.getId().equals(informationV.getRoleId())) {
            this.active = informationV.getUriActive();
            this.createdDate = informationV.getUrlCreatedDate();
        }*/
    }
}
