package com.sg.dayamed.userdetails.vo;

import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import com.sg.dayamed.persistence.domain.view.UserInformationV;
import com.sg.dayamed.vo.PatientAssociationVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 23/Feb/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserDetailInformationVO {
    private UserDetailsVO details;
    private UserInformationVO information;
    private List<UserRoleMappingVO> roleMappings = new ArrayList<>();
    private List<PatientAssociationVO> patientAssociations = new ArrayList<>();

    public UserDetailInformationVO(List<UserInformationV> informationVS, Long userId, List<PatientAssociationInformationV> associationInformationVS) {
        UserInformationV informationV = informationVS.get(0);
        this.details = new UserDetailsVO(informationV);
        this.information = new UserInformationVO(informationV);
        this.roleMappings = informationVS.stream().map(info -> new UserRoleMappingVO(info)).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(associationInformationVS))
        this.patientAssociations = associationInformationVS.stream().map(association -> new PatientAssociationVO(association)).collect(Collectors.toList());
    }
}
