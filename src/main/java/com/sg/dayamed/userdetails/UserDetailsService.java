package com.sg.dayamed.userdetails;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.util.UserRole;
import com.sg.dayamed.vo.SaveUserDetailsVO;
import com.sg.dayamed.vo.UserDetailInformationVO;

import java.util.List;

/**
 * Created by Eresh Gorantla on 14/Feb/2019
 **/

public interface UserDetailsService {
    UserDetailInformationVO saveUserDetails(SaveUserDetailsVO userDetailsVO, Boolean isPatient) throws ApplicationException;
    List<UserDetailInformationVO> searchUserDetails(UserRole userRole, String text, Integer limit, Integer offset) throws ApplicationException;
}
