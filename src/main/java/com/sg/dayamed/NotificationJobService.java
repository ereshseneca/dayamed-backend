package com.sg.dayamed;

import com.sg.dayamed.commons.exception.ApplicationException;

/**
 * Created by Eresh Gorantla on 11/Mar/2019
 **/

public interface NotificationJobService {

    void processPrescriptionNotifications() throws ApplicationException;
}
