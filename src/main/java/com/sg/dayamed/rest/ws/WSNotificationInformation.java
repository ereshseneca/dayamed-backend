package com.sg.dayamed.rest.ws;

import com.sg.dayamed.util.NotificationType;
import com.sg.dayamed.vo.NotificationInformationVO;
import com.sg.dayamed.validators.EnumIDValidator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Eresh Gorantla on 04/Mar/2019
 **/
@Getter
@Setter
@ApiModel(description = "Notification information details")
public class WSNotificationInformation {

    @NotNull(message = "{notification.information.type.id.required}")
    @EnumIDValidator(enumClass = NotificationType.class, message = "{notification.type.invalid.value}")
    @ApiModelProperty(required = true, notes = "Notification type is required")
    private Integer notificationTypeId;

    private Long id;

    @NotNull
    @ApiModelProperty(required = true, notes = "Recipient separated by comma")
    private List<@NotBlank(message="{notification.recipients.values.cannot.be.blank}") String> recipients;

    @NotNull(message = "{notification.delay.in.minutes.required}")
    @ApiModelProperty(required = true, notes = "Delay in Minutes to trigger notification")
    private Integer delayInMinutes;

    public NotificationInformationVO toNotificationInformationVO() {
        NotificationInformationVO informationVO = new NotificationInformationVO();
        informationVO.setDelayInMinutes(this.delayInMinutes);
        informationVO.setNotificationTypeId(this.notificationTypeId);
        informationVO.setRecipients(this.recipients);
        informationVO.setId(this.id);
        return informationVO;
    }
}
