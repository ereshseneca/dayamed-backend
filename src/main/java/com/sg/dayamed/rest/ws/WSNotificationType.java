package com.sg.dayamed.rest.ws;

import com.sg.dayamed.vo.NotificationTypeVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSNotificationType {
    private Integer id;
    private String type;

    public WSNotificationType(NotificationTypeVO notificationTypeVO) {
        this.id = notificationTypeVO.getId();
        this.type = notificationTypeVO.getType();
    }
}
