package com.sg.dayamed.rest.ws;

import com.sg.dayamed.persistence.domain.TestData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSTestData {
    private Integer id;
    private String name;
    private String city;

    public WSTestData(TestData testData) {
        this.id = testData.getData().getId();
        this.city = testData.getData().getCity();
        this.name = testData.getData().getName();
    }
}
