package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Getter
@Setter
public class WSPatientAssociation {
    private Long id;
    private Long roleMappingId;
    private String role;
    private Boolean active;
    private ZonedDateTime createdDate;
}
