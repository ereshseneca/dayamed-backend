package com.sg.dayamed.rest.ws;

import com.sg.dayamed.prescription.vo.FrequencyVO;
import com.sg.dayamed.util.FrequencyType;
import com.sg.dayamed.validators.EnumIDValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 10/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSFrequencyInfo {

    @EnumIDValidator(enumClass = FrequencyType.class, message = "{frequency.type.is.invalid}")
    private Integer typeId;

    private String type;

    private List<String> actualValues = new ArrayList<>();
    private Integer recurrence = 1;

    @NotNull(message = "{duration.is.required}")
    private Integer duration;

    @NotNull
    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}", message = "{invalid.date.format}")
    private String startDate;

    public WSFrequencyInfo(FrequencyVO vo) {
        if (vo != null) {
            this.typeId = vo.getTypeId();
            this.actualValues = vo.getActualValues();
            this.recurrence = vo.getRecurrence();
            this.duration = vo.getDuration();
            this.startDate = vo.getStartDate();
            FrequencyType type = FrequencyType.getFrequencyType(this.typeId);
            this.type = type != null ? type.getValue() : null;
        }
    }

    public List<String> getActualValues() {
        //validateActualValues();
        return this.actualValues;
    }

}
