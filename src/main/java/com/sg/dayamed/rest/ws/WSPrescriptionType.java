package com.sg.dayamed.rest.ws;

import com.sg.dayamed.prescription.vo.PrescriptionTypeVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPrescriptionType {
    private Integer id;
    private String name;
    private String description;
    private ZonedDateTime createdDate;

    public WSPrescriptionType(PrescriptionTypeVO typeVO) {
        if (typeVO != null) {
            this.createdDate = typeVO.getCreatedDate();
            this.name = typeVO.getName();
            this.description = typeVO.getDescription();
            this.id = typeVO.getId();
        }
    }
}
