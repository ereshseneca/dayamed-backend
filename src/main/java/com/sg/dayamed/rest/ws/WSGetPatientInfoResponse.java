package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.rest.RestResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 22/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSGetPatientInfoResponse extends RestResponse {
    private WSPatientInfo info;
}
