package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.rest.RestResponse;
import com.sg.dayamed.vo.UserDetailInformationVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSUserDetailInformation extends RestResponse {
    private WSUserDetails details;
    private WSUserInformation information;
    private List<WSRoleMappingInformation> roleMappings = new ArrayList<>();
    private List<WSPatientAssociation> patientAssociations = new ArrayList<>();

    public WSUserDetailInformation(UserDetailInformationVO userDetailInformationVO) {
        if (userDetailInformationVO != null) {
            this.details = userDetailInformationVO.getDetails() != null ? userDetailInformationVO.getDetails().toWSUserDetails() : null;
            this.information = userDetailInformationVO.getInformation() != null ? userDetailInformationVO.getInformation().toWSUserInformation() : null;
            this.roleMappings = userDetailInformationVO.getRoleMappings().stream().map(role -> role.toWsUserRoleMapping()).collect(Collectors.toList());
            this.patientAssociations = userDetailInformationVO.getPatientAssociations().stream().map(assoc -> assoc.toWSPatientAssociation()).collect(Collectors.toList());
        }
    }
}
