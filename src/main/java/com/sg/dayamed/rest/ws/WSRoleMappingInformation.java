package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSRoleMappingInformation {
    private Long userId;
    private Integer roleId;
    private String role;
    private WSUserInformation userRoleInformation;
}
