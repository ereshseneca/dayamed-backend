package com.sg.dayamed.rest.ws;

import com.sg.dayamed.prescription.vo.PrescriptionInfoDetailVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPrescriptionInfoDetails {
    private Long id;
    private String comment;
    private Integer duration;
    private List<String> details;
    private Boolean active;
    private WSPrescriptionInfoType prescriptionInfoType;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
    private WSFrequencyInfo frequency;
    private List<WSNotificationDetails> notifications = new ArrayList<>();

    public WSPrescriptionInfoDetails(PrescriptionInfoDetailVO infoDetailVO) {
        if (infoDetailVO != null) {
            this.id = infoDetailVO.getId();
            this.active = infoDetailVO.getActive();
            this.duration = infoDetailVO.getDuration();
            this.details = infoDetailVO.getDetails();
            this.prescriptionInfoType = new WSPrescriptionInfoType(infoDetailVO.getPrescriptionInfoType());
            this.comment = infoDetailVO.getComment();
            this.notifications = infoDetailVO.getNotifications().stream().map(WSNotificationDetails::new).collect(Collectors.toList());
            this.createdDate = infoDetailVO.getCreatedDate();
            this.updatedDate = infoDetailVO.getUpdatedDate();
            this.frequency = new WSFrequencyInfo(infoDetailVO.getFrequency());
        }
    }
}
