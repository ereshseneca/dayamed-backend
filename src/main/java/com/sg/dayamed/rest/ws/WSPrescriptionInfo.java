package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.prescription.vo.FrequencyVO;
import com.sg.dayamed.prescription.vo.PrescriptionInfoVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.math.NumberUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@Getter
@Setter
@ApiModel(description = "Model for prescription information")
public class WSPrescriptionInfo {
    private Long id;
    private String comment;

    @NotNull(message = "{prescription.info.course.duration.required}")
    @ApiModelProperty(required = true, notes = "Course duration")
    private Integer duration;

    private String identityCode;
    private String pharmacistComment;

    @NotNull(message = "{prescription.info.intake.time.required}")
    @ApiModelProperty(required = true, notes = "This property will capture intake time of medicine in the 24 Hr format")
    private List<@Valid String> details;

    @NotNull(message = "{prescription.info.type.id.required}")
    @ApiModelProperty(required = true, notes = "Prescription info type id is required.")
    private Long prescriptionInfoTypeId;

    private Boolean active = true;

    private List<@Valid WSNotificationInformation> notifications = new ArrayList<>();

    @Valid
    private WSFrequencyInfo frequency;

    private List<@Valid Long> deletedNotifications = new ArrayList<>();

    public PrescriptionInfoVO toPrescriptionInfoVO(String zoneId) {
        PrescriptionInfoVO info = new PrescriptionInfoVO();
        info.setComment(this.comment);
        info.setDuration(this.duration);
        info.setId(this.id);
        info.setDetails(this.details);
        info.setIdentityCode(this.identityCode);
        info.setPrescriptionInfoTypeId(this.prescriptionInfoTypeId);
        info.setActive(this.active);
        info.setPharmacistComment(this.pharmacistComment);
        info.setFrequency(new FrequencyVO(this.frequency));
        info.setNotifications(this.notifications.stream().map(notification -> notification.toNotificationInformationVO()).collect(Collectors.toList()));
        info.setDeletedNotificationIds(IntStream.range(NumberUtils.INTEGER_ZERO, this.deletedNotifications.size()).mapToObj(index -> new IdIndexVO(index,
                this.deletedNotifications.get(index))).collect(Collectors.toList()));
        return info;
    }
}
