package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Created by Eresh Gorantla on 21/Feb/2019
 **/
@Getter
@Setter
public class WSUserRoleMapping {

    private Long userId;

    @NotNull(message = "{user.role.mapping.role.id.required}")
    private Integer roleId;
}
