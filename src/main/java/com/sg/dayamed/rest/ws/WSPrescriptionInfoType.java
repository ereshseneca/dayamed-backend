package com.sg.dayamed.rest.ws;

import com.sg.dayamed.prescription.vo.PrescriptionInfoTypeVO;
import com.sg.dayamed.vo.AdditionalInfoVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPrescriptionInfoType {
    private Long id;
    private String name;
    private String description;
    private AdditionalInfoVO additionalInfo;
    private WSPrescriptionType prescriptionType;
    private ZonedDateTime createdDate;


    public WSPrescriptionInfoType(PrescriptionInfoTypeVO typeVO) {
        if (typeVO != null) {
            this.id = typeVO.getId();
            this.name = typeVO.getName();
            this.description = typeVO.getDescription();
            this.additionalInfo = getAdditionalInfo();
            this.prescriptionType = new WSPrescriptionType(typeVO.getPrescriptionType());
            this.createdDate = typeVO.getCreatedDate();
        }
    }
}
