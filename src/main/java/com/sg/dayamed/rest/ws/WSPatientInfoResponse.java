package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.rest.RestResponse;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 21/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "Model for getting list of patients")
public class WSPatientInfoResponse extends RestResponse {
    private List<WSPatientInfo> patients;
    private Long totalRecords;
}
