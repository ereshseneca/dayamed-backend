package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 20/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSUserDetailsResponse {
    private String id;
    private String age;
    private String emailId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String imageUrl;
    private String imanticUserId;
    private Boolean active = true;
    private String mobileNumber;
    //private String password;
    private String pwdChangeToken;
    private String pwdTokenExpireDate;
    private String race;
    private String token;
    private String deviceType;
    private String voipToken;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
    private WSAddress address;
}
