package com.sg.dayamed.rest.ws;

import com.sg.dayamed.vo.NotificationDetailsVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSNotificationDetails {
    private Long id;
    private String recipients;
    private Integer delayInMinutes;
    private Boolean defaultNotification;
    private WSNotificationType type;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;

    public WSNotificationDetails(NotificationDetailsVO detailsVO) {
        if (detailsVO != null) {
            this.id = detailsVO.getId();
            this.recipients = detailsVO.getRecipients();
            this.defaultNotification = detailsVO.getDefaultNotification();
            this.delayInMinutes = detailsVO.getDelayInMinutes();
            this.createdDate = detailsVO.getCreatedDate();
            this.type = new WSNotificationType(detailsVO.getType());
        }
    }
}
