package com.sg.dayamed.rest.ws;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.dayamed.commons.vo.IdIndexVO;
import com.sg.dayamed.config.DayamedIdDeserializer;
import com.sg.dayamed.prescription.vo.PrescriptionVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Request Model for saving prescription")
public class WSSavePrescription {

    @ApiModelProperty(notes = "Required when updating prescription")
    @JsonDeserialize(using = DayamedIdDeserializer.class)
    private String id;
    private String name;
    private String providerComment;
    private String illnessDesc;
    private String diagnosis;
    @ApiModelProperty(notes = "Required when updating prescription whether to activate or deactivate")
    private Boolean active = true;

    @NotNull(message = "{prescription.info.details.are.required}")
    private List<@Valid WSPrescriptionInfo> prescriptionInformationS = new ArrayList<>();
    private List<@Valid Long> deletedPrescriptionInfoIds = new ArrayList<>();

    public PrescriptionVO toPrescriptionVO(String zoneId) {
        PrescriptionVO prescriptionVO = new PrescriptionVO();
        prescriptionVO.setActive(this.active);
        prescriptionVO.setDiagnosis(this.diagnosis);
        prescriptionVO.setId(StringUtils.isNotBlank(this.id) ? NumberUtils.toLong(this.id) : null);
        prescriptionVO.setIllnessDesc(this.illnessDesc);
        prescriptionVO.setIsUpdate(this.id != null);
        prescriptionVO.setProviderComment(this.providerComment);
        prescriptionVO.setName(this.name);
        prescriptionVO.setPrescriptionInfoS(this.prescriptionInformationS.stream().map(pres -> pres.toPrescriptionInfoVO(zoneId)).collect(Collectors.toList()));
        prescriptionVO.setDeletedPrescriptionInfoIds(IntStream.range(NumberUtils.INTEGER_ZERO, this.deletedPrescriptionInfoIds.size()).mapToObj(index -> new IdIndexVO(index,
                this.deletedPrescriptionInfoIds.get(index))).collect(Collectors.toList()));
        return prescriptionVO;
    }
}
