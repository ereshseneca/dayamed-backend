package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.rest.RestResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/
@Getter
@Setter
public class WSTestDataResponse extends RestResponse {
    private List<WSTestData> testData = new ArrayList<WSTestData>();
}
