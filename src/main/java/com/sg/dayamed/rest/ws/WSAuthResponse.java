package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.rest.RestResponse;
import com.sg.dayamed.vo.UserDetailInformationVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 20/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSAuthResponse extends RestResponse {
    private String accessKey;
    private WSUserDetailsResponse userDetails;
    private WSUserInformation information;
    private List<WSRoleMappingInformation> roleMappings;
    private List<WSUserDetailsResponse> patients;
    private String loggedInAs;

    public WSAuthResponse(UserDetailInformationVO informationVO) {
        if (informationVO != null) {
            this.userDetails = informationVO.getDetails() != null ? informationVO.getDetails().toWSUserDetailsResponse() : null;
            this.information = informationVO.getInformation() != null ? informationVO.getInformation().toWSUserInformation() : null;
            this.roleMappings = informationVO.getRoleMappings().stream().map(role -> role.toWsUserRoleMapping()).collect(Collectors.toList());
            //this.patientAssociations = informationVO.getPatientAssociations().stream().map(assoc -> assoc.toWSPatientAssociation()).collect(Collectors.toList());
        }
    }
}
