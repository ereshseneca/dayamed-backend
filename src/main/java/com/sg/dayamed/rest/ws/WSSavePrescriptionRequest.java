package com.sg.dayamed.rest.ws;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.dayamed.config.DayamedIdDeserializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 02/Apr/2019
 **/
@Getter
@Setter
public class WSSavePrescriptionRequest {

    @NotNull(message = "{patient.id.required}")
    @ApiModelProperty(required = true, notes = "Patient Id")
    @JsonDeserialize(using = DayamedIdDeserializer.class)
    private String patientId;
    private String zoneId;
    private List<@Valid WSSavePrescription> prescriptions;
    private List<@Valid Long> deletedPrescriptionIds = new ArrayList<>();
}
