package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.rest.RestResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 04/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPrescriptionMinInfoResponse extends RestResponse {
    private List<WSPrescriptionBasicInfo> prescriptions;
    private Long totalRecords;
}
