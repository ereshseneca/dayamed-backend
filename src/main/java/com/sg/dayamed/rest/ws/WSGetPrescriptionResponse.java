package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.rest.RestResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSGetPrescriptionResponse extends RestResponse {
    private WSPrescriptionInformation prescription;
}
