package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@Getter
@Setter
public class WSSavePatientDetailsRequest {

    @Valid
    @NotNull(message = "{user.details.required}")
    private WSUserDetails userDetails;

    /*@NotNull(message = "{user.role.id.is.required}", groups = {UserRole.class})
    @UserRoleValidator(expectedValues = {UserRole.PATIENT, UserRole.PHARMACIST, UserRole.PROVIDER, UserRole.CARE_TAKER}, message = "{user.role.is.invalid}" )
    private Integer roleId;*/

    @Valid
    private WSUserInformation information;

    private List<Long> patientAssociations;

}
