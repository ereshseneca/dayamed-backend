package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 21/Feb/2019
 **/
@Getter
@Setter
public class WSUserInformation {
    private boolean adminFlag;
    private String deviceId;
    private String emoji;
    private Integer status;
    private String pharmaCompanyName;
    private String license;
    private String providerSpecializationOn;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
}
