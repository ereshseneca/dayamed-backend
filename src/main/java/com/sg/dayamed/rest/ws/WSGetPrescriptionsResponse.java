package com.sg.dayamed.rest.ws;

import com.sg.dayamed.commons.rest.RestResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSGetPrescriptionsResponse extends RestResponse {
    private String patientId;
    private String creatorId;
    private String zoneId;
    private List<WSPrescriptionInformation> prescriptions;
}
