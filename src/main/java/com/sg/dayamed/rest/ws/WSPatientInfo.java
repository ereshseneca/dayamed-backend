package com.sg.dayamed.rest.ws;

import com.sg.dayamed.patients.vo.PatientInfoVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 21/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPatientInfo {
    private String id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String emailId;
    private String mobileNumber;
    private String gender;
    private String age;
    private Boolean hasPrescriptions;
    private ZonedDateTime createdDate;

    public WSPatientInfo(PatientInfoVO vo) {
        this.age = vo.getAge() != null ? vo.getAge().toString() : null;
        this.createdDate = vo.getCreatedDate();
        this.emailId = vo.getEmailId();
        this.middleName = vo.getMiddleName();
        this.firstName = vo.getFirstName();
        this.lastName = vo.getLastName();
        this.mobileNumber = vo.getMobileNumber();
        this.id = vo.getId() != null ? vo.getId().toString() : null;
        this.gender = vo.getGender();
        this.hasPrescriptions = vo.getHasPrescriptions();
    }
}
