package com.sg.dayamed.rest.ws;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.dayamed.config.EncryptJacksonDeserializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "About Login Request Model Object")
public class WSLoginRequest {

    @JsonDeserialize(using = EncryptJacksonDeserializer.class)
    @NotBlank(message = "{login.request.email.id.required}")
    @ApiModelProperty(required = true, notes = "Email Id of user")
    private String emailId;

    @NotBlank(message = "{login.request.password.required}")
    @ApiModelProperty(required = true)
    private String password;

    @ApiModelProperty(required = false)
    private Integer roleId;
}
