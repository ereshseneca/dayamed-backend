package com.sg.dayamed.rest.ws;

import com.sg.dayamed.prescription.vo.PrescriptionInformationVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPrescriptionInformation {
    private String prescriptionId;
    private String name;
    private String providerComment;
    private String illnessDesc;
    private String diagnosis;
    private Boolean active;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
    private List<WSPrescriptionInfoDetails> details = new ArrayList<>();

    public WSPrescriptionInformation(PrescriptionInformationVO informationVO) {
        if (informationVO != null) {
            this.prescriptionId = informationVO.getId() != null ? informationVO.getId().toString() : null;
            this.name = informationVO.getName();
            this.providerComment = informationVO.getProviderComment();
            this.illnessDesc = informationVO.getIllnessDesc();
            this.diagnosis = informationVO.getDiagnosis();
            this.active = informationVO.getActive();
            this.details = informationVO.getDetails().stream().map(WSPrescriptionInfoDetails::new).collect(Collectors.toList());
        }
    }
}
