package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 21/Feb/2019
 **/
@Getter
@Setter
public class WSUserAssociations {
    private List<Long> assosications;
}
