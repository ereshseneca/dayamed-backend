package com.sg.dayamed.rest.ws;

import com.sg.dayamed.util.AdherenceStatus;
import com.sg.dayamed.validators.EnumIDValidator;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Created by Eresh Gorantla on 20/Feb/2019
 **/
@Getter
@Setter
public class WSUpdatePrescriptionDetailsRequest {

    @NotNull(message = "{prescription.details.userid.not.null}")
    private Long userId;

    @NotNull(message = "{prescription.detail.id.not.null}")
    private String id;

    @NotNull(message = "{status.is.required}")
    @EnumIDValidator(enumClass = AdherenceStatus.class, message = "{status.is.not.valid}")
    private Integer statusId;

    private Integer delay;
}
