package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@Getter
@Setter
public class WSAddress {
    private Long id;

    @NotBlank(message = "{user.address.address1.required}")
    @Size(min = 10, max = 50, message = "{user.address.address1.size.message}")
    private String address1;
    private String address2;

    @NotBlank(message = "{user.address.city.required}")
    private String city;

    private Long stateId;
    private Integer countryId;

    @NotBlank(message = "{user.address.zipcode.required}")
    private String zipCode;
    private String location;
}
