package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 25/Mar/2019
 **/
@Getter
@Setter
public class WSErrorResponse {
    private String result;
    private String uid;
    private String fieldName;
    private String errorKey;
    private String errorMessage;
    private List<String> params;
    private Object[] errorparameters;
}
