package com.sg.dayamed.rest.ws;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Getter
@Setter
public class WSStartPrescriptionRequest {

    @NotNull(message = "{patient.prescription.id.required}")
    private List<Long> prescriptionIds;

    @NotBlank(message = "{patient.zone.id.is.required}")
    private String zoneId;

    private Boolean isUpdated = false;

    private Boolean forceStart = false;
}
