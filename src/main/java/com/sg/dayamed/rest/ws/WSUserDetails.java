package com.sg.dayamed.rest.ws;

import com.sg.dayamed.util.Gender;
import com.sg.dayamed.validators.EnumIDValidator;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@Getter
@Setter
public class WSUserDetails {
    private String id;

    @NotNull(message = "{user.age.required}")
    @Min(value = 0,  message = "{user.age.length}")
    private Integer age;

    @NotBlank(message = "{user.email.required}")
    @Size(max = 50, message = "{user.email.length}")
    @Email(message = "{user.email.invalid.format}")
    private String emailId;

    @NotBlank(message = "{user.firstName.required}")
    @Size(min = 2, max = 25, message = "{user.firstName.length}")
    private String firstName;

    private String middleName;
    private String lastName;

    @NotNull(message = "{user.gender.required}")
    @EnumIDValidator(enumClass = Gender.class, message = "{user.gender.invalid.value}")
    private Integer gender;

    private String imageUrl;
    private String imanticUserId;

    @NotNull(message = "{user.active.flag.required}")
    private Boolean active = true;

    @NotBlank(message = "{user.mobile.number.required}")
    private String mobileNumber;

    @NotBlank(message = "{user.password.required}")
    @Size(min = 8, max = 25, message = "{user.password.required.length}")
    private String password;

    private String pwdChangeToken;
    private String pwdTokenExpireDate;
    private String race;
    private String token;
    private String deviceType;
    private String voipToken;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;

    @NotNull(message = "{user.address.required}")
    @Valid
    private WSAddress address;
}
