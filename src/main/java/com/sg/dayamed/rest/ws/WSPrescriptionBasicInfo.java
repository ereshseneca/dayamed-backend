package com.sg.dayamed.rest.ws;

import com.sg.dayamed.prescription.vo.PrescriptionBasicInfoVO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 04/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPrescriptionBasicInfo {
    private String id;
    private String name;
    private String providerComment;
    private String illnessDesc;
    private String diagnosis;
    private Boolean active;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;

    public WSPrescriptionBasicInfo(PrescriptionBasicInfoVO basicInfoVO) {
        this.id = basicInfoVO.getId() != null ? basicInfoVO.getId().toString() : null;
        this.active = basicInfoVO.getActive();
        this.name = basicInfoVO.getName();
        this.providerComment = basicInfoVO.getProviderComment();
        this.illnessDesc = basicInfoVO.getIllnessDesc();
        this.diagnosis = basicInfoVO.getDiagnosis();
        this.createdDate = basicInfoVO.getCreatedDate();
        this.updatedDate = basicInfoVO.getUpdatedDate();
    }
}
