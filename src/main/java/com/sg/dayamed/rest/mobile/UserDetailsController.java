package com.sg.dayamed.rest.mobile;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@RestController(value = UserDetailsController.RESOURCE_NAME + UserDetailsController.API_VERSION)
@RequestMapping("/api/" + UserDetailsController.API_VERSION)
public class UserDetailsController extends com.sg.dayamed.rest.web.UserDetailsController {

    public static final String API_VERSION = "m";
    public static final String RESOURCE_NAME = "UserDetailsController";

}
