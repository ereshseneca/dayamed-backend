package com.sg.dayamed.rest.mobile;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@RestController(value = AuthController.RESOURCE_NAME + AuthController.API_VERSION)
@RequestMapping("/api/" + AuthController.API_VERSION)
@Api(value = "DayaMed Application", description = "Defines Operations for signIn.")
public class AuthController extends BaseRestApiImpl {

    public static final String API_VERSION = "m";
    public static final String RESOURCE_NAME = "AuthController";

}
