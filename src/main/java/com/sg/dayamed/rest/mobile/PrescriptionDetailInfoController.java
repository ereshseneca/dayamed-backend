package com.sg.dayamed.rest.mobile;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@RestController(value = PrescriptionDetailInfoController.RESOURCE_NAME + PrescriptionDetailInfoController.API_VERSION)
@RequestMapping("/api/" + PrescriptionDetailInfoController.API_VERSION)
public class PrescriptionDetailInfoController extends com.sg.dayamed.rest.web.PrescriptionDetailInfoController {

    public static final String API_VERSION = "m";
    public static final String RESOURCE_NAME = "PrescriptionDetailInfoController";
}
