package com.sg.dayamed.rest.mobile;

import com.sg.dayamed.commons.rest.RestApiException;
import com.sg.dayamed.commons.rest.RestResponse;
import com.sg.dayamed.commons.rest.ServiceMethod;
import com.sg.dayamed.rest.ws.WSStartPrescriptionRequest;
import com.sg.dayamed.security.CurrentUser;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.prescription.PrescriptionInfoService;
import com.sg.dayamed.vo.StartPrescriptionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@RestController(value = PrescriptionController.RESOURCE_NAME + PrescriptionController.API_VERSION)
@RequestMapping("/api/" + PrescriptionController.API_VERSION)
public class PrescriptionController extends com.sg.dayamed.rest.web.PrescriptionController {

    public static final String API_VERSION = "m";
    public static final String RESOURCE_NAME = "PrescriptionController";
    private static ServiceMethod SERVICE_METHOD_START_PRESCRIPTION = new ServiceMethod(RESOURCE_NAME, "startPrescription");

    @Autowired
    PrescriptionInfoService prescriptionInfoService;

    @PostMapping("/prescription/start")
    public ResponseEntity<RestResponse> startPrescription(@Valid @RequestBody WSStartPrescriptionRequest request, @CurrentUser UserPrincipal userPrincipal) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_START_PRESCRIPTION, request, service -> {
            prescriptionInfoService.startPrescription(new StartPrescriptionVO(request), userPrincipal);
            return new ResponseEntity<>(new RestResponse(), HttpStatus.OK);
        });
    }

}
