package com.sg.dayamed.rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.commons.rest.RestApiException;
import com.sg.dayamed.commons.rest.RestResponse;
import com.sg.dayamed.commons.rest.ServiceMethod;
import com.sg.dayamed.rest.ws.WSSavePatientDetailsRequest;
import com.sg.dayamed.rest.ws.WSUserDetailInformation;
import com.sg.dayamed.rest.ws.WSUserDetails;
import com.sg.dayamed.rest.ws.WSUserInformation;
import com.sg.dayamed.userdetails.UserDetailsService;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.Gender;
import com.sg.dayamed.util.UserRole;
import com.sg.dayamed.vo.AddressVO;
import com.sg.dayamed.vo.SaveUserDetailsVO;
import com.sg.dayamed.vo.UserDetailInformationVO;
import com.sg.dayamed.vo.UserDetailsVO;
import com.sg.dayamed.vo.UserInformationVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 15/Feb/2019
 **/
@RestController(value = UserDetailsController.RESOURCE_NAME + UserDetailsController.API_VERSION)
@RequestMapping("/api/" + UserDetailsController.API_VERSION)
public class UserDetailsController extends BaseRestApiImpl {

    public static final String API_VERSION = "w";
    public static final String RESOURCE_NAME = "UserDetailsController";

    public static final ServiceMethod SERVICE_METHOD_USER_DETAILS_CONTROLLER_SAVE_USER_DETAILS = new ServiceMethod(RESOURCE_NAME, "saveUserDetails");

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    ObjectMapper objectMapper;

    @PostMapping("/userdetails/{role}")
    public ResponseEntity<RestResponse> saveUserDetails(@PathVariable(name = "role") String role, @Valid @RequestBody WSSavePatientDetailsRequest request) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_USER_DETAILS_CONTROLLER_SAVE_USER_DETAILS, request, service -> {
            UserRole userRole = UserRole.getUserRole(role);
            if (role == null) {
                throw new DataValidationException("role", APIErrorKeys.ERROR_USER_ROLE_INVALID, new Object[] {role});
            }
            SaveUserDetailsVO saveUserDetailsVO = mapSaveUserDetailsVO(request, userRole);
            UserDetailInformationVO detailInformationVO = new UserDetailInformationVO();
            if (StringUtils.equalsIgnoreCase(UserRole.PATIENT.getValue(), role)) {
                detailInformationVO = userDetailsService.saveUserDetails(saveUserDetailsVO, true);
            } else {
                detailInformationVO = userDetailsService.saveUserDetails(saveUserDetailsVO, false);
            }
            WSUserDetailInformation response = new WSUserDetailInformation();
            WSUserDetails userDetails = new WSUserDetails();
            WSUserInformation userInformation = new WSUserInformation();
            BeanUtils.copyProperties(detailInformationVO, response);
            BeanUtils.copyProperties(detailInformationVO.getDetails(), userDetails);
            BeanUtils.copyProperties(detailInformationVO.getInformation(), userInformation);
            response.setDetails(userDetails);
            response.setInformation(userInformation);
            return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
        }, Stream.of("emailId", "mobileNumber").collect(Collectors.toList()), null, WSUserDetailInformation.class);
    }

    /*@GetMapping("/user/{id}")
    public ResponseEntity<RestResponse> getUserById(@PathVariable (name = "id") String id) throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
           return null;
        });
    }*/

    private SaveUserDetailsVO mapSaveUserDetailsVO(WSSavePatientDetailsRequest request, UserRole userRole) {
        SaveUserDetailsVO saveUserDetailsVO = new SaveUserDetailsVO();
        UserDetailsVO userDetailsVO = new UserDetailsVO();
        AddressVO addressVO = new AddressVO();
        BeanUtils.copyProperties(request.getUserDetails().getAddress(), addressVO);
        BeanUtils.copyProperties(request.getUserDetails(), userDetailsVO);
        userDetailsVO.setGender(Gender.getGender(request.getUserDetails().getGender()).getValue());
        userDetailsVO.setAddress(addressVO);
        saveUserDetailsVO.setUserDetails(userDetailsVO);
        if (request.getInformation() != null) {
            UserInformationVO informationVO = new UserInformationVO();
            BeanUtils.copyProperties(request.getInformation(), informationVO);
            saveUserDetailsVO.setUserInformation(informationVO);
        }
        saveUserDetailsVO.setRoleId(userRole.getId());
        saveUserDetailsVO.setPatientAssociations(request.getPatientAssociations());
        return saveUserDetailsVO;
    }
}
