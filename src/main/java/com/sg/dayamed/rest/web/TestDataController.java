package com.sg.dayamed.rest.web;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.TestDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/
@RestController(value = TestDataController.RESOURCE_NAME + TestDataController.API_VERSION)
@RequestMapping("/api/" + TestDataController.API_VERSION)
public class TestDataController extends BaseRestApiImpl {

    public static final String API_VERSION = "w";
    public static final String RESOURCE_NAME = "TestDataController";

    @Autowired
    private TestDataService testDataService;

   /* @PostMapping("/test/data")
    public ResponseEntity<RestResponse> saveTestData(@RequestBody WSTestData request) throws RestApiException {
        return inboundServiceCall(request, service -> {
            testDataService.saveTestData(request);
            return new ResponseEntity<>(new RestResponse(), HttpStatus.OK);
        });
    }

    @GetMapping("/test/data/all")
    public ResponseEntity<RestResponse> getAllTestData() throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
            List<TestData> list = testDataService.getTestData();
            WSTestDataResponse response = new WSTestDataResponse();
            response.setTestData(list.stream().map(WSTestData::new).collect(Collectors.toList()));
            return new ResponseEntity<>(response, HttpStatus.OK);
        });
    }

    @GetMapping("/test/data/city/{cityName}")
    public ResponseEntity<RestResponse> getTestDataByCityName(@PathVariable(name = "cityName") String cityName) throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
            List<TestData> list = testDataService.getTestDataByCity(cityName);
            WSTestDataResponse response = new WSTestDataResponse();
            response.setTestData(list.stream().map(WSTestData::new).collect(Collectors.toList()));
            return new ResponseEntity<>(response, HttpStatus.OK);
        });
    }*/
}
