package com.sg.dayamed.rest.web;

import com.sg.dayamed.cache.RedisVOComponent;
import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.commons.rest.RestApiException;
import com.sg.dayamed.commons.rest.RestRequest;
import com.sg.dayamed.commons.rest.RestResponse;
import com.sg.dayamed.commons.rest.ServiceMethod;
import com.sg.dayamed.rest.ws.WSGetPrescriptionResponse;
import com.sg.dayamed.rest.ws.WSGetPrescriptionsResponse;
import com.sg.dayamed.rest.ws.WSPrescriptionBasicInfo;
import com.sg.dayamed.rest.ws.WSPrescriptionInformation;
import com.sg.dayamed.rest.ws.WSPrescriptionMinInfoResponse;
import com.sg.dayamed.rest.ws.WSSavePrescriptionRequest;
import com.sg.dayamed.security.CurrentUser;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.prescription.PrescriptionDetailsProcessingService;
import com.sg.dayamed.prescription.PrescriptionService;
import com.sg.dayamed.prescription.vo.PrescriptionInformationVO;
import com.sg.dayamed.prescription.vo.PrescriptionMinInfoVO;
import com.sg.dayamed.prescription.vo.SavePrescriptionVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@RestController(value = PrescriptionController.RESOURCE_NAME + PrescriptionController.API_VERSION)
@RequestMapping("/api/" + PrescriptionController.API_VERSION)
@Api(value = "DayaMed Application", description = "Contains Operations for Prescriptions.")
public class PrescriptionController extends BaseRestApiImpl {

    public static final String API_VERSION = "w";
    public static final String RESOURCE_NAME = "PrescriptionController";

    public static final ServiceMethod SERVICE_METHOD_PRESCRIPTION_CONTROLLER_SAVE_PRESCRIPTION = new ServiceMethod(RESOURCE_NAME, "savePrescription");
    public static final ServiceMethod SERVICE_METHOD_PRESCRIPTION_CONTROLLER_GET_PATIENT_PRESCRIPTIONS = new ServiceMethod(RESOURCE_NAME, "getPatientPrescriptions");
    public static final ServiceMethod SERVICE_METHOD_PRESCRIPTION_CONTROLLER_GET_PRESCRIPTION = new ServiceMethod(RESOURCE_NAME, "getPrescription");

    @Autowired
    PrescriptionService prescriptionService;


    @Autowired
    PrescriptionDetailsProcessingService detailsProcessingService;

    @Autowired
    RedisVOComponent redisVOComponent;

    @PutMapping("/prescription")
    @ApiOperation(value = "Method to save or update prescription for user", response = WSGetPrescriptionsResponse.class)
    public ResponseEntity<RestResponse> savePrescription(@RequestBody @Valid WSSavePrescriptionRequest request, @ApiIgnore @CurrentUser UserPrincipal userPrincipal) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_PRESCRIPTION_CONTROLLER_SAVE_PRESCRIPTION, request, service -> {
            List<PrescriptionInformationVO> informationVOS = prescriptionService.savePrescription(new SavePrescriptionVO(request), userPrincipal);
            WSGetPrescriptionsResponse response = new WSGetPrescriptionsResponse();
            if (CollectionUtils.isNotEmpty(informationVOS)) {
                List<WSPrescriptionInformation> informationS = informationVOS.stream().map(WSPrescriptionInformation::new).collect(Collectors.toList());
                response.setPrescriptions(informationS);
            }
            response.setCreatorId(userPrincipal.getRoleMappingId() != null ? userPrincipal.getRoleMappingId().toString() : null);
            response.setPatientId(request.getPatientId() != null ? request.getPatientId().toString() : null);
            response.setZoneId(request.getZoneId());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }, null, Stream.of("patientId", "creatorId", "prescriptionId").collect(Collectors.toList()), WSGetPrescriptionsResponse.class);
    }

    @GetMapping("/prescriptions/{patientId}")
    @ApiOperation(value = "Method get prescription information of patient", response = WSGetPrescriptionsResponse.class)
    public ResponseEntity<RestResponse> getPatientPrescriptions(@PathVariable (name = "patientId") String patientId, @ApiIgnore @CurrentUser UserPrincipal userPrincipal,
                                                                @ApiParam(value = "limit for results") @RequestParam(name = "limit", defaultValue = "-1") Integer limit,
                                                                @ApiParam(value = "offset of page") @RequestParam(name = "offset", defaultValue = "0") Integer offset) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_PRESCRIPTION_CONTROLLER_GET_PATIENT_PRESCRIPTIONS, new RestRequest(), service -> {
            PrescriptionMinInfoVO minInfoVO = prescriptionService.getPrescriptions(patientId, userPrincipal, limit, offset);
            WSPrescriptionMinInfoResponse response = new WSPrescriptionMinInfoResponse();
            response.setPrescriptions(minInfoVO.getPrescriptions().stream().map(WSPrescriptionBasicInfo::new).collect(Collectors.toList()));
            response.setTotalRecords(minInfoVO.getTotalRecords());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }, null, Stream.of("id").collect(Collectors.toList()), WSPrescriptionMinInfoResponse.class);
    }

    @GetMapping("/prescription/{id}")
    @ApiOperation(value = "Method get prescription information with its id", response = WSGetPrescriptionResponse.class)
    public ResponseEntity<RestResponse> getPrescription(@PathVariable (name = "id") Long id, @ApiIgnore @CurrentUser UserPrincipal userPrincipal) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_PRESCRIPTION_CONTROLLER_GET_PRESCRIPTION, new RestRequest(), service -> {
            PrescriptionInformationVO informationVO = prescriptionService.getPrescriptionInfo(id, userPrincipal);
            WSGetPrescriptionResponse response = new WSGetPrescriptionResponse();
            response.setPrescription(new WSPrescriptionInformation(informationVO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }, null, Stream.of("patientId", "creatorId").collect(Collectors.toList()), WSGetPrescriptionResponse.class);
    }

    /*@PostMapping("/prescription/details")
    public ResponseEntity<RestResponse> startPrescription() throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
           detailsProcessingService.persistPrescriptionDetails();
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.OK);
        });
    }

    @GetMapping("/prescription/get")
    public ResponseEntity<RestResponse> getMethod() throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
            System.out.println("Web Resource");
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.OK);
        });
    }

    @PostMapping("/redis/test")
    public ResponseEntity<RestResponse> testRedis(@RequestBody RedisVO redisVO) throws RestApiException {
        return inboundServiceCall(redisVO, service -> {
            redisVOComponent.loadData(redisVO);
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.OK);
        });
    }*/

}
