package com.sg.dayamed.rest.web;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.prescription.PrescriptionDetailInfoService;
import com.sg.dayamed.prescription.PrescriptionInfoService;
import com.sg.dayamed.prescription.PrescriptionNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@RestController(value = PrescriptionDetailInfoController.RESOURCE_NAME + PrescriptionDetailInfoController.API_VERSION)
@RequestMapping("/api/" + PrescriptionDetailInfoController.API_VERSION)
public class PrescriptionDetailInfoController extends BaseRestApiImpl {

    public static final String API_VERSION = "w";
    public static final String RESOURCE_NAME = "PrescriptionDetailInfoController";

    @Autowired
    private PrescriptionDetailInfoService prescriptionDetailInfoService;

    @Autowired
    private PrescriptionInfoService prescriptionInfoService;

    @Autowired
    PrescriptionNotificationService prescriptionNotificationService;

    /*@PostMapping("/prescription/start")
    public ResponseEntity<RestResponse> startPrescription(@Valid @RequestBody WSStartPrescriptionRequest request) throws RestApiException {
        return inboundServiceCall(request, service -> {
           // prescriptionDetailInfoService.savePrescriptionDetailInfo(new PrescriptionDetailInfoVO(request));
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.OK);
        });
    }*/

    /*@GetMapping("/test")
    public ResponseEntity<RestResponse> test() throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
            prescriptionInfoService.addPrescriptionData();
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.OK);
        });
    }

    @PostMapping("/job")
    public ResponseEntity<RestResponse> test1() throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
            prescriptionNotificationService.persistNotificationData();
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.OK);
        });
    }

    @PutMapping("/random/data")
    public ResponseEntity<RestResponse> putRandomData() throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
            prescriptionInfoService.generateData();
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.OK);
        });
    }*/

    /*@PostMapping("/notification/processings")
    public ResponseEntity<RestResponse> processNotifications() throws RestApiException {
        return inboundServiceCall(new RestRequest(), service -> {
            prescriptionNotificationService.persistPrescriptionNotifications(ZonedDateTime.now());
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.OK);
        });
    }*/

}
