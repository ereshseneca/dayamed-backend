package com.sg.dayamed.rest.web;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.commons.rest.RestApiException;
import com.sg.dayamed.commons.rest.RestRequest;
import com.sg.dayamed.commons.rest.RestResponse;
import com.sg.dayamed.commons.rest.ServiceMethod;
import com.sg.dayamed.rest.ws.WSAuthResponse;
import com.sg.dayamed.rest.ws.WSLoginRequest;
import com.sg.dayamed.AuthService;
import com.sg.dayamed.vo.AuthRequestVO;
import com.sg.dayamed.vo.AuthResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@RestController(value = AuthController.RESOURCE_NAME + AuthController.API_VERSION)
@RequestMapping("/api/" + AuthController.API_VERSION)
@Api(value = "DayaMed Application", description = "Defines Operations for signIn.")
public class AuthController extends BaseRestApiImpl {

    public static final String API_VERSION = "w";
    public static final String RESOURCE_NAME = "AuthController";

    public static final ServiceMethod SERVICE_METHOD_AUTH_CONTROLLER_LOGIN = new ServiceMethod(RESOURCE_NAME, "login");

    @Autowired
    AuthService authService;

    @PostMapping("/login")
    @ApiOperation(value = "Method to sign in registered users.", response = WSAuthResponse.class)
    public ResponseEntity<RestResponse> loginUser(@RequestBody @Valid WSLoginRequest request) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_AUTH_CONTROLLER_LOGIN, request, service -> {
            AuthResponseVO authResponseVO = authService.signIn(new AuthRequestVO(request));
            WSAuthResponse response = new WSAuthResponse(authResponseVO.getInformation());
            response.setAccessKey(authResponseVO.getAccessKey());
            response.setLoggedInAs(CollectionUtils.isNotEmpty(authResponseVO.getRoles()) ? authResponseVO.getRoles().get(0) : null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }, Stream.of("emailId", "mobileNumber", "firstName", "age").collect(Collectors.toList()), Stream.of("id").collect(Collectors.toList()), WSAuthResponse.class);
    }

    @GetMapping("/special/{text}")
    public ResponseEntity<RestResponse> specialCase(@PathVariable("text") String text) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_AUTH_CONTROLLER_LOGIN, new RestRequest(), service -> {
            RestResponse restResponse = new RestResponse();
            restResponse.setResult(text);
           return new ResponseEntity<>(restResponse, HttpStatus.OK);
        });
    }
}
