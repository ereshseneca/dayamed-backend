package com.sg.dayamed.rest.web;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.commons.rest.RestApiException;
import com.sg.dayamed.commons.rest.RestRequest;
import com.sg.dayamed.commons.rest.RestResponse;
import com.sg.dayamed.commons.rest.ServiceMethod;
import com.sg.dayamed.rest.ws.WSGetPatientInfoResponse;
import com.sg.dayamed.rest.ws.WSPatientInfo;
import com.sg.dayamed.rest.ws.WSPatientInfoResponse;
import com.sg.dayamed.security.CurrentUser;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.patients.PatientService;
import com.sg.dayamed.patients.vo.PatientInfoVO;
import com.sg.dayamed.patients.vo.PatientInfoVOResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 21/Mar/2019
 **/
@RestController(value = PatientController.RESOURCE_NAME + PatientController.API_VERSION)
@RequestMapping("/api/" + PatientController.API_VERSION)
@Api(value = "DayaMed Application", description = "Defines Operations for Patients.")
@Validated
public class PatientController extends BaseRestApiImpl {

    public static final String API_VERSION = "w";
    public static final String RESOURCE_NAME = "PatientController";

    public static final ServiceMethod SERVICE_METHOD_PATIENT_CONTROLLER_GET_PATIENTS = new ServiceMethod(RESOURCE_NAME, "getPatients");
    public static final ServiceMethod SERVICE_METHOD_PATIENT_CONTROLLER_GET_PATIENT = new ServiceMethod(RESOURCE_NAME, "getPatient");
    public static final ServiceMethod SERVICE_METHOD_PATIENT_CONTROLLER_SEARCH_PATIENT = new ServiceMethod(RESOURCE_NAME, "searchPatients");

    @Autowired
    PatientService patientService;

    @GetMapping("/patients")
    @ApiOperation(value = "Method to get all patients for logged in user.", response = WSPatientInfoResponse.class)
    public ResponseEntity<RestResponse> getPatients(@ApiParam(value = "limit for results") @RequestParam(name = "limit", defaultValue = "-1") Integer limit,
                                                    @ApiParam(value = "offset of page") @RequestParam(name = "offset", defaultValue = "0") Integer offset,
                                                    @ApiIgnore @CurrentUser UserPrincipal userPrincipal) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_PATIENT_CONTROLLER_GET_PATIENTS, new RestRequest(), service -> {
                   PatientInfoVOResponse voResponse = patientService.getPatients(limit, offset, userPrincipal);
                    WSPatientInfoResponse response = new WSPatientInfoResponse();
                    response.setPatients(voResponse.getPatients().stream().map(WSPatientInfo::new).collect(Collectors.toList()));
                    response.setTotalRecords(voResponse.getTotalRecords());
                    return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
                }, Stream.of("emailId", "mobileNumber", "firstName", "age", "middleName", "lastName", "gender").collect(Collectors.toList()),
                Stream.of("id").collect(Collectors.toList()), WSPatientInfoResponse.class);
    }

    @RequestMapping(value = "/patient/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Method to get patient details from Id, provided logged user should have privileges to view data.", response = WSGetPatientInfoResponse.class)
    public ResponseEntity<RestResponse> getPatient(@PathVariable(name = "id") String id, @ApiIgnore @CurrentUser UserPrincipal userPrincipal) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_PATIENT_CONTROLLER_GET_PATIENT, new RestRequest(), service -> {
                    PatientInfoVO patientInfoVO = patientService.getPatient(id, userPrincipal);
                    WSGetPatientInfoResponse response = new WSGetPatientInfoResponse();
                    response.setInfo(new WSPatientInfo(patientInfoVO));
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }, Stream.of("emailId", "mobileNumber", "firstName", "age", "middleName", "lastName", "gender").collect(Collectors.toList()),
                Stream.of("id").collect(Collectors.toList()), WSGetPatientInfoResponse.class);
    }

    @GetMapping("/patients/search")
    @PreAuthorize("hasAnyAuthority('Provider', 'Admin', 'Pharmacist', 'Caretaker')")
    public ResponseEntity<RestResponse> searchPatients(@ApiParam(value = "Search Criteria") @RequestParam(name = "text") String text,
                                                       @ApiParam(value = "limit for results") @RequestParam(name = "limit", defaultValue ="-1") @Valid @Min(value = 1, message =
                                                               "{limit.min.value}") Integer limit,
                                                       @ApiParam(value = "offset of page") @RequestParam(name = "offset", defaultValue = "-1") Integer offset,
                                                       @ApiParam(value = "Search Only active patients") @RequestParam(name = "active", defaultValue = "false") Boolean active,
                                                       @ApiIgnore @CurrentUser UserPrincipal userPrincipal) throws RestApiException {
        return inboundServiceCall(SERVICE_METHOD_PATIENT_CONTROLLER_SEARCH_PATIENT, new RestRequest(), service -> {
            PatientInfoVOResponse voResponse = patientService.searchPatients(text, limit, offset, userPrincipal, active);
            WSPatientInfoResponse response = new WSPatientInfoResponse();
            response.setPatients(voResponse.getPatients().stream().map(WSPatientInfo::new).collect(Collectors.toList()));
            response.setTotalRecords(voResponse.getTotalRecords());
            return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
        });

    }


}
