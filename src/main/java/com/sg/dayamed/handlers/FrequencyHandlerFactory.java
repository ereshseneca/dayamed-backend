package com.sg.dayamed.handlers;

import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.FrequencyType;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 12/Apr/2019
 **/
@Component
public class FrequencyHandlerFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public FrequencyHandler createFrequencyHandler(Integer type) throws DataValidationException {
        FrequencyType frequencyType = FrequencyType.getFrequencyType(type);
        if (type == null) {
            throw new DataValidationException("type", APIErrorKeys.ERROR_INVALID_FREQUENCY_TYPE);
        }
        switch (frequencyType) {
            case DAILY:
                return applicationContext.getBean(DailyFrequencyHandler.class);
            case MONTHLY:
                return applicationContext.getBean(MonthlyFrequencyHandler.class);
            case WEEKLY:
                return applicationContext.getBean(WeeklyFrequencyHandler.class);
            default:
                throw new DataValidationException("type", APIErrorKeys.ERROR_INVALID_FREQUENCY_TYPE_FOR_IMPLEMENTATION);
        }
    }
}
