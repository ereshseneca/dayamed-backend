package com.sg.dayamed.handlers.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 12/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionInfoHandlerData {
    private ZonedDateTime startDate;
    private ZonedDateTime endDate;
    private Integer duration;
    private Integer frequencyTypeId;
    private Integer recurrence;
    private String startDateText;
}
