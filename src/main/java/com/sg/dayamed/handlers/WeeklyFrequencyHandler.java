package com.sg.dayamed.handlers;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.util.DateUtils;
import com.sg.dayamed.handlers.vo.PrescriptionInfoHandlerData;
import com.sg.dayamed.persistence.domain.FrequencyDetails;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 12/Apr/2019
 **/
@Component
public class WeeklyFrequencyHandler implements FrequencyHandler {

    @Override
    public PrescriptionInfoHandlerData getPrescriptionInfoHandlerData(PrescriptionInfo prescriptionInfo, Boolean forcedStart) throws ApplicationException {
        try {
            FrequencyDetails frequencyDetails = prescriptionInfo.getFrequency();
            PrescriptionInfoHandlerData handlerData = new PrescriptionInfoHandlerData();
            handlerData.setStartDateText(frequencyDetails.getStartDate());
            handlerData.setDuration(frequencyDetails.getDuration());
            handlerData.setRecurrence(frequencyDetails.getRecurrence());
            handlerData.setFrequencyTypeId(frequencyDetails.getTypeId());
            if (forcedStart) {
                handlerData.setStartDate(DateUtils.getCurrentZonedDateTimeForZone(prescriptionInfo.getZoneId()));
            } else {
                handlerData.setStartDate(DateUtils.getZonedDateTime(handlerData.getStartDateText(), prescriptionInfo.getZoneId()));
            }
            Integer numberOfWeeks = handlerData.getDuration() * handlerData.getRecurrence();
            handlerData.setEndDate(handlerData.getStartDate().plusWeeks(NumberUtils.toLong(numberOfWeeks.toString())));
            return handlerData;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }
}
