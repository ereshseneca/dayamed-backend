package com.sg.dayamed.handlers;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.handlers.vo.PrescriptionInfoHandlerData;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;

/**
 * Created by Eresh Gorantla on 12/Apr/2019
 **/

public interface FrequencyHandler {
    abstract PrescriptionInfoHandlerData getPrescriptionInfoHandlerData(PrescriptionInfo prescriptionInfo, Boolean forcedStart) throws ApplicationException;
}
