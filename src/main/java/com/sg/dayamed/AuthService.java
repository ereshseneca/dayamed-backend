package com.sg.dayamed;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.vo.AuthRequestVO;
import com.sg.dayamed.vo.AuthResponseVO;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/

public interface AuthService {
    AuthResponseVO login(AuthRequestVO requestVO) throws ApplicationException;
    AuthResponseVO signIn(AuthRequestVO requestVO) throws ApplicationException;
}
