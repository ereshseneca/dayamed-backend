package com.sg.dayamed.patients;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.patients.vo.PatientInfoVO;
import com.sg.dayamed.patients.vo.PatientInfoVOResponse;
import com.sg.dayamed.security.UserPrincipal;

/**
 * Created by Eresh Gorantla on 21/Mar/2019
 **/

public interface PatientService {
    PatientInfoVOResponse getPatients(Integer limit, Integer offset, UserPrincipal userPrincipal) throws ApplicationException;
    PatientInfoVO getPatient(String id, UserPrincipal userPrincipal) throws ApplicationException;
    PatientInfoVOResponse searchPatients(String text, Integer limit, Integer offset, UserPrincipal userPrincipal, Boolean active) throws ApplicationException;
}
