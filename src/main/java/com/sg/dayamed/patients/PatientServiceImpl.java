package com.sg.dayamed.patients;

import com.sg.dayamed.commons.crypto.CryptoUtil;
import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.commons.exception.DataValidationException;
import com.sg.dayamed.commons.exception.ExceptionLogUtil;
import com.sg.dayamed.patients.vo.PatientInfoVO;
import com.sg.dayamed.patients.vo.PatientInfoVOResponse;
import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import com.sg.dayamed.persistence.domain.view.PrescriptionInfoV;
import com.sg.dayamed.persistence.repository.PatientAssociationInformationVRepository;
import com.sg.dayamed.persistence.repository.PrescriptionInfoVRepository;
import com.sg.dayamed.persistence.repository.UserInformationVRepository;
import com.sg.dayamed.security.UserPrincipal;
import com.sg.dayamed.util.APIErrorKeys;
import com.sg.dayamed.util.UserRole;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 21/Mar/2019
 **/
@Service
public class PatientServiceImpl implements PatientService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    PatientAssociationInformationVRepository associationInformationVRepository;

    @Autowired
    PrescriptionInfoVRepository infoVRepository;

    @Autowired
    UserInformationVRepository userInformationVRepository;

    @Override
    public PatientInfoVOResponse getPatients(Integer limit, Integer offset, UserPrincipal userPrincipal) throws ApplicationException {
        try {
            Long userId = userPrincipal.getId();
            UserRole userRole = getUserRole(userPrincipal);
            limit = limit == -1 ? Integer.MAX_VALUE : limit;
            Page<PatientAssociationInformationV> associationInformationPages = associationInformationVRepository.findByUserIdAndRoleId(userId, userRole.getId(),
                    PageRequest.of(offset, limit));
            List<PatientInfoVO> patientInfoVOS = getPatientInfoVOS(associationInformationPages.getContent());
            PatientInfoVOResponse response = new PatientInfoVOResponse();
            response.setTotalRecords(associationInformationPages.getTotalElements());
            response.setPatients(patientInfoVOS);
            return response;
        } catch (Exception e) {
            ExceptionLogUtil.log(e, logger);
            throw new ApplicationException(e);
        }
    }

    private UserRole getUserRole(UserPrincipal userPrincipal) throws DataValidationException {
        List<String> roles = userPrincipal.getAuthorities().stream().map(auth -> auth.getAuthority()).collect(Collectors.toList());
        String role = roles.get(0);
        UserRole userRole = UserRole.getUserRole(role);
        if (userRole == null) {
            throw new DataValidationException("role", APIErrorKeys.ERROR_USER_ROLE_INVALID);
        }
        return userRole;
    }

    @Override
    public PatientInfoVO getPatient(String id, UserPrincipal userPrincipal) throws ApplicationException {
        try {
            String decryptedId = CryptoUtil.decryptIdField(id);
            if (StringUtils.isBlank(decryptedId)) {
                throw new DataValidationException("id", APIErrorKeys.ERROR_USER_DETAILS_NOT_FOUND_FOR_ID, new Object[]{ id });
            }
            Long patientId = NumberUtils.toLong(decryptedId);
            UserRole userRole = getUserRole(userPrincipal);
            List<PatientAssociationInformationV> associationInformationVS = associationInformationVRepository.findByPatientId(patientId);
            associationInformationVS = associationInformationVS.stream().filter(ass -> ass.getUserId().equals(userPrincipal.getId())).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(associationInformationVS)) {
                throw new DataValidationException("id", APIErrorKeys.ERROR_NO_PRIVILEGES_TO_VIEW_PATIENT_DATA);
            }
            associationInformationVS = associationInformationVS.stream().filter(ass -> userRole.getId().equals(ass.getRoleId())).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(associationInformationVS)) {
                throw new DataValidationException("id", APIErrorKeys.ERROR_NO_PRIVILEGES_TO_VIEW_PATIENT_DATA);
            }
            return new PatientInfoVO(associationInformationVS.get(0), null);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public PatientInfoVOResponse searchPatients(String text, Integer limit, Integer offset, UserPrincipal userPrincipal, Boolean active) throws ApplicationException {
        PatientInfoVOResponse response = new PatientInfoVOResponse();
        List<PatientInfoVO> patientInfoVOS = new ArrayList<>();
        List<PatientAssociationInformationV> associationInformationVS;
        try {
            Integer newLimit = limit.equals(-1) ? Integer.MAX_VALUE : limit;
            Integer newOffset = offset.equals(-1) ? NumberUtils.INTEGER_ZERO : offset;
            if (active) {
                associationInformationVS = associationInformationVRepository.findByUserRoleMapIdAndActiveOrderByActiveDesc(userPrincipal.getRoleMappingId(), true);
            } else {
                associationInformationVS = associationInformationVRepository.findByUserRoleMapIdOrderByActiveDesc(userPrincipal.getRoleMappingId());
            }
            Long totalRecords = NumberUtils.LONG_ZERO;
            if (CollectionUtils.isNotEmpty(associationInformationVS)) {
                AtomicInteger count = new AtomicInteger();
                Collection<List<PatientAssociationInformationV>> collections  =
                        associationInformationVS.stream().filter(data -> StringUtils.startsWith(data.getPatientFirstName(), text) || StringUtils.startsWith(data.getPatientLastName(), text)).collect(Collectors.groupingBy(it -> count.getAndIncrement()/newLimit)).values();
                Integer index = 0;
                List<PatientAssociationInformationV> patientAssociationInformationVS = new ArrayList<>();
                for (Collection collection : collections) {
                    if (newOffset.equals(index)) {
                        patientAssociationInformationVS = new ArrayList<PatientAssociationInformationV>(collection);
                        break;
                    }
                    index ++;
                }
                patientInfoVOS = getPatientInfoVOS(patientAssociationInformationVS);
                for (Collection collection : collections) {
                    List<PatientAssociationInformationV> informationVS = new ArrayList<>(collection);
                    totalRecords += informationVS.size();
                }
            }
            response.setTotalRecords(totalRecords);
            response.setPatients(patientInfoVOS);
            return response;
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    private List<PatientInfoVO> getPatientInfoVOS(List<PatientAssociationInformationV> patientAssociationInformationVS) {
        List<PatientInfoVO> patientInfoVOS = new ArrayList<>();
        List<Long> patientIds = patientAssociationInformationVS.stream().map(PatientAssociationInformationV::getPatientId).distinct().collect(Collectors.toList());
        List<PrescriptionInfoV> prescriptionInfoVS = infoVRepository.findByUserIdIn(patientIds);
        patientAssociationInformationVS.forEach(details -> {
            PrescriptionInfoV prescriptionInfoV = prescriptionInfoVS.stream().filter(pres -> pres.getUserId().equals(details.getPatientId())).findFirst().orElse(null);
            patientInfoVOS.add(new PatientInfoVO(details, prescriptionInfoV != null));
        });
        return patientInfoVOS;
    }
}
