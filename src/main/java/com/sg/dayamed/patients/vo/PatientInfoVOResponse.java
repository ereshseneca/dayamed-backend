package com.sg.dayamed.patients.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 07/Apr/2019
 **/
@Getter
@Setter
public class PatientInfoVOResponse {
    private List<PatientInfoVO> patients = new ArrayList<>();
    private Long totalRecords;
}
