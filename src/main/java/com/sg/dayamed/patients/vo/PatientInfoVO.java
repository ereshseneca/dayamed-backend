package com.sg.dayamed.patients.vo;

import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 21/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PatientInfoVO {
    private Long id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String emailId;
    private String mobileNumber;
    private String gender;
    private Integer age;
    private Boolean hasPrescriptions;
    private ZonedDateTime createdDate;
    private String role;
    private Boolean active;
    private String race;
    private ZonedDateTime updatedDate;

    public PatientInfoVO(PatientAssociationInformationV informationV, Boolean hasPrescriptions) {
        this.age = informationV.getPatientAge();
        this.createdDate = informationV.getPatientCreatedDate();
        this.emailId = informationV.getPatientEmailId();
        this.middleName = informationV.getPatientMiddleName();
        this.firstName = informationV.getPatientFirstName();
        this.lastName = informationV.getPatientLastName();
        this.mobileNumber = informationV.getPatientMobileNumber();
        this.id = informationV.getPatientId();
        this.gender = informationV.getPatientGender();
        this.hasPrescriptions = hasPrescriptions;
    }

    /*public PatientInfoVO(UserInformationV informationV, Boolean hasPrescriptions) {
        if (informationV != null) {
            this.id = informationV.getUserId();
            this.role = informationV.getRoleName();
            this.firstName = informationV.getUserFirstName();
            this.lastName = informationV.getUserLastName();
            this.middleName = informationV.getUserMiddleName();
            this.gender = informationV.getGender();
            this.active = informationV.getUserActive();
            this.age = informationV.getUserAge();
            this.mobileNumber = informationV.getUserMobileNumber();
            this.race = informationV.getRace();
            this.createdDate = informationV.getUserCreatedDate();
            this.updatedDate = informationV.getUserUpdatedDate();
            this.hasPrescriptions = hasPrescriptions;
        }
    }*/
}
