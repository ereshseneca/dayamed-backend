package com.sg.dayamed;

import com.sg.dayamed.commons.exception.ApplicationException;
import com.sg.dayamed.persistence.domain.JsonSample;
import com.sg.dayamed.persistence.domain.TestData;
import com.sg.dayamed.persistence.repository.TestDataRepositiry;
import com.sg.dayamed.rest.ws.WSTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/
@Service
public class TestDataService {

    @Autowired
    TestDataRepositiry dataRepositiry;

    public void saveTestData(WSTestData testData) throws ApplicationException {
        try {
            TestData data = new TestData();
            JsonSample sample = new JsonSample();
            sample.setId(testData.getId());
            sample.setCity(testData.getCity());
            sample.setName(testData.getName());
            data.setData(sample);
            dataRepositiry.save(data);
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    public List<TestData> getTestData() throws ApplicationException {
        try {
            return dataRepositiry.findAll();
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    public List<TestData> getTestDataByCity(String city) throws ApplicationException {
        try {
            return dataRepositiry.fingByCityName(city + "%");
        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }
}
