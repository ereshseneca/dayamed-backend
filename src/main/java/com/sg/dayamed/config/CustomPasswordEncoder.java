package com.sg.dayamed.config;

import com.sg.dayamed.jpa.converters.CipherInitializer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.util.Base64;

/**
 * Created by Eresh Gorantla on 20/Mar/2019
 **/

public class CustomPasswordEncoder implements PasswordEncoder  {

    CipherInitializer cipherInitializer = new CipherInitializer();

    @Override
    public String encode(CharSequence rawPassword) {
        String data = null;
        try {
            Cipher cipher = cipherInitializer.prepareAndInitCipher(Cipher.ENCRYPT_MODE, "ZY6PiJQi9AM8ohNIx4YPbC0iFbLIRInw");
            data = encrypt(cipher, rawPassword.toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return data;
    }
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return StringUtils.equals(rawPassword, encode(encodedPassword));
    }

    protected String encrypt(Cipher cipher, String value)  {
        try {
            byte[] bytesToEncrypt = value.getBytes();
            byte[] encryptedBytes = callCipherDoFinal(cipher, bytesToEncrypt);
            return Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (Exception e) {
            return null;
        }
    }

    byte[] callCipherDoFinal(Cipher cipher, byte[] bytes) throws IllegalBlockSizeException, BadPaddingException {
        return cipher.doFinal(bytes);
    }


}
