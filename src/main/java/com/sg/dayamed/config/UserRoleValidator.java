package com.sg.dayamed.config;

import com.sg.dayamed.util.UserRole;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Eresh Gorantla on 23/Feb/2019
 **/
@Documented
@Constraint(validatedBy = {UserRoleValidatorImpl.class})
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserRoleValidator {

    public abstract String message() default "Invalid value. This is not permitted.";

    public abstract Class<?>[] groups() default {};

    public abstract  UserRole[] expectedValues();

    public abstract Class<? extends Payload>[] payload() default {};

    public abstract Class<? extends UserRole> enumClass() default UserRole.class;
}
