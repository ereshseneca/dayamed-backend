package com.sg.dayamed.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Eresh Gorantla on 18/Feb/2019
 **/
@Component
public class ExecutorServiceConfig {

    @Bean("prescriptionDetailsProcessing")
    public ExecutorService prescriptionDetailsProcessing() {
        ExecutorService executorService = new ThreadPoolExecutor(5, 10, 5, TimeUnit.MINUTES, new ArrayBlockingQueue<>(100));
        return executorService;
    }

    @Bean("prescriptionNotifications")
    public ExecutorService prescriptionNotifications() {
        ExecutorService executorService = new ThreadPoolExecutor(5, 10, 5, TimeUnit.MINUTES, new ArrayBlockingQueue<>(100));
        return executorService;
    }

    @Bean("emailNotifications")
    public ExecutorService emailNotifications() {
        ExecutorService executorService = new ThreadPoolExecutor(3, 5, 2, TimeUnit.MINUTES, new ArrayBlockingQueue<>(100));
        return executorService;
    }

    @Bean("smsNotifications")
    public ExecutorService smsNotifications() {
        ExecutorService executorService = new ThreadPoolExecutor(3, 5, 2, TimeUnit.MINUTES, new ArrayBlockingQueue<>(100));
        return executorService;
    }

    @Bean(name = "errorLogs")
    public ExecutorService errorLogs() {
        ExecutorService executorService = new ThreadPoolExecutor(3, 5, 2, TimeUnit.MINUTES, new ArrayBlockingQueue<>(10));
        return executorService;
    }

}
