package com.sg.dayamed.config;

import com.sg.dayamed.commons.exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 24/Apr/2019
 **/
@Component
public class DayamedStartupConfiguration implements ApplicationRunner {

    @Autowired
    Environment environment;

    private void updateCountryAndStateInformation() throws ApplicationException {
        try {

        } catch (Exception e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(environment.getProperty("load.country.and.state.data"));
        System.out.println(args.getOptionNames());
    }
}
