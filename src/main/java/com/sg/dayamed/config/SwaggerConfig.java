package com.sg.dayamed.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 22/Mar/2019
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    private TypeResolver typeResolver;

    ModelRef errorModel = new ModelRef("WSErrorResponse");

    private List<ResponseMessage> responseMessages =
            Stream.of(new ResponseMessageBuilder().code(400).responseModel(new ModelRef("RestFault")).message("Bad request123").build()).collect(Collectors.toList());

    @Bean
    public Docket webApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sg.dayamed.rest.web"))
                .paths(PathSelectors.regex("/api/w.*"))
                .build().apiInfo(apiEndPointsInfo())
                .forCodeGeneration(true).groupName("Web Apis")
                .useDefaultResponseMessages(false);
                //.globalResponseMessage(RequestMethod.POST, responseMessages).enableUrlTemplating(true)
                //.alternateTypeRules(AlternateTypeRules.newRule(typeResolver.resolve(RestFault.class, typeResolver.resolve(RestFault.class, WildcardType.class),
                //Ordered.HIGHEST_PRECEDENCE)));

    }

    @Bean
    public Docket mobileApi() {
        return new Docket(DocumentationType.SWAGGER_2).forCodeGeneration(true).groupName("Mobile Apis").select()
                .apis(RequestHandlerSelectors.basePackage("com.sg.dayamed.rest.mobile"))
                .paths(PathSelectors.regex("/api/m.*"))
                .build().apiInfo(apiEndPointsInfo());
    }

    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .deepLinking(true)
                .displayOperationId(false)
                .defaultModelRendering(ModelRendering.MODEL)
                .docExpansion(DocExpansion.LIST)
                .filter(false)
                .maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA)
                .showExtensions(false)
                .tagsSorter(TagsSorter.ALPHA)
                //.supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
                .validatorUrl(null)
                .build();
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("DayaMed Application")
                .description("DayaMed REST API")
                .contact(new Contact("xxxxxxxxxx", "xxxxxxxxxx", "xxxxxxxxx"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}
