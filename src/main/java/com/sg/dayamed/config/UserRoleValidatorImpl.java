package com.sg.dayamed.config;

import com.sg.dayamed.util.UserRole;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Eresh Gorantla on 23/Feb/2019
 **/

public class UserRoleValidatorImpl implements ConstraintValidator<UserRoleValidator, Integer> {

    private UserRoleValidator annotation;


    @Override
    public void initialize(UserRoleValidator annotation) {
        this.annotation = annotation;
    }

    @Override
    public boolean isValid(Integer valueForValidation, ConstraintValidatorContext constraintValidatorContext) {
        if (valueForValidation == null) {
            return false;
        }
        UserRole[] enumValues = this.annotation.enumClass().getEnumConstants();

        if (enumValues != null) {
            UserRole[] expectedValues = this.annotation.expectedValues();
            for (UserRole userRole : expectedValues) {
                if (userRole.getId().equals(valueForValidation)) {
                    return true;
                }
            }
        }

        return false;
    }
}
