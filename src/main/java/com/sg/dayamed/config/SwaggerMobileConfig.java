package com.sg.dayamed.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.ModelReference;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 22/Mar/2019
 **/
@Configuration
@EnableSwagger2
public class SwaggerMobileConfig {

    ModelReference modelReference = new ModelRef("RestResponse");
    ModelReference errorRef = new ModelRef("RestFault");

    private List<ResponseMessage> responseMessages1 =
            Stream.of(new ResponseMessageBuilder().code(HttpStatus.OK.value()).message(HttpStatus.OK.name()).responseModel(modelReference).build(),
                    new ResponseMessageBuilder().code(HttpStatus.UNAUTHORIZED.value()).message(HttpStatus.UNAUTHORIZED.name()).responseModel(errorRef).build(),
                    new ResponseMessageBuilder().code(HttpStatus.BAD_REQUEST.value()).message(HttpStatus.BAD_REQUEST.name()).responseModel(errorRef).build(),
                    new ResponseMessageBuilder().code(HttpStatus.FORBIDDEN.value()).message(HttpStatus.FORBIDDEN.name()).responseModel(errorRef).build(),
                    new ResponseMessageBuilder().code(HttpStatus.INTERNAL_SERVER_ERROR.value()).message(HttpStatus.INTERNAL_SERVER_ERROR.name()).responseModel(errorRef).build()).collect(Collectors.toList());



    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("DayaMed Application")
                .description("DayaMed REST API")
                .contact(new Contact("xxxxxxxxxx", "xxxxxxxxxx", "xxxxxxxxx"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}
