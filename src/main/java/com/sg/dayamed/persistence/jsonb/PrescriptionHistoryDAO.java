package com.sg.dayamed.persistence.jsonb;

import com.sg.dayamed.persistence.domain.PrescriptionHistoryDetails;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * Created by Eresh Gorantla on 27/Mar/2019
 **/
@Component
public class PrescriptionHistoryDAO {

    private JdbcTemplate jdbcTemplate;

    public PrescriptionHistoryDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void updatePrescriptionHistoryDetails(PrescriptionHistoryDetails historyDetails, Integer index) {

        //JsonbQueryUtil.generateJsonSet()
    }
}
