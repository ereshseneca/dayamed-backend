package com.sg.dayamed.persistence.jsonb;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.config.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Created by Eresh Gorantla on 27/Mar/2019
 **/
public class JsonbQueryUtil {

    private static final String COMMA = ",";
    private static final String OPEN_BRACKET = "(";
    private static final String CLOSE_BRACKET = ")";
    private static final String SINGLE_QUOTE = "'";
    private static final String DOUBLE_QUOTE = "\"\"";
    private static final String OPEN_CURLY_BRACE = "{";
    private static final String CLOSED_CURLY_BRACE = "}";

    public static String generateJsonSet(String columnName, String target, Integer indexPos, Object object, Boolean createMissingTag, Boolean isArray) {
        StringBuilder jsonSet = new StringBuilder(64);
        jsonSet.append("jsonb_set").append(OPEN_BRACKET);
        if (isArray) {
            jsonSet.append(columnName);
            jsonSet.append(COMMA).append(StringUtils.SPACE);
            if (indexPos == null || NumberUtils.INTEGER_ZERO.equals(indexPos)) {
                jsonSet.append(SINGLE_QUOTE).append(OPEN_CURLY_BRACE).append(target).append(CLOSED_CURLY_BRACE).append(SINGLE_QUOTE);
            } else {
                jsonSet.append(SINGLE_QUOTE).append(OPEN_CURLY_BRACE).append(target).append(COMMA).append(StringUtils.SPACE).append(indexPos).append(CLOSED_CURLY_BRACE).append(SINGLE_QUOTE);
            }
            jsonSet.append(SINGLE_QUOTE).append(convertObjectToString(object)).append(SINGLE_QUOTE);
            jsonSet.append(COMMA).append(StringUtils.SPACE);
            jsonSet.append(createMissingTag);
        }
        return jsonSet.toString();
    }

    private static String convertObjectToString(Object object) {
        try {
            ObjectMapper objectMapper = BeanUtil.getBean(ObjectMapper.class);
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {

        }
        return null;
    }
}
