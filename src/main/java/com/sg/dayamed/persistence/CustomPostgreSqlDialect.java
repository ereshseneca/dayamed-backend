package com.sg.dayamed.persistence;

import org.hibernate.dialect.PostgreSQL94Dialect;

import java.sql.Types;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/

public class CustomPostgreSqlDialect extends PostgreSQL94Dialect {
    public CustomPostgreSqlDialect() {
        this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
    }
}
