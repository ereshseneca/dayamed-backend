package com.sg.dayamed.persistence.specification;

import com.sg.dayamed.commons.crypto.CryptoUtil;
import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 07/Apr/2019
 **/

public class PatientSearchSpecification implements Specification<PatientAssociationInformationV> {

    String searchText;
    Long roleMapId;

    public PatientSearchSpecification(String searchText, Long roleMapId) {
        this.searchText = searchText;
        this.roleMapId = roleMapId;
    }

    @Override
    public Predicate toPredicate(Root<PatientAssociationInformationV> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(cb.and(cb.equal(root.get("userRoleMapId"), roleMapId)));
        predicates.add(cb.or(cb.like(root.get(CryptoUtil.decryptData("patientFirstName")), searchText), cb.like(root.get("patientLastName"), searchText)));
        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
