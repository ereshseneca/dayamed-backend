package com.sg.dayamed.persistence.specification;

import com.sg.dayamed.persistence.domain.PrescriptionDetailsProcessing;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 20/Feb/2019
 **/

public class PrescriptionProcessingSpecification implements Specification<PrescriptionDetailsProcessing> {

    private JobRequestVO requestVO;

    public PrescriptionProcessingSpecification(JobRequestVO requestVO) {
        this.requestVO = requestVO;

    }

    @Override
    public Predicate toPredicate(Root<PrescriptionDetailsProcessing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<Predicate>();
        query.orderBy(cb.asc(root.get("prescribedTime")));
        predicates.add(cb.and(cb.in(root.get("statusId")).value(requestVO.getAdherenceStatuses()), cb.lessThanOrEqualTo(root.get("prescribedTime"), requestVO.getCurrentTime())));

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
