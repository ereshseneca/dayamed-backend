package com.sg.dayamed.persistence.specification;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by Eresh Gorantla on 20/Feb/2019
 **/
@Getter
@Setter
public class JobRequestVO {
    private Date currentTime;
    private List<Integer> adherenceStatuses;
}
