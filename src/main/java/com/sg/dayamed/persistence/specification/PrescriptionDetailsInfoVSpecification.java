package com.sg.dayamed.persistence.specification;

import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/

public class PrescriptionDetailsInfoVSpecification implements Specification<PrescriptionDetailInfoV> {

    @Override
    public Predicate toPredicate(Root<PrescriptionDetailInfoV> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(cb.and(cb.greaterThanOrEqualTo(root.get("presStartDate"), ZonedDateTime.now()), cb.lessThanOrEqualTo(root.get("presEndDate"), ZonedDateTime.now())));
        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
