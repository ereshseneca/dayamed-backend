package com.sg.dayamed.persistence.specification;

import com.sg.dayamed.persistence.domain.view.ActiveUserPrescriptionDetailsV;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/

public class PrescriptionHourlyDataSpecification implements Specification<ActiveUserPrescriptionDetailsV> {

    private Integer hour;

    public PrescriptionHourlyDataSpecification(Integer hour) {
        this.hour = hour;
    }

    @Override
    public Predicate toPredicate(Root<ActiveUserPrescriptionDetailsV> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<Predicate>();
        query.orderBy(cb.asc(root.get("prescribedTime")));
        String startMinute = hour + ":00";
        String endMinute = hour + ":59";
        predicates.add(
                cb.and(cb.greaterThanOrEqualTo(root.get("prescribedTime"), startMinute),
                        cb.lessThanOrEqualTo(root.get("prescribedTime"), endMinute)));
        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
