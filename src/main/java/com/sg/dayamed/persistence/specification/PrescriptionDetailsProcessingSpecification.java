package com.sg.dayamed.persistence.specification;

import com.sg.dayamed.persistence.domain.PrescriptionDetailsProcessing;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Eresh Gorantla on 01/Mar/2019
 **/

public class PrescriptionDetailsProcessingSpecification implements Specification<PrescriptionDetailsProcessing> {



    @Override
    public Predicate toPredicate(Root<PrescriptionDetailsProcessing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<Predicate>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = simpleDateFormat.parse("2019-03-01");
        } catch (Exception e) {

        }
        //predicates.add(cb.equal(cb.function("TRUNC", ZonedDateTime.class, root.get("createdDate")), cb.function("TO_DATE", String.class,  root.get("createdDate"), cb.literal("YYYY-MM-DD"))));
        predicates.add(cb.equal(cb.function("DATE", ZonedDateTime.class, root.get("createdDate")), date));
        System.out.println();
        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
