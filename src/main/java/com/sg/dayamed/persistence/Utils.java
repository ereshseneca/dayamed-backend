package com.sg.dayamed.persistence;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.sg.dayamed.persistence.domain.JsonSample;

import java.io.IOException;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/

public class Utils {
    private static ObjectWriter joWriter;
    private static ObjectReader joReader;
    static {
        final ObjectMapper mapper = new ObjectMapper();
        joWriter = mapper.writer();
        joReader = mapper.readerFor(JsonSample.class);
    }

    public static String getJsonRepresenatation(JsonSample hm) throws IOException {
        return joWriter.writeValueAsString(hm);
    }

    public static JsonSample getObjectFromJson(final String jsonString) throws IOException {
        return joReader.readValue(jsonString);
    }
}
