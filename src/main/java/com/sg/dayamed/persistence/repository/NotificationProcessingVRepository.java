package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.NotificationProcessingV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

/**
 * Created by Eresh Gorantla on 09/Mar/2019
 **/
@Repository
public interface NotificationProcessingVRepository extends JpaRepository<NotificationProcessingV, String> {
    List<NotificationProcessingV> findByTriggerTimeAndNpCreatedDateBetween(Date triggerTime, ZonedDateTime start, ZonedDateTime end);
}
