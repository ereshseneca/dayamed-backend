package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.PrescriptionInfoType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Repository
public interface PrescriptionInfoTypeRepository extends JpaRepository<PrescriptionInfoType, Long> {
}
