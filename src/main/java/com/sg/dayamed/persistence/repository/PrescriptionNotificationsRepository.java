package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import com.sg.dayamed.persistence.domain.PrescriptionNotifications;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 06/Mar/2019
 **/
@Repository
public interface PrescriptionNotificationsRepository extends JpaRepository<PrescriptionNotifications, Long> {
    List<PrescriptionNotifications> findByPrescriptionInfoInAndDefaultFlag(List<PrescriptionInfo> prescriptionInfoS, Boolean defaultFlag);
}
