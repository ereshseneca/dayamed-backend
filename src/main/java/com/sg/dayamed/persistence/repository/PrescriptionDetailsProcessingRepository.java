package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.PrescriptionDetailsProcessing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Repository
public interface PrescriptionDetailsProcessingRepository extends JpaRepository<PrescriptionDetailsProcessing, String>, JpaSpecificationExecutor<PrescriptionDetailsProcessing> {

    public List<PrescriptionDetailsProcessing> findByProcessed(Boolean processed);
    List<PrescriptionDetailsProcessing> findByCreatedDateBetween(ZonedDateTime start, ZonedDateTime end);
    List<PrescriptionDetailsProcessing> findByPrescriptionInfoIdInAndCreatedDate(List<Long> prescriptionInfoId, ZonedDateTime createdDate);
}
