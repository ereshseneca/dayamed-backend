package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.PresNotificationInformationV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Eresh Gorantla on 27/Feb/2019
 **/
@Repository
public interface PresNotificationInformationVRepository extends JpaRepository<PresNotificationInformationV, String> {
    List<PresNotificationInformationV> findByPatientIdIn(List<Long> patientIds);
    List<PresNotificationInformationV> findAllByPrescriptionInfoIdAndPresEndDateGreaterThanEqual(Long prescriptionInfoId, ZonedDateTime endDate);
}
