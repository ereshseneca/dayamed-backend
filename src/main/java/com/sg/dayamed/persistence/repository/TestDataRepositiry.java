package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.TestData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/
@Repository
public interface TestDataRepositiry extends JpaRepository<TestData, Long> {

    @Query(value = "SELECT * FROM test_data WHERE data ->> 'city' ILIKE ?1", nativeQuery = true)
    public List<TestData> fingByCityName(String city);
}
