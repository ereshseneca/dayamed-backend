package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.PrescriptionDetailInfoV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Repository
public interface PrescriptionDetailInfoVRepository extends JpaRepository<PrescriptionDetailInfoV, String> {
    List<PrescriptionDetailInfoV> findByPatientIdAndPrescriptionIdAndPresEndDateGreaterThan(Long patientId, Long prescriptionId, ZonedDateTime currentDate);
    List<PrescriptionDetailInfoV> findAllByActivePrescriptionInfoAndPresEndDateGreaterThanEqual(Boolean active, ZonedDateTime currentDate);
    List<PrescriptionDetailInfoV> findAllByPresEndDateGreaterThanEqual(ZonedDateTime currentDate);
    List<PrescriptionDetailInfoV> findByPatientIdAndCreatorIdAndActivePrescription(Long patientId, Long creatorId, Boolean activePrescription);
    List<PrescriptionDetailInfoV> findByPrescriptionId(Long patientId);
    List<PrescriptionDetailInfoV> findByPrescriptionInfoIdIn(List<Long> prescriptionInfoId);
    List<PrescriptionDetailInfoV> findByPrescriptionIdInAndPrescriptionInfoIdIn(List<Long> prescriptionIds, List<Long> prescriptionInfoIds);
    List<PrescriptionDetailInfoV> findByPrescriptionIdIn(List<Long> ids);
}
