package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.Prescription;
import com.sg.dayamed.persistence.domain.UserDetails;
import com.sg.dayamed.persistence.domain.UserRoleMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Repository
public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {
    List<Prescription> findByIdIn(List<Long> ids);
    Page<Prescription> findByPatientAndUserRoleMappingOrderByActiveDesc(UserDetails userDetails, UserRoleMapping userRoleMapping, Pageable pageable);
}
