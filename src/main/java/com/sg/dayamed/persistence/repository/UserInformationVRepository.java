package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.UserInformationV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 22/Feb/2019
 **/
@Repository
public interface UserInformationVRepository extends JpaRepository<UserInformationV, String>, JpaSpecificationExecutor<UserInformationV> {
    UserInformationV findByUserIdAndRoleId(Long userId, Integer roleId);
    List<UserInformationV> findByUserEmailId(String emailId);
    List<UserInformationV> findByUserIdIn(List<Long> userIds);
    List<UserInformationV> findByUserId(Long userId);
    List<UserInformationV> findByRoleMappingIdInAndRoleActive(List<Long> roleMappingIds, Boolean userActive);
    UserInformationV findByRoleMappingId(Long roleMappingId);
}
