package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.PatientAssociations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 23/Feb/2019
 **/
@Repository
public interface PatientAssociationsRepostory extends JpaRepository<PatientAssociations, Long> {
}
