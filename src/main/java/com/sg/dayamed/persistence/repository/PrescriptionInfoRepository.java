package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.Prescription;
import com.sg.dayamed.persistence.domain.PrescriptionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Repository
public interface PrescriptionInfoRepository extends JpaRepository<PrescriptionInfo, Long> {
    List<PrescriptionInfo> findByPrescription(Prescription prescription);
}
