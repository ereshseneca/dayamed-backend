package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.NotificationProcessing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 01/Mar/2019
 **/
@Repository
public interface NotificationProcessingRepository extends JpaRepository<NotificationProcessing, Long> {
    //List<NotificationProcessing> findByTriggerTimeEqualAndCreatedDateBetween(Date dateTime, ZonedDateTime start, ZonedDateTime end);
    List<NotificationProcessing> findByPrescriptionProcessingIdIn(List<String> processingIds);
}
