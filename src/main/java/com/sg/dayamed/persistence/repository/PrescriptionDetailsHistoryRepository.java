package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.PrescriptionDetailsHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 28/Mar/2019
 **/
@Repository
public interface PrescriptionDetailsHistoryRepository extends JpaRepository<PrescriptionDetailsHistory, String> {
    List<PrescriptionDetailsHistory> findByPrescriptionId(Long prescriptionId);
    List<PrescriptionDetailsHistory> findByPrescriptionIdIn(List<Long> prescriptionIds);
}
