package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.PrescriptionInfoV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Repository
public interface PrescriptionInfoVRepository extends JpaRepository<PrescriptionInfoV, String> {
    List<PrescriptionInfoV> findByPrescriptionIdAndUserId(Long prescriptionId, Long userId);
    List<PrescriptionInfoV> findByPrescriptionIdIn(List<Long> prescriptionIds);
    List<PrescriptionInfoV> findByUserIdIn(List<Long> patientIds);
}
