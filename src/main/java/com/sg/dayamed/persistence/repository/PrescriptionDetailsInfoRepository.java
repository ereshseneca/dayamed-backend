package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.PrescriptionDetailInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Repository
public interface PrescriptionDetailsInfoRepository extends JpaRepository<PrescriptionDetailInfo, Long> {
}
