package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.ActiveUserPrescriptionDetailsV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Repository
public interface ActiveUserPrescriptionDetailsVRepository extends JpaRepository<ActiveUserPrescriptionDetailsV, String>, JpaSpecificationExecutor<ActiveUserPrescriptionDetailsV> {
}
