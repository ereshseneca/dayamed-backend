package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 14/Feb/2019
 **/
@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {
}
