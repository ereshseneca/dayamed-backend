package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.SchedulerJobs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 09/Mar/2019
 **/
@Repository
public interface SchedulerJobsRepository extends JpaRepository<SchedulerJobs, Long> {
}
