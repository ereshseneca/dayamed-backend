package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.NotificationSubscriptionDetailsV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 04/Mar/2019
 **/
@Repository
public interface NotificationSubscriptionDetailsVRepository extends JpaRepository<NotificationSubscriptionDetailsV, String> {
    public List<NotificationSubscriptionDetailsV> findByDefaultFlag(Boolean flag);
}
