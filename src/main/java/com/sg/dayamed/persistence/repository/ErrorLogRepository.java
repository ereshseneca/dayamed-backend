package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.ErrorLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 29/Mar/2019
 **/
@Repository
public interface ErrorLogRepository extends JpaRepository<ErrorLog, String> {
}
