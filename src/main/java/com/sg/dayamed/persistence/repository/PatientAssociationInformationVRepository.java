package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.PatientAssociationInformationV;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Repository
public interface PatientAssociationInformationVRepository extends JpaRepository<PatientAssociationInformationV, String>, JpaSpecificationExecutor<PatientAssociationInformationV> {
    List<PatientAssociationInformationV> findByPatientId(Long patientId);
    Page<PatientAssociationInformationV> findByUserIdAndRoleId(Long userId, Integer roleId, Pageable pageRequest);
    PatientAssociationInformationV findByPatientIdAndUserRoleMapId(Long patientId, Long roleMapId);
    List<PatientAssociationInformationV> findByUserRoleMapIdAndActiveOrderByActiveDesc(Long roleMapId, Boolean active);
    List<PatientAssociationInformationV> findByUserRoleMapIdOrderByActiveDesc(Long roleMapId);
    //List<PatientAssociationInformationV> findByPatientIdAndUserIdAndRoleId(Long patientId, Long userId, Integer roleId);
}
