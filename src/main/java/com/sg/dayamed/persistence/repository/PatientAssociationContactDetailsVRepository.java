package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.view.PatientAssociationContactDetailsV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 26/Feb/2019
 **/
@Repository
public interface PatientAssociationContactDetailsVRepository extends JpaRepository<PatientAssociationContactDetailsV, String> {
    PatientAssociationContactDetailsV findByPatientId(Long patientId);
}
