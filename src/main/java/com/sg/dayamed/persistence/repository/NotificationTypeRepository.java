package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.NotificationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 05/Mar/2019
 **/
@Repository
public interface NotificationTypeRepository extends JpaRepository<NotificationType, Integer> {

}
