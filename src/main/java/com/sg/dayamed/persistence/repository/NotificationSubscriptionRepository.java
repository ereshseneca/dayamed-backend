package com.sg.dayamed.persistence.repository;

import com.sg.dayamed.persistence.domain.NotificationSubscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Eresh Gorantla on 04/Mar/2019
 **/
@Repository
public interface NotificationSubscriptionRepository extends JpaRepository<NotificationSubscription, Long> {
}
