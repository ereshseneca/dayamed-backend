package com.sg.dayamed.persistence.domain.view;

import com.sg.dayamed.jpa.converters.StringConverterWithPattern;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Eresh Gorantla on 26/Feb/2019
 **/
@Entity
@Table(name = "patient_association_contact_details_v")
@Getter
@Setter
public class PatientAssociationContactDetailsV {

    @Id
    private String id;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "email_ids")
    @Convert(converter = StringConverterWithPattern.class)
    private String emailIds;

    @Column(name = "roles")
    private String roles;

    @Column(name = "mobile_numbers")
    @Convert(converter = StringConverterWithPattern.class)
    private String mobileNumbers;

    /*@Column(name = "ids")
    private String ids;*/

    @Column(name = "names")
    @Convert(converter = StringConverterWithPattern.class)
    private String names;

}
