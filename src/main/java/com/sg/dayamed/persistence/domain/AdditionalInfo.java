package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 19/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class AdditionalInfo {
    private String brandName;
    private String composition;
}
