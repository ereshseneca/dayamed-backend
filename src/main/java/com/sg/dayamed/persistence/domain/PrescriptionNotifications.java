package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Entity
@Table(name = "prescription_notifications")
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class PrescriptionNotifications {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "prescription_info_id")
    private PrescriptionInfo prescriptionInfo;

    @Column(name = "recipients")
    private String recipients;

    @Column(name = "delay_in_minutes")
    private Integer delayInMinutes;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "notification_type_id")
    private NotificationType notificationType;

    @Column(name = "created_date", updatable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "updated_date", insertable = false)
    @UpdateTimestamp
    private ZonedDateTime updatedDate;

    @Column(name = "default_flag")
    private Boolean defaultFlag = false;
}
