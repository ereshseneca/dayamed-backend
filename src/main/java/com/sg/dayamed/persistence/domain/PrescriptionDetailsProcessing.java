package com.sg.dayamed.persistence.domain;

import com.sg.dayamed.jpa.converters.StringCryptoConverter;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Created by Eresh Gorantla on 18/Feb/2019
 **/
@Entity
@Table(name = "prescription_details_processing")
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
public class PrescriptionDetailsProcessing {

    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "patient_name")
    @Convert(converter = StringCryptoConverter.class)
    private String patientName;

    @Column(name = "patient_mobile_number")
    @Convert(converter = StringCryptoConverter.class)
    private String patientMobileNumber;

    @Column(name = "patient_email_id")
    @Convert(converter = StringCryptoConverter.class)
    private String patientEmailId;

    @Column(name = "prescribed_time")
    @Temporal(TemporalType.TIME)
    private Date prescribedTime;

    @Column(name = "observed_time")
    @Temporal(TemporalType.TIME)
    private Date observedTime;

    @Column(name = "actual_attempts")
    private Integer actualAttempts = 0;

    @Column(name = "status")
    private String status;

    @Column(name = "status_id")
    private Integer statusId;

    @Column(name = "created_date", updatable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "updated_date", insertable = false)
    private ZonedDateTime updatedDate;

    @Column(name = "expected_attempts")
    private Integer expectedAttempts;

    @Column(name = "prescription_info_id")
    private Long prescriptionInfoId;

    @Column(name = "actual_date")
    private ZonedDateTime actualDate;

    @Column(name = "actual_time")
    private String actualTime;

    @Column(name = "processed")
    private Boolean processed;

    @Column(name = "prescription_info_type_id")
    private Long presInfoTypeId;

}
