package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 27/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionHistory {
    private List<PrescriptionHistoryDetails> details;
}
