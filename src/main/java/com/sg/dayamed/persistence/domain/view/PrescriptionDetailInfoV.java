package com.sg.dayamed.persistence.domain.view;

import com.sg.dayamed.jpa.converters.StringCryptoConverter;
import com.sg.dayamed.persistence.domain.IntakeTime;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Entity
@Table(name = "prescription_detail_info_v")
@Getter
@Setter
public class PrescriptionDetailInfoV {

    @Id
    private String id;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "patient_name")
    @Convert(converter = StringCryptoConverter.class)
    private String patientName;

    @Column(name = "patient_mobile_number")
    @Convert(converter = StringCryptoConverter.class)
    private String patientMobileNumber;

    @Column(name = "patient_email_id")
    @Convert(converter = StringCryptoConverter.class)
    private String patientEmailId;

    @Column(name = "pres_id")
    private Long prescriptionId;

    @Column(name = "pres_name")
    private String prescriptionName;

    @Column(name = "provider_comment")
    private String providerComment;

    @Column(name = "patient_illness_desc")
    private String patientIllnessDesc;

    @Column(name = "active_prescription")
    private Boolean activePrescription = false;

    @Column(name = "pres_created_date")
    private ZonedDateTime presCreatedDate;

    @Column(name = "pres_creator_id")
    private Long creatorId;

    @Column(name = "pres_creator_role_id")
    private Integer presCreatorRoleId;

    @Column(name = "pres_creator_role")
    private String presCreatorRole;

    @Column(name = "prescription_info_id")
    private Long prescriptionInfoId;

    @Column(name = "active_prescription_info")
    private Boolean activePrescriptionInfo = false;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private IntakeTime intakeTime;

    @Column(name = "pres_info_type_id")
    private Long presInfoTypeId;

    @Column(name = "pres_info_type")
    private String presInfoType;

    @Column(name = "pres_type_id")
    private Integer presTypeId;

    @Column(name = "pres_type")
    private String presType;

    @Column(name = "pi_zone_id")
    private String zoneId;

    @Column(name = "pres_start_date")
    private ZonedDateTime presStartDate;

    @Column(name = "pres_end_date")
    private ZonedDateTime presEndDate;

    @Column(name = "pres_info_comment")
    private String presInfoComment;

    @Column(name = "course_duration")
    private Integer courseDuration;

    @Column(name = "notification_id")
    private Long notificationId;

    @Column(name = "recipients")
    private String recipients;

    @Column(name = "notification_type_id")
    private Integer notificationTypeId;

    @Column(name = "notification_type")
    private String notificationType;

    @Column(name = "notification_delay")
    private Integer notificationDelay;

    @Column(name = "default_notification")
    private Boolean defaultNotification = false;

    @Column(name = "notification_created_date")
    private ZonedDateTime notificationCreatedDate;

    @Column(name = "diagnosis")
    private String diagnosis;
}
