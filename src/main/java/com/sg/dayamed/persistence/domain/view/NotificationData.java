package com.sg.dayamed.persistence.domain.view;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Eresh Gorantla on 27/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class NotificationData {
    private String notificationType;
    private List<String> recipients;
    private ZonedDateTime triggerTime;
}
