package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 14/Feb/2019
 **/
@Entity
@Table(name = "user_role_information")
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class UserRoleInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "admin_flag")
    private Boolean adminFlag = false;

    @Column(name = "active")
    private Boolean active = false;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private UserInformationData data;

    @Column(name = "created_date", updatable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "updated_date", insertable = false)
    private ZonedDateTime updatedDate;
}
