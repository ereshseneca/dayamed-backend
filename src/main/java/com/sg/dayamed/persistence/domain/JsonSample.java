package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/
@Getter
@Setter
public class JsonSample {
    private Integer id;
    private String name;
    private String city;
}
