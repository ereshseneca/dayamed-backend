package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 10/Apr/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class FrequencyDetails {
    private Integer typeId;
    private List<Object> actualValues;
    private Integer recurrence;
    private Integer duration;
    private String startDate;
}
