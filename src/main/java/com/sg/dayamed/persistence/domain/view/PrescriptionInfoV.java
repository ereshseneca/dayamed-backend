package com.sg.dayamed.persistence.domain.view;

import com.sg.dayamed.persistence.domain.FrequencyDetails;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 19/Feb/2019
 **/
@Entity
@Table(name = "prescription_info_v")
@Getter
@Setter
public class PrescriptionInfoV {

    @Id
    @Column(name = "id")
    private String uniqueId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "prescription_id")
    private Long prescriptionId;

    @Column(name = "prescription_name")
    private String prescriptionName;

    @Column(name = "prescription_info_id")
    private Long prescriptionInfoId;

    @Column(name = "intake_time")
    private String intakeTime;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb", name = "frequency")
    private FrequencyDetails frequency;

    @Column(name = "course_duration")
    private Integer courseDuration;

    @Column(name = "active_prescription")
    private boolean activePrescription;

    @Column(name = "prescription_created_date")
    private ZonedDateTime presCreatedDate;

    @Column(name = "pres_info_created_date")
    private ZonedDateTime presInfoCreatedDate;

    @Column(name = "pres_comment")
    private String presComment;
}
