package com.sg.dayamed.persistence.domain;

import com.sg.dayamed.jpa.converters.IntegerCryptoConverter;
import com.sg.dayamed.jpa.converters.StringCryptoConverter;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Eresh Gorantla on 14/Feb/2019
 **/
@Getter
@Setter
@Entity
@Table(name = "user_details")
@DynamicInsert
@DynamicUpdate
public class UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "age")
    @Convert(converter = IntegerCryptoConverter.class)
    private Integer age;

    @Column(name = "email_id")
    @Convert(converter = StringCryptoConverter.class)
    private String emailId;

    @Column(name = "first_name")
    @Convert(converter = StringCryptoConverter.class)
    private String firstName;

    @Column(name = "middle_name")
    @Convert(converter = StringCryptoConverter.class)
    private String middleName;

    @Column(name = "last_name")
    @Convert(converter = StringCryptoConverter.class)
    private String lastName;

    @Column(name = "gender")
    @Convert(converter = StringCryptoConverter.class)
    private String gender;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "imantic_user_id")
    private String imanticUserId;

    @Column(name = "mobile_number")
    @Convert(converter = StringCryptoConverter.class)
    private String mobileNumber;

    @Column(name = "password")
    @Convert(converter = StringCryptoConverter.class)
    private String password;

    @Column(name = "active")
    private Boolean active = false;

    @Column(name = "pwd_change_token")
    private String pwdChangeToken;

    @Column(name = "pwd_token_expire_date")
    private String pwdTokenExpireDate;

    @Column(name = "race")
    private String race;

    @Column(name = "token")
    private String token;

    @Column(name = "device_type")
    private String deviceType;

    @Column(name = "voip_token")
    private String voipToken;

    @Column(name = "created_date", updatable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "updated_date", insertable = false)
    private ZonedDateTime updatedDate;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "user_address", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "address_id", referencedColumnName = "id")})
    private List<Address> addresses;

}
