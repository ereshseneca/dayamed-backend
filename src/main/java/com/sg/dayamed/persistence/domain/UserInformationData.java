package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserInformationData {
    private String emoji;
}
