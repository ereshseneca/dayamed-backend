package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 26/Feb/2019
 **/
@Entity
@Table(name = "pres_notification_information_v")
@Setter
@Getter
public class PrescriptionNotificationInfoV {
    @Id
    private String id;

    @Column(name = "prescription_id")
    private Long prescriptionId;

    @Column(name = "prescription_name")
    private String prescriptionName;

    @Column(name = "prescription_info_id")
    private Long prescriptionInfoId;

    @Column(name = "recipients")
    private String recipients;

    @Column(name = "notification_type_id")
    private Integer notificationTypeId;

    @Column(name = "notification_type")
    private String notificationType;

    @Column(name = "pres_start_date")
    private ZonedDateTime presStartDate;

    @Column(name = "pres_end_date")
    private ZonedDateTime presEndDate;

    @Column(name = "course_duration")
    private Integer courseDuration;
}
