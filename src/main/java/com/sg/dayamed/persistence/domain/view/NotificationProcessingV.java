package com.sg.dayamed.persistence.domain.view;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Created by Eresh Gorantla on 08/Mar/2019
 **/
@Entity
@Table(name = "notification_processing_v")
@Getter
@Setter
@NoArgsConstructor
public class NotificationProcessingV {

    @Id
    private String id;

    @Column(name = "prescription_processing_id")
    private String prescriptionProcessingId;

    @Column(name = "notification_processing_id")
    private String notificationProcessingId;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "patient_name")
    private String patientName;

    @Column(name = "patient_mobile_number")
    private String patientMobileNumber;

    @Column(name = "recipients")
    private String recipients;

    @Column(name = "patient_email_id")
    private String patientEmailId;

    @Column(name = "prescribed_time")
    @Temporal(TemporalType.TIME)
    private Date prescribedTime;

    @Column(name = "observed_time")
    @Temporal(TemporalType.TIME)
    private Date observedTime;

    @Column(name = "actual_date")
    private ZonedDateTime actualDate;

    @Column(name = "pdp_created_date")
    private ZonedDateTime pdpCreatedDate;

    @Column(name = "prescription_info_id")
    private Long prescriptionInfoId;

    @Column(name = "pdp_status")
    private String pdpStatus;

    @Column(name = "pdp_status_id")
    private Integer pdpStatusId;

    @Column(name = "trigger_time")
    @Temporal(TemporalType.TIME)
    private Date triggerTime;

    @Column(name = "np_created_date")
    private ZonedDateTime npCreatedDate;

    @Column(name = "notification_type")
    private String notificationType;

    @Column(name = "notification_type_id")
    private Integer notificationTypeId;

    @Column(name = "prescription_id")
    private Long prescriptionId;

    @Column(name = "zone_id")
    private String zoneId;
}
