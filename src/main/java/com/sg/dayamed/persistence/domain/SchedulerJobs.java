package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 09/Mar/2019
 **/
@Entity
@Table(name = "scheduler_jobs")
@Getter
@Setter
public class SchedulerJobs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "job_name")
    private String jobName;

    @Column(name = "status")
    private String status;

    @Column(name = "processing_time")
    private Long processingTime;

    @Column(name = "error_stacktrace")
    @Lob
    private String errorStacktrace;

    @Column(name = "created_date")
    private ZonedDateTime createdDate = ZonedDateTime.now();
}
