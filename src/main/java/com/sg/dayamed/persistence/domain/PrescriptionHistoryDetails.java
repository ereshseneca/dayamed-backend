package com.sg.dayamed.persistence.domain;

import com.sg.dayamed.persistence.domain.view.NotificationData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 27/Mar/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PrescriptionHistoryDetails {
    private ZonedDateTime consumptionDate;
    private ZonedDateTime prescribedTime;
    private ZonedDateTime observedTime;
    private Integer status;
    private String zoneId;
    private NotificationData notificationData;
}
