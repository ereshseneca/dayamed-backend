package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Created by Eresh Gorantla on 18/Feb/2019
 **/
@Entity
@Table(name = "prescription_detail_info")
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class PrescriptionDetailInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private PrescriptionInfo prescriptionInfo;

    @Column(name = "prescribed_time")
    @Temporal(TemporalType.TIME)
    private Date prescribedTime;

    @Column(name = "start_date")
    private ZonedDateTime startDate = ZonedDateTime.now();

    @Column(name = "end_date")
    private ZonedDateTime endDate;

    @Column(name = "created_date")
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "updated_date")
    private ZonedDateTime updatedDate;
}
