package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Entity
@Table(name = "notifications_processing")
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class NotificationProcessing {

    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "prescription_processing_id")
    private String prescriptionProcessingId;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "patient_name")
    private String patientName;

    @Column(name = "patient_mobile_number")
    private String patientMobileNumber;

    @Column(name = "patient_email_id")
    private String patientEmailId;

    @Column(name = "medicine_name")
    private String medicineName;

    @Column(name = "prescribed_time")
    @Temporal(TemporalType.TIME)
    private Date prescribedTime;

    @Column(name = "observed_time")
    @Temporal(TemporalType.TIME)
    private Date observedTime;

    @Column(name = "recipients")
    private String recipients;

    @Column(name = "trigger_time")
    @Temporal(TemporalType.TIME)
    private Date triggerTime;

    @Column(name = "notification_type_id")
    private Integer notificationTypeId;

    @Column(name = "notification_type")
    private String notificationType;

    @Column(name = "created_date", updatable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "updated_date", insertable = false)
    private ZonedDateTime updatedDate;

    @Column(name = "association_details")
    private String associationDetails;

    @Column(name = "processed")
    private Boolean processed;
}
