package com.sg.dayamed.persistence.domain.view;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Eresh Gorantla on 18/Feb/2019
 **/
@Entity
@Table(name = "active_user_prescription_details_v")
@Getter
@Setter
public class ActiveUserPrescriptionDetailsV {

    @Id
    @Column(name = "id")
    private String uniqueId;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "patient_name")
    private String patientName;

    @Column(name = "patient_mobile_number")
    private String patientMobileNumber;

    @Column(name = "patient_email_id")
    private String patientEmailId;

    @Column(name = "medicine_name")
    private String medicineName;

    @Column(name = "prescribed_time")
    private String prescribedTime;

    @Column(name = "notification_emails")
    private String notificationEmails;

    @Column(name = "email_time")
    private Integer emailTime;

    @Column(name = "notification_mobile_numbers")
    private String notificationMobileNumbers;

    @Column(name = "sms_time")
    private Integer smsTime;

    @Column(name = "notification_video_calls")
    private String notificationVideoCalls;

    @Column(name = "video_call_time")
    private Integer videoCallTime;

    @Column(name = "prescription_notification_id")
    private Long prescriptionNotificationId;

    @Column(name = "prescription_id")
    private Long prescriptionId;

    @Column(name = "prescription_info_id")
    private Long prescriptionInfoId;

    @Column(name = "prescription_detail_info_id")
    private Long prescriptionDetailInfoId;

    @Column(name = "active_prescription_info")
    private boolean activePrescriptionInfo;

}
