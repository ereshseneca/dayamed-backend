package com.sg.dayamed.persistence.domain.view;

import com.sg.dayamed.jpa.converters.IntegerCryptoConverter;
import com.sg.dayamed.jpa.converters.StringCryptoConverter;
import com.sg.dayamed.persistence.domain.UserInformationData;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 22/Feb/2019
 **/
@Entity
@Table(name = "user_information_v")
@Getter
@Setter
public class UserInformationV {

    @Id
    private String id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "role_mapping_id")
    private Long roleMappingId;

    @Column(name = "user_role_information_id")
    private Long userRoleInformationId;

    @Column(name = "role_id")
    private Integer roleId;

    @Column(name = "role_name")
    private  String roleName;

    @Column(name = "user_first_name")
    @Convert(converter = StringCryptoConverter.class)
    private String userFirstName;

    @Column(name = "user_middle_name")
    @Convert(converter = StringCryptoConverter.class)
    private String userMiddleName;

    @Column(name = "user_last_name")
    @Convert(converter = StringCryptoConverter.class)
    private  String userLastName;

    @Column(name = "user_age")
    @Convert(converter = IntegerCryptoConverter.class)
    private Integer userAge;

    @Column(name = "user_email_id")
    @Convert(converter = StringCryptoConverter.class)
    private String userEmailId;

    @Column(name = "gender")
    @Convert(converter = StringCryptoConverter.class)
    private String gender;

    @Column(name = "user_active")
    private Boolean userActive = false;

    @Column(name = "urm_active")
    private Boolean roleActive;

    @Column(name = "user_image_url")
    private String userImageUrl;

    @Column(name = "imantic_user_id")
    private String imanticUserId;

    @Column(name = "user_mobile_number")
    @Convert(converter = StringCryptoConverter.class)
    private String userMobileNumber;

    @Column(name = "user_password")
    @Convert(converter = StringCryptoConverter.class)
    private String userPassword;

    @Column(name = "pwd_change_token")
    private String pwdChangeToken;

    @Column(name = "pwd_token_expire_date")
    private String pwdTokenExpiryDate;

    @Column(name = "race")
    private String race;

    @Column(name = "user_token")
    private String userToken;

    @Column(name = "voip_token")
    private String voipToken;

    @Column(name = "user_created_date")
    private ZonedDateTime userCreatedDate;

    @Column(name = "user_updated_date")
    private ZonedDateTime userUpdatedDate;

    @Column(name = "admin_flag")
    private Boolean adminFlag = false;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb", name = "uri_data")
    private UserInformationData data;

    @Column(name = "uri_active")
    private Boolean uriActive;

    @Column(name = "urm_created_date")
    private ZonedDateTime urmCreatedDate;

    @Column(name = "uri_created_date")
    private ZonedDateTime urlCreatedDate;
}
