package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Entity
@Table(name = "prescription_details_history")
@Getter
@Setter
@DynamicUpdate
@DynamicInsert
public class PrescriptionDetailsHistory {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "patient_name")
    private String patientName;

    @Column(name = "patient_mobile_number")
    private String patientMobileNumber;

    @Column(name = "patient_email_id")
    private String patientEmailId;

    @Column(name = "prescription_id")
    private Long prescriptionId;

    @Column(name = "prescription_info_id")
    private Long prescriptionInfoId;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private PrescriptionHistory history;

    @Column(name = "created_date", updatable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "updated_date", insertable = false)
    private ZonedDateTime updatedDate;

}
