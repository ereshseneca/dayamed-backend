package com.sg.dayamed.persistence.domain.view;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 04/Mar/2019
 **/
@Entity
@Table(name = "notification_subscription_details_v")
@Getter
@Setter
public class NotificationSubscriptionDetailsV {

    @Id
    private String id;

    @Column(name = "notification_type_id")
    private Integer notificationTypeId;

    @Column(name = "notification_type")
    private String notificationType;

    @Column(name = "notification_subscription_id")
    private String notificationSubscriptionId;

    @Column(name = "notification_subscription")
    private String notificationSubscription;

    @Column(name = "default_flag")
    private Boolean defaultFlag;

    @Column(name = "ns_created_date")
    private ZonedDateTime nsCreatedDate;

    @Column(name = "nsd_id")
    private Long nsdId;

    @Column(name = "notification_delay")
    private Integer notificationDelay;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "nsd_created_date")
    private ZonedDateTime nsdCreatedDate;
}
