package com.sg.dayamed.persistence.domain.view;

import com.sg.dayamed.jpa.converters.IntegerCryptoConverter;
import com.sg.dayamed.jpa.converters.StringCryptoConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 24/Feb/2019
 **/
@Entity
@Table(name = "patient_association_information_v")
@Getter
@Setter
public class PatientAssociationInformationV {
    @Id
    private String id;

    @Column(name = "association_id")
    private Long associationId;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "patient_first_name")
    @Convert(converter = StringCryptoConverter.class)
    private String patientFirstName;

    @Convert(converter = StringCryptoConverter.class)
    @Column(name = "patient_middle_name")
    private String patientMiddleName;

    @Convert(converter = StringCryptoConverter.class)
    @Column(name = "patient_last_name")
    private String patientLastName;

    @Column(name = "patient_email_id")
    @Convert(converter = StringCryptoConverter.class)
    private String patientEmailId;

    @Column(name = "patient_mobile_number")
    @Convert(converter = StringCryptoConverter.class)
    private String patientMobileNumber;

    @Column(name = "patient_gender")
    @Convert(converter = StringCryptoConverter.class)
    private String patientGender;

    @Column(name = "patient_image_url")
    private String patientImageUrl;

    @Column(name = "patient_created_date")
    private ZonedDateTime patientCreatedDate;

    @Column(name = "patient_age")
    @Convert(converter = IntegerCryptoConverter.class)
    private Integer patientAge;

    @Column(name = "user_role_map_id")
    private Long userRoleMapId;

    @Column(name = "active")
    private Boolean active = false;

    @Column(name = "role_id")
    private Integer roleId;

    @Column(name = "role")
    private String role;

    @Column(name = "association_created_date")
    private ZonedDateTime associationCreatedDate;
}
