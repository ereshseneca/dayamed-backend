package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 18/Feb/2019
 **/
@Entity
@Table(name = "prescription")
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class Prescription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "provider_comment")
    private String providerComment;

    @Column(name = "patient_illness_desc")
    private String patientIllnessDesc;

    @Column(name = "patient_illness_diagnosis")
    private String patientIllnessDiagnosis;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "creator_id")
    private UserRoleMapping userRoleMapping;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "prescription")
    private List<PrescriptionInfo> prescriptionInfoS = new ArrayList<>();

    @Column(name = "active")
    private Boolean active = true;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private UserDetails patient;

    @Column(name = "created_date", updatable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "updated_date", insertable = false)
    @UpdateTimestamp
    private ZonedDateTime updatedDate;
}
