package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by Eresh Gorantla on 29/Mar/2019
 **/
@Entity
@Table(name = "error_log")
@Getter
@Setter
public class ErrorLog {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "request_uri")
    private String requestUri;

    @Column(name = "service_name")
    private String serviceName;

    @Column(name = "method_name")
    private String methodName;

    @Column(name = "status_code")
    private Integer statusCode;

    @Column(name = "user_agent")
    private String userAgent;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb", name = "request_header")
    private String requestHeader;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb", name = "response")
    private String response;


    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "stacktrace", columnDefinition = "text")
    private String stacktrace;

    @Column(name = "created_date")
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "http_method")
    private String httpMethod;

}
