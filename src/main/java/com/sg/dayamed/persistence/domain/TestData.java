package com.sg.dayamed.persistence.domain;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Eresh Gorantla on 13/Mar/2019
 **/
@Entity
@Table(name = "test_data")
@Getter
@Setter
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class TestData  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /*@Column(name = "data")
    @Type(type = "JsonDataUserType", parameters = {@Parameter(name = "className", value = "JsonSample")})*/
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private JsonSample data;

   /* @Type(type = "JsonDataUserType")
    @Access(AccessType.PROPERTY)
    @Column(name = "data")
    public String getTestAsString() throws IOException {
        return Utils.getJsonRepresenatation(data);
    }

    public TestData setTestAsString(String jsonData) throws IOException {
        if (jsonData != null)
            data = Utils.getObjectFromJson(jsonData);
        return this;
    }*/

}
