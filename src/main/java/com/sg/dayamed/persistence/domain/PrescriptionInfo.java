package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 18/Feb/2019
 **/
@Entity
@Table(name = "prescription_info")
@Getter
@Setter
@DynamicUpdate
@DynamicInsert
public class PrescriptionInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "comment")
    private String comment;

    @Column(name = "course_duration")
    private Integer courseDuration;

    @Column(name = "identity_code")
    private String identityCode;

    @Column(name = "pharmacist_comment")
    private String pharmacistComment;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private IntakeTime intakeTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "prescription_id")
    private Prescription prescription;

    @OneToMany(mappedBy = "prescriptionInfo", cascade = CascadeType.ALL)
    private List<PrescriptionNotifications> notifications = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "prescription_info_type_id")
    private PrescriptionInfoType prescriptionInfoType;

    /*@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "medicine_id")
    private Medicine medicine;*/

    @Column(name = "start_date")
    private ZonedDateTime startDate;

    @Column(name = "end_date")
    private ZonedDateTime endDate;

    @Column(name = "zone_id")
    private String zoneId;

    @Column(name = "created_date", updatable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();
    @UpdateTimestamp
    @Column(name = "updated_date", insertable = false)
    private ZonedDateTime updatedDate;

    @Column(name = "active")
    private Boolean active = true;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private FrequencyDetails frequency;
}
