package com.sg.dayamed.persistence.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 24/Mar/2019
 **/
@Getter
@Setter
public class IntakeTime {
    private List<String> details;
}
